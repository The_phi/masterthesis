﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(Camera))]
public class ScreenShotSystem : MonoBehaviour
{
    public ShaderEditorHandler m_shaderEditorHandler;
    public List<GameObject> m_screenShotObjects;

    private Camera m_camThis;
    private RenderTexture rendTex;

    private Vector3 m_centerOffset;
    private Vector3 m_currentOffset;
    private Vector3 m_currentRotation;
    private Transform m_transThis;

    private int m_frameCounter = 0;
    private int m_currentView = 0;
    private int m_currentMipLevel;
    private int m_counter = 0;

    private Vector3 m_offsetTop = new Vector3(0, 5, 0);
    private Vector3 m_offsetBottom = new Vector3(0, -5, 0);
    private Vector3 m_offsetLeft = new Vector3(-5, 0, 0);
    private Vector3 m_offsetRight = new Vector3(5, 0, 0);
    private Vector3 m_offsetFront = new Vector3(0, 0, 5);
    private Vector3 m_offsetBack = new Vector3(0, 0, -5);
    private Vector3 m_offsetFrontTop = new Vector3(-3, 6, 3);

    private Vector3 m_rotationTop = new Vector3(90, 0, 0);
    private Vector3 m_rotationBottom = new Vector3(270, 0, 0);
    private Vector3 m_rotationLeft = new Vector3(0, 90, 0);
    private Vector3 m_rotationRight = new Vector3(0, 270, 0);
    private Vector3 m_rotationFront = new Vector3(0, 180, 0);
    private Vector3 m_rotationBack = new Vector3(0, 0, 0);
    private Vector3 m_rotationFrontTop = new Vector3(45, 135, 0);

    private Bounds m_bounds;
    private string m_folderName = "top";
    private string m_folderPrefix = "screenGrabs/";

    // Start is called before the first frame update
    void Awake()
    {
        if(m_screenShotObjects.Count == 0)
        {
            Debug.LogError("no elements in screenshot list");
            return;
        }


        m_camThis = GetComponent<Camera>();
        m_transThis = this.transform;
        m_bounds = m_screenShotObjects[0].GetComponentInChildren<Renderer>().bounds;
        m_centerOffset = m_bounds.center - m_screenShotObjects[0].transform.position;

        m_currentOffset = m_centerOffset + m_offsetTop;
        m_currentRotation = m_rotationTop;
        setCameraPosAndRot(m_screenShotObjects[0]);

        // hide all objects except the first one
        foreach (GameObject go in m_screenShotObjects)
        {
            go.SetActive(false);
        }
        m_screenShotObjects[0].SetActive(true);

        updateRenderTexture(false, false);

        m_shaderEditorHandler.setShaderValues(0);
    }

    // Update is called once per frame
    void Update()
    {
        m_frameCounter++;

        //wait a little bit at the beginning to ensure everything is initialised
        if(m_frameCounter < 8)
        {
            return;
        }
        if(m_currentView > 6)
        {
            Debug.Log("all views done.");
            return;
        }

        // take screenshots
        if(m_counter < m_screenShotObjects.Count)
        {
            if(m_frameCounter % 2 == 0)
            {
                GameObject go = m_screenShotObjects[m_counter];
                string folderPath = m_folderPrefix + "/" + m_folderName  + "/mip" + m_currentMipLevel;
                ensureFolderExists(folderPath);
                saveRenderTexture(folderPath + "/" + m_counter + go.name + ".png");
            }
            else
            {
                GameObject go = m_screenShotObjects[m_counter];
                go.SetActive(false);
                m_counter++;
                if (m_counter < m_screenShotObjects.Count)
                {
                    go = m_screenShotObjects[m_counter];
                    go.SetActive(true);
                    setCameraPosAndRot(go);
                }

            }

        }
        else
        {
            if(m_currentMipLevel < 9)
            {
                m_currentMipLevel++;
                m_shaderEditorHandler.setShaderValues(m_currentMipLevel);
                resetCameraToStart();
                // make counter even again
                m_frameCounter--;
            }
            else
            {
                m_currentMipLevel = 0;
                m_shaderEditorHandler.setShaderValues(m_currentMipLevel);
                handleNextViewDirection();
            }
        }
    }

    private void handleNextViewDirection()
    {
        m_currentView++;
        // reposition camera
        if (m_currentView == 1)
        {
            m_currentOffset = m_centerOffset + m_offsetBottom;
            m_currentRotation = m_rotationBottom;
            m_folderName = "bottom";
            updateRenderTexture(false, false);
        }
        if (m_currentView == 2)
        {
            m_currentOffset = m_centerOffset + m_offsetLeft;
            m_currentRotation = m_rotationLeft;
            m_folderName = "left";
            updateRenderTexture(false, true);
        }
        if (m_currentView == 3)
        {
            m_currentOffset = m_centerOffset + m_offsetRight;
            m_currentRotation = m_rotationRight;
            m_folderName = "right";
            updateRenderTexture(false, true);
        }
        if (m_currentView == 4)
        {
            m_currentOffset = m_centerOffset + m_offsetFront;
            m_currentRotation = m_rotationFront;
            m_folderName = "front";
            updateRenderTexture(true, false);
        }
        if (m_currentView == 5)
        {
            m_currentOffset = m_centerOffset + m_offsetBack;
            m_currentRotation = m_rotationBack;
            m_folderName = "back";
            updateRenderTexture(true, false);
        }
        if (m_currentView == 6)
        {
            m_currentOffset = m_centerOffset + m_offsetFrontTop;

            Vector3 pos = m_screenShotObjects[0].transform.position + m_currentOffset;
            m_transThis.position = pos;
            this.transform.LookAt(m_bounds.center);
            m_currentRotation = this.transform.rotation.eulerAngles;
            m_folderName = "frontTop";
            updateRenderTexture(true, false);
        }


        resetCameraToStart();
        // make counter even again
        m_frameCounter--;
    }

    private void resetCameraToStart()
    {
            m_counter = 0;
            GameObject go = m_screenShotObjects[m_counter];
            go.SetActive(true);
            setCameraPosAndRot(go);
    }

    private void setCameraPosAndRot(GameObject _go)
    {
        Vector3 pos = _go.transform.position + m_currentOffset;
        m_transThis.position = pos;
        m_transThis.rotation = Quaternion.Euler(m_currentRotation.x,
            m_currentRotation.y, m_currentRotation.z);
    }

    private void ensureFolderExists(string _directoryPath)
    {
        if (!Directory.Exists(_directoryPath))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(_directoryPath);
        }
    }

    private void updateRenderTexture(bool front, bool left)
    {
        Vector3 center = m_bounds.center;
        Vector3 scale = m_bounds.size;

        float width = 10;
        float height = 10;

        if (front)
        {
            width = scale.x;
            height = scale.y;
        }
        else if (left)
        {
            width = scale.z;
            height = scale.y;
        }
        else
        {
            width = scale.x;
            height = scale.z;
        }

        m_camThis.orthographicSize = height / 2;
        float aspectRatio = width / height;
        m_camThis.aspect = aspectRatio;

        int screenWidth = 2048;
        int screenHeight = (int)(screenWidth / aspectRatio);
        rendTex = new RenderTexture(screenWidth, screenHeight, 16, RenderTextureFormat.ARGB32);
        m_camThis.targetTexture = rendTex;
    }

    private void saveRenderTexture(string _filePath)
    {
        Texture2D texture = new Texture2D(rendTex.width, rendTex.height, TextureFormat.ARGB32, false, false);
        RenderTexture.active = rendTex;
        texture.ReadPixels(new Rect(0, 0, rendTex.width, rendTex.height), 0, 0);
        texture.Apply();

        byte[] bytes = texture.EncodeToPNG();
        UnityEngine.Object.Destroy(texture);

        System.IO.File.WriteAllBytes(_filePath, bytes);
    }
}
