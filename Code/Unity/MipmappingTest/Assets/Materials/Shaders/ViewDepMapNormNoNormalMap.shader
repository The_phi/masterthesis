﻿Shader "Custom/Unlit/ViewDepMapNormNoNormalMap"
{
	Properties
	{
	}
		SubShader
		{
			Pass
			{
				Tags{ "RenderType" = "Opaque" }
				LOD 200

				CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vs_main
				#pragma fragment fs_main

				struct VS_IN
				{
					float4 position : POSITION;
					float3 normal : NORMAL;
					float3 tangent : TANGENT;
					float2 uv : TEXCOORD0;
				};

				struct VS_OUT
				{
					float4 position : POSITION;
					float3 normal : NORMAL;

					// TANGENT, BINORMAL, NORMAL semantics are only available for input of vertex shader
					float3 T : TEXCOORD3;
					float3 B : TEXCOORD4;
					float3 N : TEXCOORD5;
				};

				VS_OUT vs_main(VS_IN input)
				{
					VS_OUT o;

					// calc output position directly
					o.position = UnityObjectToClipPos(input.position);

					o.normal = input.normal;
					float3 worldNormal = mul((float3x3)unity_ObjectToWorld, input.normal);
					float3 worldTangent = mul((float3x3)unity_ObjectToWorld, input.tangent);

					float3 binormal = cross(input.normal, input.tangent.xyz); // *input.tangent.w;
					float3 worldBinormal = mul((float3x3)unity_ObjectToWorld, binormal);

					// and, set them
					o.N = normalize(worldNormal);
					o.T = normalize(worldTangent);
					o.B = normalize(worldBinormal);

					return o;
				}

				float4 fs_main(VS_OUT input) : COLOR
				{
					float3x3 TBN = float3x3(normalize(input.T), normalize(input.B), normalize(input.N));
					TBN = transpose(TBN);

					float3 finalNormal = input.normal;
					float3 worldFinalNormal = mul(TBN, finalNormal);
					worldFinalNormal = normalize(worldFinalNormal);
					worldFinalNormal = worldFinalNormal + 1;
					worldFinalNormal = worldFinalNormal / 2;

					return float4(worldFinalNormal, 1);
				}
				ENDCG
			}
		}
			FallBack "Diffuse"
}
