#pragma once

#include <cmath>
// D3D12 extension library.
#include "DirectXTex/d3dx12.h"
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"

#include "Helper.h"

#include <Eigen/Dense>

using namespace DirectX;

struct TextureMatrices
{
	Eigen::ArrayXXi* imgRed;
	Eigen::ArrayXXi* imgGreen;
	Eigen::ArrayXXi* imgBlue;
};

class PerceptualDownsamplingFilter
{
public:
	///_np = patch size
	static void perceptualDownsampling(const Image& _pSrcImage, const Image& _pDstImage, int _scalingFactor, int _np);


private :
	static Eigen::ArrayXXd& convValid(const Eigen::ArrayXXd& _mat, const int _kernelSize);
	static Eigen::ArrayXXd& convFull(const Eigen::ArrayXXd& _mat, const int _kernelSize);
	static Eigen::ArrayXXd& subSample(const Eigen::ArrayXXd& _mat, const int _sampleDist);

	static Eigen::ArrayXXi* createPerceptualDownscaledMatrix(const Eigen::ArrayXXi& _baseMat, int _s, int _np);
	static TextureMatrices loadTextureIntoMatrices(const Image& _baseImage);
	static void loadImageIntoMatrices(const uint8_t* _pSrc, Eigen::ArrayXXi& _redMat, Eigen::ArrayXXi& _greenMat, Eigen::ArrayXXi& _blueMat,
		const size_t _pixelSize, const size_t _rowPitch, const size_t _width, const size_t _height);
	static void storeMatricesInImage(uint8_t* _pDst, const Eigen::ArrayXXi& _redMat, const Eigen::ArrayXXi& _greenMat, const Eigen::ArrayXXi& _blueMat,
		const size_t _pixelSize, const size_t _rowPitch, const size_t _width, const size_t _height);
};

