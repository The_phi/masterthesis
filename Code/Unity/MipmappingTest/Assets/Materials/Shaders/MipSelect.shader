﻿Shader "Custom/Unlit/MipSelectShader" 
{
	Properties
	{
		m_LightColor("LightColor", Color) = (1,1,1,1)
		m_MainTex("Albedo (RGB)", 2D) = "white" {}
		m_MainLightDirection("MainLightDirection", Vector) = (0,0,0,0)
		m_AmbientLightStrength("Ambient Light strength",  Range(0.0,1.0)) = 0.1
		m_MipLevelAlbedo("Mip Level Albedo", Range(0,9)) = 1
		m_useMipSelector("Use mip selector slider",  Range(0.0,1.0)) = 1
	}
	SubShader
	{
		Pass
		{
			Tags{ "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vs_main
			#pragma fragment fs_main

			struct VS_IN
			{
				float4 position : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct VS_OUT
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
				float3 lightdir : TEXCOORD1;
				float3 normal : NORMAL;
			};

			uniform fixed m_MipLevelAlbedo;
			float m_NormalFactor;

			uniform float3 m_MainLightDirection;
			uniform float4 m_LightColor;
			float m_AmbientLightStrength;

			uniform sampler m_MainTex;

			uniform float m_useMipSelector;

			VS_OUT vs_main(VS_IN input)
			{
				VS_OUT output;
				output.position = UnityObjectToClipPos(input.position);
				output.uv = input.uv;
				output.normal = input.normal;
				output.lightdir = normalize(m_MainLightDirection);
				return output;
			}

			float4 fs_main(VS_OUT input) : COLOR
			{
				float4 texCoord = float4(0,0,0,0);
				texCoord.xy = input.uv;
				texCoord.w = m_MipLevelAlbedo;
				float4 albedo;
				if (m_useMipSelector > 0.5f)
				{
					albedo = tex2Dlod(m_MainTex, texCoord);
				}
				else
				{
					albedo = tex2D(m_MainTex, texCoord.xy);
				}
				float3 lightDir = normalize(input.lightdir);
				float3 diffuse = saturate(dot(input.normal, -lightDir));
				diffuse = m_LightColor * albedo.rgb * diffuse;
				float3 ambient = float3(m_AmbientLightStrength, m_AmbientLightStrength, m_AmbientLightStrength) * albedo;
				// combine all of colors
				return float4(ambient + diffuse, 1);
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}