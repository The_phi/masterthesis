#taken from https://ourcodeworld.com/articles/read/991/how-to-calculate-the-structural-similarity-index-ssim-between-two-images-with-python
# Usage:
#
# python3 script.py --input original.png --output modified.png
# Based on: https://github.com/mostafaGwely/Structural-Similarity-Index-SSIM-

# 1. Import the necessary packages
from skimage.measure import compare_ssim
from skimage.metrics import structural_similarity
import argparse
import imutils
import cv2
import os
import glob
import numpy as np
 
print("python is thinking... dont pressure it to hard")

def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n

def l1DistImage(imageA, imageB):
    l1DistImageColor = imageA - imageB
    l1DistImage = np.ndarray(grayB.shape)
    for x in range(grayB.shape[0]):
        for y in range(grayB.shape[1]):
            #for z in range(l1DistImageColor.shape[2]):
             #   l1DistImageColor[x,y,z] = clamp(groundTruth[x,y,z] - imageB[x,y,z], 0, 255)
            resultSum = (int(l1DistImageColor[x,y,0])) + (int(l1DistImageColor[x,y,1])) + (int(l1DistImageColor[x,y,2]))
            #resultSum = clamp(resultSum, 0, 255)
            l1DistImage[x,y] = resultSum / 3

    cv2.imwrite(argThird + "\\"+ path + "L1Distance.png", l1DistImage)
    cv2.imwrite(argThird + "\\"+ path + "L1DistanceColor.png", l1DistImageColor)


# 2. Construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True, help="Path of ground truth image that will be compared")
ap.add_argument("-s", "--second", required=True, help="Directory of the images that will be used to compare")
ap.add_argument("-t", "--third", required=True, help="target directors for SSIM diff images")
args = vars(ap.parse_args())

argFirst = args["first"]
argSecond = args["second"]
argThird = args["third"]

if os.path.exists(argThird) == False:
    os.makedirs(argThird)

# load groundTruth
groundTruth = cv2.imread(argFirst)
grayGroundTruth = cv2.cvtColor(groundTruth, cv2.COLOR_BGR2GRAY)

data_path = os.path.join(argSecond,'*g')
files = glob.glob(data_path)
for f1 in files:
    print(f1)
    # 3. Load the two input images
    imageB = cv2.imread(f1)
    # 4. Convert the images to grayscale
    grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

    # 5. Compute the Structural Similarity Index (SSIM) between the two
    #    images, ensuring that the difference image is returned
    (ssimScore, diffImage) = structural_similarity(grayGroundTruth, grayB, full=True)
    diffImage = (diffImage * 255).astype("uint8")
    path = f1[:-4]
    path = path[path.rindex('\\')+1:]
    cv2.imwrite(argThird + "\\"+ path + "SSIMDiff.png", diffImage)

    psnrScore = cv2.PSNR(groundTruth, imageB)
    l1DistanceScore = cv2.norm(groundTruth, imageB, cv2.NORM_L1)

    l1DistImage(groundTruth, imageB)

    # 6. You can print only the score if you want
    print("SSIM: {}".format(ssimScore))
    print("PSNR: {}".format(psnrScore))
    print("L1 Distance: {}".format(l1DistanceScore))
    print("-------------------------------------")
