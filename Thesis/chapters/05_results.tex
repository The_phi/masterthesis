% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Results}\label{chapter:results}
This chapter analyses the performance of the previously described mipmapping techniques.


\section{Comparison Methodology}
Three different objects were used to compare the results of all previously described methods, these  are: box, kaiser, perceptual, lanczos, parameterization aware, LPIPS, view dependent box mapping, and view dependent spherical mapping. All objects use textures of size $2048x2048$. To provide a fair comparison, all view-dependent textures were initialized with dimensions similar to the original textures. This assures equal mipmap details in all methods. The spherical mapping texture was created to be of equal size as the original texture. The box mapping textures were initialized with a width equal to the width of the original texture. The height depends on the actual object geometry and is therefore not exactly identical.

Both SSIM and LPIPS create scores from zero to one. Unfortunately SSIM uses one as  score for images that are perfectly equal whereas LPIPS uses zero. To improve readability in this thesis all SSIM values are flipped with zero as best score and one as worst.

\subsection{Test Objects}
All test objects were taken from blendswap under the CC-0 license. They can be found under the following urls:

\begin{itemize}
\item \href{https://www.blendswap.com/blend/24187}{Bowser (https://www.blendswap.com/blend/24187)}\\
\item \href{https://www.blendswap.com/blend/24078}{Mushroom (https://www.blendswap.com/blend/24078)}\\
\item \href{https://www.blendswap.com/blend/7865}{Blast bot robot (https://www.blendswap.com/blend/7865)}
\end{itemize}

Bowsers model is named "Browser" on Blendswap. He will be called Bowser in this thesis.


\begin{figure}
\makebox[\textwidth][c]{
\includegraphics[width=1\textwidth]{testObjects}
}
\caption{The three test objects. On the left is Bowser, in the middle the mushroom, and on the right the Blast bot.}
\label{fig:testObjects}
\end{figure}

\subsection{Image Generation}
Images of all objects were taken from several directions. To enable a fair comparison, the images were taken from the same directions with orthographic projection. The used directions are as follows: front, back, top, bottom, left, right, and from the top left front. In addition, all objects and methods were rendered from the 0th to the 9th mip level.

This approach allows to compare the effects of the different techniques on a specific object and ensures small differences between them are not lost.

\subsubsection{Methodology Issues}
In reality, scenes are almost always composed of more than one object. In addition, one object would normally not use the same mip level everywhere, but instead display multiple mip levels across its surface. This should be considered when evaluating the results presented in this thesis.

View-dependent mipmapping uses the same directions as are used for the comparison images. Therefore it can happen that it receives a better score on higher mip levels since its projected textures are effectively the same as the ground truth images used to calculate the image similarity metrics. To evaluate this effect an additional direction (from the front top left) was added to the comparison images. The results align with those from the other directions. This suggests that the conclusions gained from the other images are correct.

\subsection{Comparison Technique}
The images were compared using the SSIM and LPIPS metrics described in chapters \ref{chapter:relatedWork} and \ref{chapter:techniques}. The image of 0th level of the box mipmapping method was used as ground truth. Each mip level is compared to the ground truth image.



\section{General Findings}
Most algorithms tend to perform differently on lower mip levels than on higher ones when compared to each other. This can be seen in figure \ref{fig:browserLPIPSFrontTop}, where parameterization aware down-sampling is rated slightly worse for the first two mip levels but outperforms almost everything starting at mip level four and above. Surprisingly, both image similarity metrics rate all methods with similar scores or at least with similar trends. An example can be seen in the figures \ref{fig:browserLPIPSFrontTop} and \ref{fig:browserSSIMFrontTop} which show the results for the same scene evaluated with both metrics.
\medskip

The resulting scores for the individual methods strongly depend on the complexity of the geometry and texture. If the geometry is simpler with less concave elements (like the spikes on Bowsers back) view-dependent methods perform much better, and can even outperform all other methods in the case of box projection (for both similarity metrics). This effect is enhanced by simpler textures with less individual uv-islands.
\medskip

The methods with the best scores for higher levels are Lanczos and LPIPS. Both techniques outperform Box, Kaiser, and parametrization aware filtering on these levels. Lanczos keeps its good ratings across all mip levles, whereas LPIPS drastically looses quality after the first few mip levels. The loss is caused by the color-shift mentioned in the last chapter. On lower mip levels parametrization aware filtering receives the best scores overall. The only exception is the mushroom object. In this case view-dependent mipmapping with box projection is rated better than all other methods. This anomaly is probably caused by a combination of simple geometry and a simple texture.


\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.7\textwidth]{browserLPIPSFrontTopLeft}}%
\caption{Comparison results using the LPIPS metric of Bowser from the top left. Note that that because box and kaiser down-sampling are valued almost identical, the box line is hidden by the one for kaiser filtering.}\label{fig:browserLPIPSFrontTop}
\end{figure}

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.7\textwidth]{browserSSIMFrontTopLeft}}%
\caption{Comparison results using the SSIM metric of Bowser from the top left. Note that because Box and Kaiser down-sampling are rated almost identical the Box line is hidden by the one for Kaiser filtering.}\label{fig:browserSSIMFrontTop}
\end{figure}

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.7\textwidth]{blastBotLPIPSFrontTopLeft}}%
\caption{Comparison results using the LPIPS metric of the BlastBot from the top left. Note that because Box and Kaiser down-sampling are rated almost identical the Box line is hidden by the one for Kaiser filtering.}\label{fig:blastBotLPIPSFrontTopLeft}
\end{figure}

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.7\textwidth]{mushroomLPIPSFrontTopLeft}}%
\caption{Comparison results using the LPIPS metric of the mushroom from the top left. Note that because Box and Kaiser down-sampling are rated almost identical the Box line is hidden by the one for Kaiser filtering.}\label{fig:mushroomLPIPSFrontTopLeft}
\end{figure}

Overall, Box and Kaiser sampling perform almost identically. This might be due to the simplicity of the textures. Figure \ref{fig:mushroomMip6BoxKaiser} shows the slight differences between the methods.

\medskip


Surprisingly, perceptual down-sampling receives very low scores from both LPIPS and SSIM even though it specifically optimizes the texture for the SSIM metric. This is likely due to this down-scaling method only working in the two-dimensional texture space and completely disregarding the actual projection of the texture onto the objects surface. This creates many areas where the wrong colors can be seen on the mesh. In addition perceptual down-sampling tends to create sharp edges and corners in images. When photos are scaled down, this behaviour is desired since it aligns with human perception. Unfortunately, textures tend to be made up put of individual uv-islands separated by unused space. If these individual areas are scaled down they have to align with the vertex mapping. Otherwise the blank areas between them can be seen. Perceptual down-sampling creates sharp edges, which might not be perfectly aligned with the uv-mapping, adding to the already present mapping distortions.

\medskip

Of all tested methods, view-dependent mipmapping with spherical projection produces the worst results across both metrics and almost all mip levels. This is especially noticeable on the higher levels where the projection errors have the biggest impact. Spherical mapping receives scores similar to Kaiser filtering on the mushroom object, but keeps its worse rating on the other two objects. This is caused by the combination of simple geometry with a simple texture.


\section{Performance Impacts}
All methods only change the texture in the precomputation step where the different mip levels are generated. Therefore they have no performance impact on the actual rendering process. The only exception to this is view-dependent mipmapping with box projection, since it requires six textures instead of one. This can result in more draw-calls and larger buffers, slightly decreasing performance. In addition, it is possible that not all six textures fit in the GPUs texture cache. This will result in texture streaming from L2 cache to texture cache, increasing render times.

\subsection{Memory Usage}
Apart from the view-dependent methods all techniques only change the mip levels of the original texture and do not need any additional memory. Spherical view-dependent mipmapping behaves similarly since it only requires one texture.

A clear exception to this rule is view-dependent mipmapping with box projection, which needs multiple textures. Depending on the size of each texture, this can cause a significantly higher memory footprint. In the worst case this method will need up to six times more memory than normal mipmapping.

\section{View-dependent Methods}
Both view-dependent methods are ranked worse than all other methods for almost all mip levels due to the severe errors caused by the respective projection methods. Especially spherical projection causes big distortions for complexer geometry.

\section{Projection Errors}
Both tested methods for view-dependent mipmapping produce severe mapping errors as can be seen in image \ref{fig:boxProjErrorExample}. They are caused by projection shortcomings. In the case of box projection, if the object has several parts with normals all facing into the same direction and in-front of each other, only the one nearest to the camera will be rendered to the specific side view. This will cause all these parts to receive the same color during final rendering. Examples of this can be seen on Bowsers feet or his mouth. A similar problem occurs with the spherical projection method if bits of the object are occluded in the direction of the raycast during preprocessing.

Another mapping problem produced by the box mapping variant is seen on areas where one view texture switches to another. Here a sharp contrast caused by the misalignment of the colors in both images can be seen.

All issues mentioned in this section are only really visible if the camera is close to the target. If the object is far enough away it becomes so small on the screen that the errors are hardly visible. At these distances, view-dependent mipmapping with box mapping can have positive effects under certain circumstances (see section \ref{section:positiveResults}).

The spherical method suffers from wrong texture generation if the raycast center is outside the objects geometry.

Because of these errors it is advisable to only use view-dependent mipmapping for the lower miplevels to avoid the worst of the visual artefacts.

\subsection{Down-sampling Techniques for Box Projection}
All three previously discussed down-sampling techniques - based on a color, based on surface normals, and simple box (see chapter \ref{sec:BoxMapDownSample}) - perform similarly on the first view mip levels and only start to diverge on the lower ones. Overall, box down-scaling receives the best scores by both LPIPS and SSIM. The other two receive similar scores by both methods, with surface normal based down-scaling receiving better results on lower levels and worse scores on the higher ones. The color based approach receives the worst scores on lower levels. This is most likely because the color it uses as average color for a particular direction was chosen by the author. It was specifically chosen to not represent the average color for a specific side but rather the "desired" color seen in that direction. This causes a strong color shift on the lower mip levels leading to the lower scores.

All box projection down-sampling methods outperform spherical mapping, with the only exception being color based down-sampling on the lowest mip levels.

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.7\textwidth]{browserLPIPSFrontTopLeftViewDep}}%
\caption{Comparison results of the different down-sampling techniques for view-dependent box projection using the LPIPS metric of Bowser from the top left.}\label{fig:browserLPIPSFrontTopLeftViewDep}
\end{figure}


\section{Positive Results} \label{section:positiveResults}
In general it can be said, that view-dependent mipmapping as demonstrated in this thesis does not provide satisfactory results for single objects. Especially close-up to the object, when the higher mip levels are shown, a lot of projection errors are visible. This observation changes when large clusters of objects are observed. When viewing things like moss covered rocks or large patches of mushrooms from far away, they take up enough space on the screen that the discolourations present in conventional mipmap generation techniques can be seen clearly. In these cases view-dependent mipmapping can help prevent these errors. This can be seen in figure \ref{fig:mushroomField}.

\begin{figure}[h]
\makebox[\textwidth][c]{
\begin{tabular}{cc}
\subfloat[Box]{\includegraphics[width=0.45\textwidth]{mushroom/frontTop/mip6/0mushroomBox}}&
\subfloat[Kaiser]{\includegraphics[width=0.45\textwidth]{mushroom/frontTop/mip6/1mushroomKaiser}}
\end{tabular}
}
\caption{Box and Kaiser mipmapping on mip level six. Only slight differences are visible. In this example SSIM gives Box a score of $0.11403335$ and Kaiser of $0.11403336$. LPIPS rates Box with $0.804394$ and Kaiser with $0.804397$. Differences can be seen at the tip of the cap, at its border, or on/ around the big white spot on the right side.}
\label{fig:mushroomMip6BoxKaiser}
\end{figure}


\begin{figure}[h]
\makebox[\textwidth][c]{
\begin{tabular}{ccc}
\subfloat[Ground truth]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip0/0browserBox}} &
\subfloat[Box]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/0browserBox}} &
\subfloat[Kaiser]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/1browserKaiser}} \\
\subfloat[Perceptual]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/2browserPerceptual}} &
\subfloat[Lanczos]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/3browserLanczos}} &
\subfloat[Parametrization aware]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/5browserParamAware}} \\
\subfloat[LPIPS]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/6browserLPIPs}}&
\subfloat[View-dependent box]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/7BowserViewDep}} &
\subfloat[View-dependent spherical]{\includegraphics[width=0.4\textwidth]{browser/frontTop/mip6/8BowserViewDepSpherical}}
\end{tabular}
}
\caption{Results of the different techniques on mip level 6. Image d (Perceptual) clearly shows severe black discolorations. Version g (LPIPS) shows a red-shift of the entire texture. In h (Viewdependent box) mapping issues can be seen on the feet. Overall Lanczos and Parametrization aware come closest to the ground truth.\\\hspace{\textwidth} Differences can be seen around the spikes on the shell and the bands around the arms.}
\label{fig:browserMip6Comparison}
\end{figure}



\begin{figure}[h]
\centering
\makebox[\textwidth][c]{
\begin{tabular}{ccc}
\subfloat[Bowser]{\includegraphics[width=0.3\textwidth]{browserColorBox}} &
\subfloat[Mushroom]{\includegraphics[width=0.3\textwidth]{MushroomDiffuseBox}} &
\subfloat[BlastBot]{\includegraphics[width=0.3\textwidth]{BlastBotColorBox}}
\end{tabular}
}
\caption{The base texture for Bowser, Mushroom, and BlastBot. The individual uv-islands are separated by black areas.}\label{fig:browserTextureBase}
\end{figure}


\begin{table}[h]
\centering
\makebox[\textwidth]{
\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c|c|} 
\hline
Method & Mip 0 & Mip 1 & Mip 2 & Mip 3 & Mip 4 & Mip 5 & Mip 6 & Mip 7 & Mip 8 & Mip 9\\
\hline
Box & 0.000 & 0.012 & 0.022 & 0.030 & 0.037 & 0.049 & 0.068 & 0.095 & 0.122 & 0.133\\
Kaiser  & 0.000 & 0.012 & 0.022 & 0.030 & 0.037 & 0.049 & 0.068 & 0.095 & 0.122 & 0.133\\
Perceptual & 0.002 & 0.011 & 0.021 & 0.030 & 0.043 & 0.062 & 0.097 & 0.168 & 0.188 & 0.238\\
Lanczos & 0.002 & 0.007 & 0.015 & 0.021 & 0.028 & 0.038 & 0.057 & 0.088 & 0.107 & 0.176\\
ParamAware & 0.002 & 0.008 & 0.014 & 0.021 & 0.027 & 0.034 & 0.048 & 0.068 & 0.092 & 0.118\\
LPIPS & 0.002 & 0.010 & 0.021 & 0.031 & 0.044 & 0.070 & 0.104 & 0.148 & 0.208 & 0.237\\
VDBox & 0.036 & 0.039 & 0.046 & 0.055 & 0.063 & 0.071 & 0.088 & 0.109 & 0.128 & 0.134\\
VDSpherical & 0.110 & 0.104 & 0.099 & 0.092 & 0.089 & 0.098 & 0.111 & 0.125 & 0.132 & 0.134\\
\hline
\end{tabular}}
\caption{SSIM metric results for Bowser from the top left front (the same as in figure \ref{fig:browserLPIPSFrontTop}). Box and Kaiser down-sampling only differ in the eighth decimal place.}
\label{table:SSIMData}
\end{table}

\begin{table}[h]
\centering
\makebox[\textwidth]{
\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c|c|} 
\hline
Method & Mip 0 & Mip 1 & Mip 2 & Mip 3 & Mip 4 & Mip 5 & Mip 6 & Mip 7 & Mip 8 & Mip 9\\
\hline
Box & 0.000 & 0.009 & 0.022 & 0.030 & 0.041 & 0.057 & 0.081 & 0.108 & 0.134 & 0.151\\
Kaiser  & 0.000 & 0.009 & 0.022 & 0.030 & 0.041 & 0.057 & 0.081 & 0.108 & 0.134 & 0.151\\
Perceptual & 0.001 & 0.007 & 0.021 & 0.032 & 0.049 & 0.068 & 0.108 & 0.155 & 0.183 & 0.216\\
Lanczos & 0.001 & 0.004 & 0.018 & 0.027 & 0.035 & 0.046 & 0.071 & 0.097 & 0.117 & 0.163\\
ParamAware & 0.001 & 0.012 & 0.023 & 0.029 & 0.035 & 0.042 & 0.056 & 0.078 & 0.103 & 0.130\\
LPIPS & 0.001 & 0.006 & 0.019 & 0.032 & 0.047 & 0.066 & 0.088 & 0.128 & 0.173 & 0.197\\
VDBox & 0.058 & 0.064 & 0.073 & 0.082 & 0.089 & 0.099 & 0.119 & 0.136 & 0.149 & 0.156\\
VDSpherical & 0.118 & 0.117 & 0.116 & 0.113 & 0.113 & 0.119 & 0.132 & 0.145 & 0.152 & 0.154\\
\hline
\end{tabular}}
\caption{LPIPS metric results for Bowser from the top left front (the same as in figure \ref{fig:browserLPIPSFrontTop}). Box and Kaiser down-sampling only differ in the eighth decimal place.}
\label{table:LPIPSData}
\end{table}


\begin{figure}
\makebox[\textwidth][c]{

\begin{tabular}{ccc}
\subfloat[Ground truth]{\includegraphics[width=0.4\textwidth]{mushroomGridBoxMip0}} &
\subfloat[Box mipmapping]{\includegraphics[width=0.4\textwidth]{mushroomGridBoxMip9}} &
\subfloat[View-dependent Box]{\includegraphics[width=0.4\textwidth]{mushroomGridViewDepMip9}}
\end{tabular}
}
\caption{All three images show a grid of mushrooms from the top. The left image has mipmapping disabled. The middle image uses box mipmapping. The one on the right uses view-dependent box projection. Both images with mipmapping show mip level 9. In this example the color degradation caused by a simple box filter is clearly visible in the middle image, whereas the view-dependent version reproduces the original colors almost perfectly.}
\label{fig:mushroomField}
\end{figure}


\begin{figure}
\makebox[\textwidth][c]{
\begin{tabular}{cc}
\subfloat[Box filter]{\includegraphics[width=0.5\textwidth]{browserGridBox}} &
\subfloat[View-dependent box projection]{\includegraphics[width=0.5\textwidth]{browserGridViewDep}}
\end{tabular}
}
\caption{Both images show a herd of wild Bowsers. The left image uses unity standard box mipmaps, the right uses view-dependent box projection. Notice how the right version keeps the green on Bowsers back much more intact than the left one. Both versions use the same lighting setup.}
\label{fig:browserHerd}
\end{figure}