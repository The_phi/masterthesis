#include "pch.h"

#include "Obj3D.h"

#include "Geometry.h"

Obj3D::Obj3D(std::string filename)
{
	std::ifstream ifs(filename.c_str(), std::ifstream::in);

	if (!ifs)
	{
		std::cerr << "Cannot open " << filename << std::endl;
		exit(1);

	}
	std::string line;
	while (std::getline(ifs, line))
	{
		// if it's a vertex position (v)
		if (line.substr(0, 2) == "v ")
		{
			std::istringstream v(line.substr(2));
			float x, y, z;
			v >> x;
			v >> y;
			v >> z;

			float3 vert;
			vert.x = x;
			vert.y = y;
			vert.z = z;
			m_vertices.push_back(vert);
		}

		// if it's a normal
		else if (line.substr(0, 2) == "vn")
		{
			std::istringstream v(line.substr(2));
			float x, y, z;
			v >> x;
			v >> y;
			v >> z;

			float3 normal;
			normal.x = x;
			normal.y = y;
			normal.z = z;
			m_normals.push_back(normal);
		}

		// if it's a texture coordinate
		else if (line.substr(0, 2) == "vt")
		{
			std::istringstream subLine(line.substr(2));
			float u, v;
			subLine >> u;
			subLine >> v;

			float2 texCoord;
			texCoord.x = u;
			texCoord.y = v;
			m_texCoords.push_back(texCoord);
		}

		// if it's face data (f)
		// there's an assumption here that faces are all triangles
		else if (line.substr(0, 2) == "f ")
		{
			int vert1, vert2, vert3; //to store mesh index
			int normal1, normal2, normal3; //to store normal index
			int texCoord1, texCoord2, texCoord3; //to store texture index
			const char* chh = line.c_str();
			//here it read the line start with f and store the corresponding values in the variables
			sscanf_s(chh, "f %i/%i/%i %i/%i/%i %i/%i/%i", &vert1, &texCoord1, &normal1,
				&vert2, &texCoord2, &normal2, &vert3, &texCoord3, &normal3);

			vert1--;
			vert2--;
			vert3--;
			normal1--;
			normal2--;
			normal3--;
			texCoord1--;
			texCoord2--;
			texCoord3--;

			ObjTriangle triangle;
			triangle.vertex1 = m_vertices[vert1];
			triangle.vertex2 = m_vertices[vert2];
			triangle.vertex3 = m_vertices[vert3];

			if (m_normals.size() > 0)
			{
				triangle.vertex1Normal = m_normals[normal1];
				triangle.vertex2Normal = m_normals[normal2];
				triangle.vertex3Normal = m_normals[normal3];
			}

			if (m_texCoords.size() > 0)
			{
				triangle.vertex1UV = m_texCoords[texCoord1];
				triangle.vertex2UV = m_texCoords[texCoord2];
				triangle.vertex3UV = m_texCoords[texCoord3];

			}



			m_faces.push_back(triangle);
		}

	}

	ifs.close();
	std::cout << "               Name: " << filename << std::endl;
	std::cout << "           Vertices: " << std::to_string(m_vertices.size()) << std::endl;
	std::cout << "Texture Coordinates: " << std::to_string(m_texCoords.size()) << std::endl;
	std::cout << "            Normals: " << std::to_string(m_normals.size()) << std::endl;
	std::cout << "              Faces: " << std::to_string(m_faces.size()) << std::endl << std::endl;
}