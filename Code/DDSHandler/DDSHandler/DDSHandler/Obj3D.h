#pragma once

#include <string>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <vector>

#include "Geometry.h"

class Obj3D
{
public:

	Obj3D(std::string filename);

	std::vector<ObjTriangle>& getFaces() {
		return m_faces;
	}

private:
	std::vector<float3> m_vertices;
	std::vector<float2> m_texCoords;
	std::vector<float3> m_normals;
	std::vector<ObjTriangle> m_faces;

};