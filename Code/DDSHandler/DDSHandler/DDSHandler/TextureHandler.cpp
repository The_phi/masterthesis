#include "pch.h"
#include "TextureHandler.h"

#include <iostream>
#include <algorithm> 
// for timekeeping
#include <chrono>

#include "ParamAwareFilter.h"
#include "LanczosFilter.h"
#include "HighPassFilter.h"
#include "PerceptualDownsamplingFilter.h"
#include "ViewDependantFilter.h"
#include "Obj3D.h"
#include "Geometry.h"
#include "MipCombiner.h"

// DirectX 12 specific headers.
// D3D12 extension library.
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"
using namespace DirectX;

// https://stackoverflow.com/questions/50906891/how-to-initialize-xmvector-in-directx


TextureHandler::TextureHandler()
{
}

void TextureHandler::createPerceptualDownscaledMipTexture(std::wstring _fileName, const int _mipLevels)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(_fileName);

	// generate mip chain
	const Image* baseImage = image.get()->GetImage(0, 0, 0);
	ScratchImage mipChain;
	SetupMipChain(baseImage, _mipLevels, mipChain);

	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* srcImg = mipChain.GetImage(i - 1, 0, 0);
		const Image* dstImg = mipChain.GetImage(i, 0, 0);
		PerceptualDownsamplingFilter::perceptualDownsampling(*srcImg, *dstImg, 2, 2);
	}


	std::wstring filePath = L"images/" + _fileName + L"Perceptual.dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::createViewDependantNormalMipImage(std::wstring _fileName, const int _mipLevels)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(_fileName);
	std::unique_ptr<DirectX::ScratchImage> imageNormal = loadTexture(_fileName + L"Normal");

	ScratchImage* baseScrImage = image.get();
	const Image* baseImage = baseScrImage->GetImage(0, 0, 0);
	ScratchImage* baseNormalScrImage = imageNormal.get();
	const Image* baseNormalImage = baseNormalScrImage->GetImage(0, 0, 0);

	// generate mip chain
	ScratchImage mipChain;
	SetupMipChain(baseImage, _mipLevels, mipChain);

	int scalingFactor = 2;
	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* downScaled = mipChain.GetImage(i, 0, 0);
		ViewDependantFilter::ViewDependantNormalDownsampling(*baseImage, *baseNormalImage, *downScaled, scalingFactor);
		scalingFactor *= 2;
	}


	std::wstring filePath = L"images/" + _fileName + L"ViewDepNormal.dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::createViewDepBoxMipmappedImage(std::wstring _fileName, const int _mipLevels)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(_fileName);

	// generate mip chain
	const Image* baseImage = image.get()->GetImage(0, 0, 0);
	ScratchImage mipChain;
	SetupMipChain(baseImage, _mipLevels, mipChain);

	int scalingFactor = 2;
	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* downScaled = mipChain.GetImage(i, 0, 0);
		ViewDependantFilter::ViewDepBoxDownsampling(*baseImage, *downScaled, scalingFactor);
		scalingFactor *= 2;
	}


	std::wstring filePath = L"images/" + _fileName + L"ViewDepBox.dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::createViewDepColorMipmappedImage(std::wstring _fileName, const int _mipLevels, const float3 _avgColor)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(_fileName);

	// generate mip chain
	const Image* baseImage = image.get()->GetImage(0, 0, 0);
	ScratchImage mipChain;
	SetupMipChain(baseImage, _mipLevels, mipChain);

	int scalingFactor = 2;
	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* srcImg = mipChain.GetImage(i - 1, 0, 0);
		const Image* downScaled = mipChain.GetImage(i, 0, 0);
		ViewDependantFilter::ViewDepColorDownsampling(*srcImg, *downScaled, scalingFactor, _avgColor);
		//scalingFactor *= 2;
	}


	std::wstring filePath = L"images/" + _fileName + L"ViewDepColor.dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::createParamAwareMipTexture(std::wstring _textureFileName, std::string _objFileName, const int _mipLevels)
{
	std::unique_ptr<DirectX::ScratchImage> origScratchImg = loadTexture(_textureFileName);
	const Image* pOrigImg = &(origScratchImg.get()->GetImages()[0]);


	Obj3D obj(_objFileName);
	ParamAwareFilter paramAwareFilter;
	paramAwareFilter.computeTexelWeights(obj, pOrigImg->width, pOrigImg->height);

	createParameterAwareMipmappedImage(_textureFileName, _mipLevels, &paramAwareFilter);
}

void TextureHandler::createParameterAwareMipmappedImage(std::wstring fileName, const int mipLevels, ParamAwareFilter* _pParamAwareFilter)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(fileName);

	// generate mip chain
	const Image* baseImage = image.get()->GetImage(0, 0, 0);
	ScratchImage mipChain;
	SetupMipChain(baseImage, mipLevels, mipChain);

	int scalingFactor = 2;
	for (int i = 1; i < mipLevels; ++i)
	{
		const Image* downScaled = mipChain.GetImage(i, 0, 0);
		_pParamAwareFilter->calculateDownScaledImage(scalingFactor, *baseImage, *downScaled);
		scalingFactor *= 2;
	}


	std::wstring filePath = L"images/" + fileName + L"ParamAware.dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::createLanczosFilterMipTexture(std::wstring _fileName, const int _mipLevels)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(_fileName);

	// generate mip chain
	const Image* baseImage = image.get()->GetImage(0, 0, 0);
	ScratchImage mipChain;
	SetupMipChain(baseImage, _mipLevels, mipChain);

	int scalingFactor = 2;
	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* downScaled = mipChain.GetImage(i, 0, 0);
		LanczosFilter::lanczosDownsampling(*baseImage, 5, *downScaled, scalingFactor);
		scalingFactor *= 2;
	}
	
	std::wstring filePath = L"images/" + _fileName + L"Lanczos.dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::combineImagesInFolderToDDs(std::string _folderPath, std::wstring _ddsFileName)
{
	MipCombiner combiner;

	// generate mip chain
	TexMetadata mdata = {};
	mdata.width = 1024;
	mdata.height = 1024;
	mdata.dimension = TEX_DIMENSION_TEXTURE2D;
	mdata.depth = mdata.arraySize = 1;
	mdata.mipLevels = combiner.countFilesInFolder(_folderPath);
	mdata.format = DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM;
	ScratchImage mipChain;
	mipChain.Initialize(mdata);

	combiner.combineMipMapsInFolder(mipChain, _folderPath);

	std::wstring filePath = L"images/" + _ddsFileName + L".dds";
	saveTexture(mipChain, filePath);
}

void TextureHandler::createHighPassFilterMipTexture(std::wstring _fileName, const int _mipLevels)
{
	std::unique_ptr<DirectX::ScratchImage> image = loadTexture(_fileName);

	ScratchImage* baseScrImage = image.get();
	const Image* baseImage = baseScrImage->GetImage(0, 0, 0);


	// generate mip chain
	TexMetadata mdata = {};
	mdata.width = baseImage->width;
	mdata.height = baseImage->height;
	mdata.dimension = TEX_DIMENSION_TEXTURE2D;
	mdata.depth = mdata.arraySize = 1;
	mdata.mipLevels = _mipLevels;
	mdata.format = baseImage->format;

	ScratchImage highPassMipChain;
	ThrowIfFailed(Setup2DMips(baseImage, 1, mdata, highPassMipChain));
	int scalingFactor = 2;
	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* downScaled = highPassMipChain.GetImage(i, 0, 0);
		HighPassFilter::highPassDownsampling(*baseImage, 5, *downScaled, scalingFactor);
		scalingFactor *= 2;
	}

	std::wstring filePath = L"images/" + _fileName + L"HighPass.dds";
	saveTexture(highPassMipChain, filePath);

	ScratchImage lanczosMipChain;
	ThrowIfFailed(Setup2DMips(baseImage, 1, mdata, lanczosMipChain));
	scalingFactor = 2;
	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* downScaled = lanczosMipChain.GetImage(i, 0, 0);
		LanczosFilter::lanczosDownsampling(*baseImage, 5, *downScaled, scalingFactor);
		scalingFactor *= 2;
	}

	for (int i = 1; i < _mipLevels; ++i)
	{
		const Image* lanczos = lanczosMipChain.GetImage(i, 0, 0);
		uint8_t* laczosData = lanczos->pixels;
		const Image* highPass = highPassMipChain.GetImage(i, 0, 0);
		uint8_t* highPassData = highPass->pixels;

		int dimX = lanczos->width;
		int dimY = lanczos->height;
		const size_t srcRowPitch = lanczos->rowPitch;
		const size_t srcPixelSize = srcRowPitch / dimX;

		for (int y = 0; y < dimY; ++y)
		{
			for (int x = 0; x < dimX; ++x)
			{
				int dstPixelIndex = y * srcRowPitch + x * srcPixelSize;

				int red = highPassData[dstPixelIndex + 2] + laczosData[dstPixelIndex + 2];
				int green = highPassData[dstPixelIndex + 1] + laczosData[dstPixelIndex + 1];
				int blue = highPassData[dstPixelIndex + 0] + laczosData[dstPixelIndex + 0];

				highPassData[dstPixelIndex + 2] = (uint8_t) std::min<int>(red / 2, 255);
				highPassData[dstPixelIndex + 1] = (uint8_t)std::min<int>(green / 2, 255);
				highPassData[dstPixelIndex + 0] = (uint8_t)std::min<int>(blue / 2, 255);
				highPassData[dstPixelIndex + 3] = 255;
			}
		}

	}

	filePath = L"images/" + _fileName + L"HighPassCombined.dds";
	saveTexture(highPassMipChain, filePath);





	/*TexMetadata mdata = {};
	mdata.width = baseImage->width;
	mdata.height = baseImage->height;
	mdata.dimension = TEX_DIMENSION_TEXTURE2D;
	mdata.depth = mdata.arraySize = 1;
	mdata.mipLevels = 1;
	mdata.format = baseImage->format;
	ScratchImage highPassImage;
	highPassImage.Initialize(mdata);

	const Image* downScaled = highPassImage.GetImage(0, 0, 0);
	HighPassFilter::highPassDownsampling(*baseImage, 5, *downScaled, 1);
	std::wstring filePath = L"images/" + _fileName + L"HighPassBig.dds";
	saveTexture(highPassImage, filePath);*/
}

std::unique_ptr<DirectX::ScratchImage> TextureHandler::loadTexture(std::wstring _fileName)
{
	std::wstring filePath = L"images/" + _fileName + L".dds";

	TexMetadata info;
	std::unique_ptr<DirectX::ScratchImage> image = std::make_unique<ScratchImage>();
	ThrowIfFailed(
		LoadFromDDSFile(filePath.c_str(), DDS_FLAGS_NONE, &info, *image));


	return image;
}

void TextureHandler::saveTexture(const std::unique_ptr<DirectX::ScratchImage>& _image, std::wstring _fileName)
{
	saveTexture(*_image.get(), _fileName);
}

void TextureHandler::saveTexture(const DirectX::ScratchImage& _image, std::wstring _fileName)
{
	const Image* pImg = _image.GetImages();
	assert(pImg);
	size_t nimg = _image.GetImageCount();
	assert(nimg > 0);

	ThrowIfFailed(
		SaveToDDSFile(pImg, nimg, _image.GetMetadata(),
			DDS_FLAGS_NONE, _fileName.c_str()));
}

void TextureHandler::saveTexture(const Image* _image, std::wstring _fileName)
{
	ScratchImage scImg;
	scImg.InitializeFromImage(*_image);
	saveTexture(scImg, _fileName);
}

void TextureHandler::saveMipMapsToFolder(std::wstring _fileName, std::wstring _folderName, ScratchImage& _mipChain)
{
	TexMetadata mipChainMetaData = _mipChain.GetMetadata();
	for (int i = 0; i < mipChainMetaData.mipLevels; i++)
	{
		const Image* mipLevel = _mipChain.GetImage(i, 0, 0);
		std::wstring fileName = L"images/" + _folderName + L"/" + _fileName + std::to_wstring(i) +  L".dds";
		saveTexture(mipLevel, fileName);
	}
}

HRESULT TextureHandler::Setup2DMips(
	_In_reads_(nimages) const Image* baseImages,
	_In_ size_t nimages,
	_In_ const TexMetadata& mdata,
	_Out_ ScratchImage& mipChain) noexcept
{
	if (!baseImages || !nimages)
		return E_INVALIDARG;

	assert(mdata.mipLevels > 1);
	assert(mdata.arraySize == nimages);
	assert(mdata.depth == 1 && mdata.dimension != TEX_DIMENSION_TEXTURE3D);
	assert(mdata.width == baseImages[0].width);
	assert(mdata.height == baseImages[0].height);
	assert(mdata.format == baseImages[0].format);

	HRESULT hr = mipChain.Initialize(mdata);
	if (FAILED(hr))
		return hr;

	// Copy base image(s) to top of mip chain
	for (size_t item = 0; item < nimages; ++item)
	{
		const Image& src = baseImages[item];

		const Image *dest = mipChain.GetImage(0, item, 0);
		if (!dest)
		{
			mipChain.Release();
			return E_POINTER;
		}

		assert(src.format == dest->format);

		uint8_t* pDest = dest->pixels;
		if (!pDest)
		{
			mipChain.Release();
			return E_POINTER;
		}

		const uint8_t *pSrc = src.pixels;
		size_t rowPitch = src.rowPitch;
		for (size_t h = 0; h < mdata.height; ++h)
		{
			size_t msize = std::min<size_t>(dest->rowPitch, rowPitch);
			memcpy_s(pDest, dest->rowPitch, pSrc, msize);
			pSrc += rowPitch;
			pDest += dest->rowPitch;
		}
	}

	return S_OK;
}

void TextureHandler::SetupMipChain(const Image* _pBaseImage, const int _mipLevels, ScratchImage& _mipChain)
{
	// generate mip chain
	TexMetadata mdata = {};
	mdata.width = _pBaseImage->width;
	mdata.height = _pBaseImage->height;
	mdata.dimension = TEX_DIMENSION_TEXTURE2D;
	mdata.depth = mdata.arraySize = 1;
	mdata.mipLevels = _mipLevels;
	mdata.format = _pBaseImage->format;
	ThrowIfFailed(Setup2DMips(_pBaseImage, 1, mdata, _mipChain));
}

