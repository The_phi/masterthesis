#pragma once

#include <string>

typedef unsigned long long ULONG_PTR;

namespace DirectX
{
	class ScratchImage;
}

class MipCombiner
{
public:
	MipCombiner();
	~MipCombiner();

	void combineMipMapsInFolder(DirectX::ScratchImage& _mipChain, std::string _folderPath);
	int countFilesInFolder(std::string _folderPath);

private:
	void loadMipLevel(DirectX::ScratchImage& _mipChain, int _mipLevel, std::string _filePath);
	ULONG_PTR m_gdiplusToken;
};