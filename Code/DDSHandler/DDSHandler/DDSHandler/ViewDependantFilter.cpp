#include "pch.h"
#include "ViewDependantFilter.h"
#include "Geometry.h"
#include <math.h>

void ViewDependantFilter::ViewDependantNormalKernel(const Image& _pSrcImage, const Image& _pSrcNormalImage,
	int _fX, int _fY, const Image& _pDstImage, const int _scalingFactor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;
	const uint8_t* pSrcData = _pSrcImage.pixels;
	const uint8_t* pSrcNormalData = _pSrcNormalImage.pixels;
	const size_t srcRowPitch = _pSrcImage.rowPitch;
	const size_t srcPixelSize = srcRowPitch / dimX;


	int usedPixels = 0;
	float3 averageDirection;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int iX = _fX + x;
			int iY = _fY + y;
			// if outside bounds
			if (iX >= dimX || iY >= dimY)
			{
				continue;
			}

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t alphaTexel = pSrcNormalData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			uint8_t redNormalTexel = pSrcNormalData[srcPixelIndex + 2];
			uint8_t greenNormalTexel = pSrcNormalData[srcPixelIndex + 1];
			uint8_t blueNormalTexel = pSrcNormalData[srcPixelIndex + 0];

			float3 normalVec(redNormalTexel, greenNormalTexel, blueNormalTexel);
			averageDirection = averageDirection + normalVec;
			usedPixels++;
		}
	}
	averageDirection = averageDirection / usedPixels;

	float totalVecDistance = 0;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int iX = _fX + x;
			int iY = _fY + y;
			// if outside bounds
			if (iX >= dimX || iY >= dimY)
			{
				continue;
			}

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t alphaTexel = pSrcNormalData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			uint8_t redNormalTexel = pSrcNormalData[srcPixelIndex + 2];
			uint8_t greenNormalTexel = pSrcNormalData[srcPixelIndex + 1];
			uint8_t blueNormalTexel = pSrcNormalData[srcPixelIndex + 0];

			float3 normalVec(redNormalTexel, greenNormalTexel, blueNormalTexel);

			totalVecDistance += normalVec.norm(averageDirection);
		}
	}

	float redCol = 0;
	float greenCol = 0;
	float blueCol = 0;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int iX = _fX + x;
			int iY = _fY + y;
			// if outside bounds
			if (iX >= dimX || iY >= dimY)
			{
				continue;
			}

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t alphaTexel = pSrcNormalData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			uint8_t redNormalTexel = pSrcNormalData[srcPixelIndex + 2];
			uint8_t greenNormalTexel = pSrcNormalData[srcPixelIndex + 1];
			uint8_t blueNormalTexel = pSrcNormalData[srcPixelIndex + 0];

			float3 normalVec(redNormalTexel, greenNormalTexel, blueNormalTexel);

			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];

			float factor = normalVec.norm(averageDirection);
			// this is necessary if all normal values are equal within the kernel
			if (totalVecDistance == 0)
			{
				factor = 1.0f / usedPixels;
			}
			else
			{
				factor = factor / totalVecDistance;
			}
			redCol += redTexel * factor;
			greenCol += greenTexel * factor;
			blueCol += blueTexel * factor;
		}
	}


	// Write our weighted sum to our output
	uint8_t* pDstdata = _pDstImage.pixels;
	int dstPixelIndex = (_fY / _scalingFactor) * _pDstImage.rowPitch + (_fX / _scalingFactor) * srcPixelSize;
	pDstdata[dstPixelIndex + 2] = (uint8_t)redCol;
	pDstdata[dstPixelIndex + 1] = (uint8_t)greenCol;
	pDstdata[dstPixelIndex + 0] = (uint8_t)blueCol;
	pDstdata[dstPixelIndex + 3] = 255;
}

void ViewDependantFilter::ViewDepBoxKernel(const Image& _pSrcImage,
	int _x, int _y, const Image& _pDstImage, const int _scalingFactor)
{
	float fSampleCount = 0;

	//
	// Scan the kernel space adding up the bicubic weights and pixel values
	//

	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;
	const uint8_t* pSrcData = _pSrcImage.pixels;
	const size_t srcRowPitch = _pSrcImage.rowPitch;
	const size_t srcPixelSize = srcRowPitch / dimX;

	int redCol = 0;
	int greenCol = 0;
	int blueCol = 0;

	int pixelCount = 0;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int deltaX = _x + x;
			int deltaY = _y + y;

			// if outside bounds
			if (deltaX < 0 || deltaY < 0 || deltaX >= dimX || deltaY >= dimY)
			{
				continue;
			}


			int srcPixelIndex = deltaY * srcRowPitch + deltaX * srcPixelSize;
			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];
			uint8_t alphaTexel = pSrcData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			++pixelCount;
			redCol += redTexel;
			greenCol += greenTexel;
			blueCol += blueTexel;
		}
	}

	if (pixelCount > 0)
	{
		redCol /= pixelCount;
		greenCol /= pixelCount;
		blueCol /= pixelCount;
	}

	//
	// Write our weighted sum to our output
	//

	uint8_t* pDstdata = _pDstImage.pixels;
	int dstPixelIndex = (_y / _scalingFactor) * _pDstImage.rowPitch + (_x / _scalingFactor) * srcPixelSize;
	pDstdata[dstPixelIndex + 2] = (uint8_t)redCol;
	pDstdata[dstPixelIndex + 1] = (uint8_t)greenCol;
	pDstdata[dstPixelIndex + 0] = (uint8_t)blueCol;
	pDstdata[dstPixelIndex + 3] = 255;
}

void ViewDependantFilter::ViewDepColorKernel(const Image& _pSrcImage, const float3 _avgColor,
	int _x, int _y, const Image& _pDstImage, const int _scalingFactor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;
	const uint8_t* pSrcData = _pSrcImage.pixels;
	const size_t srcRowPitch = _pSrcImage.rowPitch;
	const size_t srcPixelSize = srcRowPitch / dimX;

	float totalColorDistance = 0;
	int usedPixels = 0;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int iX = _x + x;
			int iY = _y + y;
			// if outside bounds
			if (iX >= dimX || iY >= dimY)
			{
				continue;
			}

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t alphaTexel = pSrcData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];

			float3 colorVec(redTexel, greenTexel, blueTexel);
			totalColorDistance += colorVec.norm(_avgColor);
			usedPixels++;
		}
	}

	float totalNegColorDistance = 0;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int iX = _x + x;
			int iY = _y + y;
			// if outside bounds
			if (iX >= dimX || iY >= dimY)
			{
				continue;
			}

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t alphaTexel = pSrcData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];

			float3 colorVec(redTexel, greenTexel, blueTexel);
			totalNegColorDistance += totalColorDistance - colorVec.norm(_avgColor);
		}
	}

	float redCol = 0;
	float greenCol = 0;
	float blueCol = 0;
	for (int y = 0; y < _scalingFactor; ++y)
	{
		for (int x = 0; x < _scalingFactor; ++x)
		{
			int iX = _x + x;
			int iY = _y + y;
			// if outside bounds
			if (iX >= dimX || iY >= dimY)
			{
				continue;
			}

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t alphaTexel = pSrcData[srcPixelIndex + 3];
			if (alphaTexel == 0)
			{
				continue;
			}

			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];

			float3 colorVec(redTexel, greenTexel, blueTexel);
			float factor = colorVec.norm(_avgColor);
			factor = totalColorDistance - factor;
			// this is necessary if all color values are equal within the kernel
			if (totalNegColorDistance == 0)
			{
				factor = 1.0f / usedPixels;
			}
			else
			{
				factor = factor / totalNegColorDistance;
			}
			redCol += redTexel * factor;
			greenCol += greenTexel * factor;
			blueCol += blueTexel * factor;
		}
	}

	if (redCol < 0)
		redCol = 0;
	if (greenCol < 0)
		greenCol = 0;
	if (blueCol < 0)
		blueCol = 0;
	if (redCol > 255)
		redCol = 255;
	if (greenCol > 255)
		greenCol = 255;
	if (blueCol > 255)
		blueCol = 255;


	// Write our weighted sum to our output
	uint8_t* pDstdata = _pDstImage.pixels;
	int dstPixelIndex = (_y / _scalingFactor) * _pDstImage.rowPitch + (_x / _scalingFactor) * srcPixelSize;
	pDstdata[dstPixelIndex + 2] = (uint8_t)redCol;
	pDstdata[dstPixelIndex + 1] = (uint8_t)greenCol;
	pDstdata[dstPixelIndex + 0] = (uint8_t)blueCol;
	pDstdata[dstPixelIndex + 3] = 255;
}

void ViewDependantFilter::ViewDependantNormalDownsampling(const Image& _pSrcImage, const Image& _pSrcNormalImage,
	const Image& _pDstImage, const int _scalingFactor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;

	///////////////////////
	//TODO: check if src image and src normal are same dimensions
	///////////////////////

	for (int y = 0; y < dimY; y = y + _scalingFactor)
	{
		for (int x = 0; x < dimX; x = x +_scalingFactor)
		{
			ViewDependantNormalKernel(_pSrcImage, _pSrcNormalImage, x, y, _pDstImage, _scalingFactor);
		}

	}
}

void ViewDependantFilter::ViewDepBoxDownsampling(const Image& _pSrcImage,
	const Image& _pDstImage, const int _scalingFactor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;

	for (int y = 0; y < dimY / _scalingFactor; ++y)
	{
		int yDif = y * _scalingFactor;
		for (int x = 0; x < dimX / _scalingFactor; ++x)
		{
			int xDif = x * _scalingFactor;
			ViewDepBoxKernel(_pSrcImage, xDif, yDif, _pDstImage, _scalingFactor);
		}

	}
}

void ViewDependantFilter::ViewDepColorDownsampling(const Image& _pSrcImage,
	const Image& _pDstImage, const int _scalingFactor, const float3 _avgColor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;

	for (int y = 0; y < dimY / _scalingFactor; ++y)
	{
		int yDif = y * _scalingFactor;
		for (int x = 0; x < dimX / _scalingFactor; ++x)
		{
			int xDif = x * _scalingFactor;
			ViewDepColorKernel(_pSrcImage, _avgColor, xDif, yDif, _pDstImage, _scalingFactor);
		}

	}
}