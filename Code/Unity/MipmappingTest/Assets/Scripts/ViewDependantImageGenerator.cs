﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ViewDependantImageGenerator : MonoBehaviour
{
    private Renderer rend;
    private Camera m_camera;
    private RenderTexture rendTex;
    private Transform camTrans;

    public GameObject m_targetGO;
    private Transform m_transTarget;

    public string m_namePrefix = "";


    void Start()
    {
        rend = m_targetGO.GetComponent<Renderer>();
        m_transTarget = m_targetGO.transform;
        m_camera = GetComponent<Camera>();
        camTrans = this.transform;
    }

    private int counter = -10;

    private void Update()
    {
        // setup camera front
        if(counter == 0)
        {
            updateRenderTextureAndCamera(true, false, new Vector3(0, 0, 2), new Vector3(0, 180, 0));
        }
        // render front
        if(counter == 1)
        {
            saveRenderTexture("Front");
        }

        // setup camera back
        if (counter == 2)
        {
            updateRenderTextureAndCamera(true, false, new Vector3(0, 0, -2), new Vector3(0, 0, 0));
        }
        // render back
        if (counter == 3)
        {
            saveRenderTexture("Back");
        }

        // setup camera left
        if (counter == 4)
        {
            updateRenderTextureAndCamera(false, true, new Vector3(2, 0, 0), new Vector3(0, 270, 0));
        }
        // render left
        if (counter == 5)
        {
            saveRenderTexture("Left");
        }


        // setup camera right
        if (counter == 6)
        {
            updateRenderTextureAndCamera(false, true, new Vector3(-2, 0, 0), new Vector3(0, 90, 0));
        }
        // render right
        if (counter == 7)
        {
            saveRenderTexture("Right");
        }

        // setup camera top
        if (counter == 8)
        {
            updateRenderTextureAndCamera(false, false, new Vector3(0, 2, 0), new Vector3(90, 0, 0));
        }
        // render top
        if (counter == 9)
        {
            saveRenderTexture("Top");
        }

        // setup camera bottom
        if (counter == 10)
        {
            updateRenderTextureAndCamera(false, false, new Vector3(0, -2, 0), new Vector3(-90, 0, 0));
        }
        // render bottom
        if (counter == 11)
        {
            saveRenderTexture("Bottom");
        }


        counter++;
    }

    void updateRenderTextureAndCamera(bool front, bool left, Vector3 camOffset, Vector3 camRot)
    {
        Vector3 center = rend.bounds.center;
        Vector3 scale = rend.bounds.size;

        float width = 10;
        float height = 10;

        if(front)
        {
            width = scale.x;
            height = scale.y;
        }
        else if(left)
        {
            width = scale.z;
            height = scale.y;
        }
        else
        {
            width = scale.x;
            height = scale.z;
        }

        m_camera.orthographicSize = height / 2;
        float aspectRatio = width / height;
        m_camera.aspect = aspectRatio;

        int screenWidth = 2048;
        int screenHeight = (int)(screenWidth / aspectRatio);
        rendTex = new RenderTexture(screenWidth, screenHeight, 16, RenderTextureFormat.ARGB32);
        m_camera.targetTexture = rendTex;

        camTrans.position = center + camOffset;
        camTrans.rotation = Quaternion.Euler(camRot.x, camRot.y, camRot.z);
    }

    private void saveRenderTexture(string fileName)
    {
        Texture2D texture = new Texture2D(rendTex.width, rendTex.height, TextureFormat.ARGB32, false, false);
        RenderTexture.active = rendTex;
        texture.ReadPixels(new Rect(0, 0, rendTex.width, rendTex.height), 0, 0);
        texture.Apply();

        byte[] bytes = texture.EncodeToPNG();
        UnityEngine.Object.Destroy(texture);

        string filePath = "screenGrabs/" + m_namePrefix + fileName + ".png";

        System.IO.File.WriteAllBytes(filePath, bytes);
    }
}
