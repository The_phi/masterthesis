import os
import os.path
import glob
import argparse
import imutils
import cv2
from skimage.metrics import structural_similarity
import csv



# needed to load perceptal nn
import sys
sys.path.insert(0, r"C:/Users/Cortana/Desktop/UniZeug/masterThesis/Code/python/PerceptualSimilarity")
from util import util
import models
import torch



def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

# 2. Construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--path", required=True, help="path to base folder")
ap.add_argument("-s", "--ssim", required=False, help="use ssim", default=True, type=str2bool)
ap.add_argument("-l", "--lpips", required=False, help="use lpips", default=True, type=str2bool)
args = vars(ap.parse_args())

directory = args["path"]
useSsim = args["ssim"]
useLPIPs = args["lpips"]


model = 1
if useLPIPs:
    # load nn
    model = models.PerceptualLoss(model='net-lin', net='alex', use_gpu=True, gpu_ids=[0])
    

# iterate main folder
for folderName in os.listdir(directory):
    # iterate mip folders
    folderPath = directory + "/" + folderName
    # count methods
    path, dirs, files = next(os.walk(folderPath+ "/mip1"))
    methodCount = len(files)
    # count mip levels
    path, dirs, files = next(os.walk(folderPath))
    mipLevels = len(dirs)
    ssimValues = [[] for y in range(mipLevels)] 
    LPIPsValues = [[] for y in range(mipLevels)] 


    # iterate all downscaling methods
    for method in range(0, methodCount):

        # get correct ground truth
        groundTruth = 0
        path = folderPath + "/mip0/"
        # use box as groundtruth
        for mipFile in os.listdir(path):
            mipFilePath = path  + mipFile
            # find box method image
            if "Box" in mipFile:
                groundTruth = cv2.imread(mipFilePath)
                break
        if not hasattr(groundTruth, "__len__"):
            #use method mip level 0 if box not found
            print("using method mip0 as ground truth (box mipmapping not found)")
            for mipFile in os.listdir(path):
                mipFilePath = path  + mipFile
                # find correct method
                if mipFile[0] == "" + str(method):
                    groundTruth = cv2.imread(mipFilePath)
                    break
        baseWidth, baseHeight, _ = groundTruth.shape

        if(useLPIPs):
            lpipsGroundTruth = util.im2tensor(groundTruth)

        for mipFolder in os.listdir(folderPath):
            mipFolderPath = folderPath + "/" + mipFolder 
            # iterate all files in folder
            for mipFile in os.listdir(mipFolderPath):
                mipFilePath = mipFolderPath + "/" + mipFile
                # find correct method
                if mipFile[0] == "" + str(method):
                    # load image
                    imageB = cv2.imread(mipFilePath)
                    imageBScaled = cv2.resize(imageB, dsize=(baseHeight, baseWidth), interpolation=cv2.INTER_CUBIC)

                    # compute values
                    if useSsim:
                        ssimScore = structural_similarity(groundTruth, imageBScaled, multichannel=True)
                        # store values
                        ssimValues[method].append(ssimScore)


                    if useLPIPs:
                        lpipsImageB = util.im2tensor(imageBScaled)
                        lpipsScore = model.forward(lpipsGroundTruth, lpipsImageB)
                        # store values
                        LPIPsValues[method].append(lpipsScore.item())
                        del lpipsImageB
                        torch.cuda.empty_cache()


                    break
    
    # write values to csv file
    if useSsim:
        with open(folderName + "SSIMResult.csv","w+") as ssim_csv:
            csvWriter = csv.writer(ssim_csv, delimiter=',')
            csvWriter.writerows(ssimValues)

    if useLPIPs:
        del lpipsGroundTruth
        torch.cuda.empty_cache()

        with open(folderName + "LPIPsResult.csv","w+") as lpips_csv:
            csvWriter = csv.writer(lpips_csv, delimiter=',')
            csvWriter.writerows(LPIPsValues)

    print(folderName + " folder done")