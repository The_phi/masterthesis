#pragma once

#include <cmath>
// D3D12 extension library.
#include "DirectXTex/d3dx12.h"
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"

using namespace DirectX;

class HighPassFilter
{
public:
	static void highPassKernel(const Image& _pSrcImage,
		int _kernelSize, int _x, int _y, const Image& _pDstImage, const int _scalingFactor);

	static void highPassDownsampling(const Image& _pSrcImage,
		int _kernelSize, const Image& _pDstImage, const int _scalingFactor);
};