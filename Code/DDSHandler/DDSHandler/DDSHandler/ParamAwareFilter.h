#pragma once

#include <Eigen/Dense>
#include <vector>
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"

#include "Geometry.h"

using namespace DirectX;

const int N = 15; // clipped (new) polygon size

class Obj3D;


struct TexelCoverage
{
	float areaTexSpace = 0;
	float areaObjSpace = 0;
	float areaTriangleTexSpace = 0;
	float areaTriangleObjSpace = 0;

	TexelCoverage(float _areaTexSpace, float _areaObjSpace,
		float _areaTriangleTexSpace, float _areaTriangleObjSpace)
	{
		areaTexSpace = _areaTexSpace;
		areaObjSpace = _areaObjSpace;
		areaTriangleTexSpace = _areaTriangleTexSpace;
		areaTriangleObjSpace = _areaTriangleObjSpace;
	}
};

class ParamAwareFilter
{
public:
	void computeTexelWeights(Obj3D& obj, int dimX, int dimY);
	void calculateDownScaledImage(const int _scalingFactor, const Image& _baseImage, const Image& _targetImage);

private:
	std::vector<float2> calculateIntersectionPoly(std::vector<float2> &rectangle, std::vector<float2> &triangle);


	float sign(float2 p1, float2 p2, float2 p3);
	bool pointInTriangle(float2 pt, float2 v1, float2 v2, float2 v3);
	bool intersection(float2 p1, float2 p2, float2 v1, float2 v2, float2& out);

	int handleTriangle(Triangle2D& _triangle, int xBiggestTexCoord, int yBiggestTexCoord, int xSmallestTexCoord, int ySmallestTexCoord, float triangleAreaObjSpace, float triangleAreaTexSpace);

	int findBiggest(float a, float b, float c);
	int findSmallest(float a, float b, float c);
	int findBiggest(float a, float b, float c, float d);
	int findSmallest(float a, float b, float c, float d);

	std::vector< std::vector< std::vector<TexelCoverage> > > m_texelCoverageMap;

	void keepCoordinatesInRange(float2& _coordinates, int _dimX, int _dimY);
};