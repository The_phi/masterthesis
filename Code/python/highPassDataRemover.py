# flips the numbers in a csv file
import os
import os.path
import argparse
from numpy import genfromtxt
import numpy as np
import csv

# 2. Construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--path", required=True, help="path to base folder")
args = vars(ap.parse_args())

directory = args["path"]


for folderName in os.listdir(directory):
    # iterate mip folders
    folderPath = directory + "/" + folderName

    for csvFile in os.listdir(folderPath):
        filePath = folderPath + "/" + csvFile
        print(filePath)
        data = genfromtxt(filePath, delimiter=',')

        data = np.delete(data, (4), axis=0)

        with open(filePath,"w+") as csvFile:
                csvWriter = csv.writer(csvFile, delimiter=',')
                csvWriter.writerows(data)
