// DDSHandler.cpp : This file contains the 'main' function. Program execution begins and ends there.

// DDSTextureLoader from https://github.com/Microsoft/DirectXTex
//

#include "pch.h"
#include <iostream>

#include "TextureHandler.h"
#include "PngToDDsConverter.h"
#include "Geometry.h"
#include <vector>

int main()
{
	std::cout << "DDS Handler is thinking!\nHuman is instructed to wait.\n";
	TextureHandler texHandler;

	std::wstring fileName = L"blastBotColor";
	std::string objFileName = "modles/blastBot.obj";
	int mipLevel = 10;

	texHandler.createPerceptualDownscaledMipTexture(fileName, mipLevel);
	std::cout << "created perceptual\n";
	//texHandler.createLanczosFilterMipTexture(fileName, mipLevel);
	//std::cout << "created lanczos\n";
	//texHandler.createHighPassFilterMipTexture(fileName, mipLevel);
	//std::cout << "created high pass\n";
	//texHandler.createParamAwareMipTexture(fileName, objFileName, mipLevel);
	//std::cout << "created param aware\n";


	/*fileName = L"blastBot/blastBotBack";
	texHandler.createViewDepBoxMipmappedImage(fileName, mipLevel);
	fileName = L"blastBot/blastBotBottom";
	texHandler.createViewDepBoxMipmappedImage(fileName, mipLevel);
	fileName = L"blastBot/blastBotFront";
	texHandler.createViewDepBoxMipmappedImage(fileName, mipLevel);
	fileName = L"blastBot/blastBotLeft";
	texHandler.createViewDepBoxMipmappedImage(fileName, mipLevel);
	fileName = L"blastBot/blastBotRight";
	texHandler.createViewDepBoxMipmappedImage(fileName, mipLevel);
	fileName = L"blastBot/blastBotTop";
	texHandler.createViewDepBoxMipmappedImage(fileName, mipLevel);*/


	
	//PngToDDsConverter converter(&texHandler);
	//converter.convertFolder("images/pngs");

	//std::string folderPath = "images\\pngs";
	//fileName = L"field";
	//texHandler.combineImagesInFolderToDDs(folderPath, fileName);
}
