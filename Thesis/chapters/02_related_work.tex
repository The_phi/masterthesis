% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Related Work}\label{chapter:relatedWork}
This chapter reviews a variety of different papers on image similarity metrics and mipmapping techniques.

\section{Image Comparison Metrics}
Even though one of the standard error metrics, the mean squared error (MSE), is easy to use, parameter free, an excellent metric for optimization and historically used in many signal processing tasks, it is well known that MSE under-performs for human perception tasks when measuring image differences since it correlates poorly with human perception \cite{4775883}.

\subsection{SSIM}
The structured similarity (SSIM) index \cite{SSIMOrig} tries to alleviate the shortcomings found in the MSE and is specifically built to work as a human perception metric. It is based on the degradation of structural information under the assumption that human visual perception is highly adapted for extracting structural information from the visual scene. SSIM takes the local luminance, contrast, and structure comparisons to compute a matching score between two images. Later works such as the multi-scale SSIM (MS-SSIM) \cite{1292216} or the information content weighted SSIM (IW-SSIM) index \cite{5635337} improved on the standard SSIM index. However, the standard SSIM is still widely used as image comparison metric \cite{6467150}.


\subsection{FSIM}
The human visual system (HVS) understands an image mainly based on its low level features \cite{FeaturesInHumanVision}. This idea together with phase congruency (PC) is used as the basis for feature similarity (FSIM) \cite{5705575}. PC is a dimensionless measure of the significance of a local structure.

\subsection{HDR-VDP-2}
In their paper "A calibrated visual metric for visibility and quality predictions
in all luminance conditions" Mantiuk et al. \cite{10.1145/2010324.1964935}  describe a new image similarity metric called HDR-VDP-2 that is based on a new visual model for all luminance conditions and was derived from new contrast sensitivity measurements. In addition, they demonstrate comparable or better results than previous metrics such as MS-SSIM.


\subsection{Learned Perceptual Image Patch Similarity}
A completely different approach is used by Zhang et al. in their paper "The Unreasonable Effectiveness of Deep Features as a Perceptual Metric" \cite{zhang2018unreasonable}. They use deep features of neural networks as image similarity metric. Additionally, they find that internal activations of networks trained for high-level classification tasks correspond to judgements performed by the human visual system (HVS). Their results indicate that networks trained to solve image comprehension tasks, such as prediction or modelling, also end up learning a representation of the real world which correlates with HVS judgements. Or, as they put it: "the stronger a feature set is at classification and detection, the stronger it is as a model of perceptual similarity judgements" \cite{zhang2018unreasonable}. The resulting networks can be used as an image similarity metric called Learned Perceptual Image Patch Similarity (LPIPS).

\section{Mipmapping}
Mipmapping has long been known to the computer graphics community. It was first introduced by Lance Williams in 1983 \cite{10.1145/964967.801126} under the title "Pyramidal parametrics" and is a form of level of detail (LOD) system. Its goal was the reduction of computation time for downsampled images by providing a stack of precomputed images of decreasing scales and interpolating between these images. The name is derived from the Latin phrase “multum in parvo” which means “much in little”.

In \cite{doi:10.1111/cgf.13221} Xu et al. demonstrated a novel technique to join mipmapped BRDFs and normal maps. They built their mipmaps by interpolating von Mises-Fisher (vMF) distributions which are further enhanced by enabling multi-lobe representations via lobe clustering.

Manson and Schaefer introduced a mipmap generation technique based on surface parameterization \cite{10.1111/j.1467-8659.2012.03141.x}. While generating the mip stack they take into account texture distortions due to the mapping from 2D texture space to 3D object space. Therefore their method only impacts mipgeneration and not run-time performance.


\subsection{Anisotropic Filtering}
Anisotropic filtering (AF) is an improvement on "normal" isotropic mipmapping for textures on surfaces that are at oblique viewing angles with respect to the camera. In contrast to other mipmapping techniques AF creates its mip levels not only by scaling both image axis simultaneously but also creating versions where both axis are scaled independently. An example of this technique can be seen in figure \ref{fig:anisotropicExample}.

Vertex-based anisotropic texturing by Olano et al. \cite{10.1145/383507.383532} provides an algorithm for a software implementation of AF. It creates the anisotropic mipmap from several mipmap texturing passes. Other techniques are based on the pixel footprint in projection space \cite{1260775} or \cite{Hüttner}.

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.7\textwidth]{MipMap_Example_STS101_Anisotropic}}%
\caption{Example of an anisotropic mipmapped texture. The original texture (top left) is accompanied on the diagonal by the scaled images and on the off-diagonals by linearly distorted versions.  \cite{anisotropicExampleImage}}\label{fig:anisotropicExample}
\end{figure}

\section{Other LOD Techniques}
Apart from textures many other areas in computer graphics also benefit from level of detail systems.

\subsection{Meshes}
Over the years 3D meshes for games and movies became more and more complex. Current meshes created from photogrametry can have several billion triangles on a single object. This results in many faces being smaller than a single pixel if the object is further away from the camera which drastically reduces performance. Level of detail systems help prevent this by creating consecutive lower resolution versions of the original mesh. During rendering the most appropriate model is then chosen depending on the distance to the camera. A good survey of mesh LOD methods can be found in \cite{polygonLOD}.

\subsection{Simulations}
\subsubsection{Fluids}
Fluid simulations can greatly benefit from LOD systems. One example is the work "Adaptive particles for incompressible fluid simulation" on FLIP fluids by Hong et al. \cite{WoosuckHong}. In their work they propose a level of detail approach where areas of high complexity are sampled finer than areas of a lower one.

A similar approach is demonstrated by Barbara Solenthaler and Markus Gross in their paper "Two-Scale Particle Simulation" \cite{Sol11}. They use two separate simualtions, one low- and one high-resolution, with the higher resolution representing the part of the entire simulation domain with the highest complexity.

\subsubsection{Cloth}
Cloth simulations also benefit from level of detail approaches. It is unnecessary to simulate cloth parts far away from the camera with the same complexity as those close to it. This idea is implemented and demonstrated by Koh et al. in their paper on view-dependent adaptive cloth simulations \cite{10.5555/2849517.2849543}. They use an adaptive mesh grid for simulating cloth which adapts the tessellation rate of the mesh depending on the distance to the camera.





