% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Techniques}\label{chapter:techniques}
This thesis will consider those mip levels with higher resolutions as higher mip levels and those with smaller texture resolutions as lower ones.

\section{Bilinear Down-sampling}
The simplest form of image down-sampling is bilinear sampling. It samples the image twice in one dimension and then once between these two values in the other image dimension. If an image is scaled by more than a factor of two this scaling method will ignore some pixels leading to a loss of data. Box filtering solves this issue.

\section{Box Down-sampling}
A box filter is one of the simplest down-sampling techniques. It works by calculating the average color of a set of pixels. Specifically it divides the input image into equally sized patches aligned on a grid. The grid has the size of the output image. Then all pixels in a patch are summed together and normalized by the amount of pixels in the patch. Finally the result is stored in the target image.

This technique is easily implemented and very fast. Although it preserves the general colors of a texture, it leads to image smoothing on lower mip levels, as can be seen in figure \ref{fig:mushTexBoxMip7}.

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{ccc}
\subfloat[Base texture]{\includegraphics[width=0.18\textwidth]{mushroomTex/base}}&
\subfloat[Box]{\includegraphics[width=0.18\textwidth]{mushroomTex/boxMip7}}&
\subfloat[Object]{\includegraphics[width=0.14\textwidth]{mushroom/front/mip7/0mushroomBox}}
\end{tabular}
}
\caption{Example for Box mipmapping. The middle texture shows the mipmapped version (at mip level 7) with colors being blended together. On the right the mipmapped texture can be seen mapped to the object.}
\label{fig:mushTexBoxMip7}
\end{figure}

\section{Kaiser Down-sampling}
Kaiser down-sampling employs an image sharpening algorithm during the scaling process and is one of the standard Unity mip-generation methods. According to the official Unity reference \cite{UnityTextures} a Kaiser type window is used, which reduces blurriness on lower mip levels. The Kaiser window (or Kaiser-Bessel window) was developed by James Kaiser at Bell Laboratories \cite{SASPWEB2011} and is primarily used in signal processing.


The formula for a Kaiser Window can be seen below in equation \eqref{eq:kaiserWindowEquation}.

\begin{equation}
w_0(x)=
	\begin{cases}
	  \frac{I_0 [\pi \alpha \sqrt{1-(2x/L)^2}]}{I_0 [\pi \alpha]},	& \text{if}\ \left|x\right| \leq L/2 \\
      0, & \left|x\right| > L/2
    \end{cases}
\label{eq:kaiserWindowEquation}
\end{equation}

\begin{equation}
I_0(x)= \sum_{k=0}^{\infty} \left[\frac{(\frac{x}{2})^2}{k!}\right]^2
\label{eq:kaiserIEquation}
\end{equation}

\begin{align*}
 L   &=  \text{window duration} \\
 \alpha   &=  \text{ non-negative real number that determines the window shape} \\
\end{align*}


In general $L$ is the window duration. In this specific case it describes the kernel size.


As mentioned above this mip generation technique comes directly with Unity. Therefore the actual function used in the implementation might differ a little from \ref{eq:kaiserWindowEquation} since I could not verify the Unity implementation.

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{cccc}
\subfloat[Base texture]{\includegraphics[width=0.25\textwidth]{mushroomTex/base}}&
\subfloat[Box]{\includegraphics[width=0.25\textwidth]{mushroomTex/boxMip7}}&
\subfloat[Kaiser]{\includegraphics[width=0.25\textwidth]{mushroomTex/kaiserMip7}}&
\subfloat[Object]{\includegraphics[width=0.21\textwidth]{mushroom/front/mip7/1mushroomKaiser}}
\end{tabular}
}
\caption{Box and Kaiser mipmapped textures (both at mip level 7). The object on the right has the Kaiser mipmapped texture projected onto it.}
\label{fig:mushTexKaiserMip7}
\end{figure}

\section{Lanczos Downsampling}
Lanczos down-sampling uses a lanczos filter which works as a low pass filter. It is based on a normalized $sin$ function called $sinc(x)$ which is multiplied by the Lanczos window. This window is the central lobe of a horizontally stretched $sinc$ function $sinc(x/a)$ in the range $-a \leq x \leq a$.

\subsection{Lanczos Filter}
The lanczos filter is defined as follows:
\begin{equation}
\begin{aligned}
L(x)&=
	\begin{cases}
      sinc(x)sinc(x/a), & \text{if}\ -a<x<a \\
      0, & \text{otherwise}
    \end{cases}\\
sinc(x) &= \frac{\sin(\pi x)}{\pi x}
\end{aligned}
\label{eq:lanczosEquation}
\end{equation}

The parameter $a$ is a positive integer and defines the size of the filter kernel. In this case it describes the amount of pixels around the current one that are used for evaluation. $x$ is the distance of the current pixel to the kernel center.

As with the box filter the lanczos filter divides the input image into individual patches and works on each patch individually. But instead of equal weights for all pixels within the patch each pixel is weighted based on the value computed with formula \eqref{eq:lanczosEquation}. Afterwards the color sum is renormalized to the correct color range.

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{cccc}
\subfloat[Base texture]{\includegraphics[width=0.25\textwidth]{mushroomTex/base}}&
\subfloat[Kaiser]{\includegraphics[width=0.25\textwidth]{mushroomTex/kaiserMip7}}&
\subfloat[Lanczos]{\includegraphics[width=0.25\textwidth]{mushroomTex/lanczosMip7}}&
\subfloat[Object]{\includegraphics[width=0.21\textwidth]{mushroom/front/mip7/3mushroomLanczos}}
\end{tabular}
}
\caption{Kaiser and Lanczos mipmapped textures (both at mip level 7). The Lanczos texture has much sharper edges than the kaiser version, resulting in cleaner colors when mapped to the object (seen on the right).}
\label{fig:mushTexLanczosMip7}
\end{figure}

\section{High Pass Filter Downsampling}
In addition to low pass filters such as Box or Lanczos, high pass filters could also be used for image down-scaling. In image processing they can be used as edge detectors or as image sharpeners.

Since high pass filters only preserve high frequency details in images they are not really viable for image down sampling in the case of mip maps. In nature when viewing an object from far away mostly only the low frequency broad details are visible. Preserving only the high frequency image features would result in very noisy scenes. In addition big but smooth features that can definitely be seen from far away would be lost as well.


\section{Perceptual Down-sampling}
Perceptual down-sampling by Cengiz Öztireli and Markus Gross \cite{percepDownScale} proposes a perceptually based method for image downscaling. They adapt the SSIM metric to create an optimization problem for image scaling. The solution to this problem results in images that are optimized in respect to the SSIM metric and as such retain visually important image features.

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.8\textwidth]{perceptualDownExample}}%
\caption{An example for perceptual image down-scaling (bottom right). This image is from the original paper \cite{percepDownScale}. Left: ground truth, top-left: subsampling, top-right: bicubic filtering, bottom left: content-adaptive downscaling \cite{10.1145/2508363.2508370}, bottom-right: perceptual (theirs).}\label{fig:perceptualExample}
\end{figure}

\pagebreak

\subsection{SSIM}
The basis for perceptual image down-sampling is the SSIM index which uses the local luminance, contrast, and structure comparisons to calculate a similarity score between two images. It is computed using formula \eqref{eq:SSIM}.

\begin{equation}
\begin{aligned}
SSIM(x, y) &= \frac{(2\mu_x\mu_y+c_1)(2\sigma-{xy}+c_2)}{(\mu_x^2 + \mu_y^2+c_1)(\sigma_x^2 + \sigma_y^2+c_2)}\\
\mu_x &= \sum w_i x_i\\
\sigma_x^2 &= \sum w_i(x_i-\mu_x)^2\\
\sigma_{xy} &= \sum w_i(x_i-\mu_x)(y_i-\mu_y)\\
\label{eq:SSIM}
\end{aligned}
\end{equation}

\begin{align*}
 \mu_x   &=  \text{mean} \\
 \sigma_x^2   &=  \text{variance} \\
 \sigma_{xy}   &=  \text{covariance} \\
 w_i   &=  \text{weights} \\
 x_i     &=  i^{th}\text{ component of } x \\   
 c_1 \text{ and } c_2 &=  \text{small constants to avoid instability}\\
\end{align*}

For simplicity reasons and because it does not make a difference for the down-scaling problem $c_1$ and $c_2$ are assumed to be $0 = c_1 = c_2$. The result of equation \eqref{eq:SSIM} is in $SSIM(x, h) \in [0,1]$, since both $x$ and $h$ are in $[0, 1]$ and is q if $x = h$. This allows to define a dissimilarity score as $d(h, x) = 1 - SSIM(h, x)$.

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{cccc}
\subfloat[Base texture]{\includegraphics[width=0.20\textwidth]{mushroomTex/base}}&
\subfloat[Lanczos]{\includegraphics[width=0.2\textwidth]{mushroomTex/lanczosMip7}}&
\subfloat[Perceptual]{\includegraphics[width=0.2\textwidth]{mushroomTex/perceptualMip7}}&
\subfloat[Object]{\includegraphics[width=0.16\textwidth]{mushroom/front/mip7/2mushroomPerceptual}}
\end{tabular}
}
\caption{Lanczos and Perceptually mipmapped textures (both at mip level 7). Image c has extremely sharp edges caused by the perceptual down-sampling algorithm. This leads to black discolorations when projected onto the object.}
\label{fig:mushTexPerceptualMip7}
\end{figure}

\subsection{Downsampling Problem}
Given the high resolution input image $H$ the goal of the down-sampling problem is to find the scaled output image $D$ that is as close to the original image as possible when measured with the SSIM index. The final scaled image $D^*$ is the one that results when using the distance measure $d(H, D)$.

Since most image comparison techniques are only designed to compare images with the same dimensions the output image has to be scaled back up to input resolution. This up-scaled image is denoted $X$. Piecewise constant interpolation is used for rescaling.

The SSIM index only computes the similarity between local patches of images. These similarity scores are summed up for all patches. The downscaling problem is then represented by formula \eqref{eq:perseptualDownsamplingProblem} where $S$ is the set of patches.

\begin{equation}
X^* = \argmin_x \sum_{P_i \in S} {D(P_i(H), P_i(X))}
\label{eq:perseptualDownsamplingProblem}
\end{equation}

Since the patches of $S$ do not overlap the optimum patch $P^*(X)$ of image $X$ can be computed by
\begin{equation}
P^* = \argmin_{P(X)} D(P(H), P(X))
\label{eq:perseptualDownsamplingProblemPatch}
\end{equation}

As mentioned above the distance measure $d(H, D)$ is defined as $d(h, x) = 1-SSIM(h, x)$.

\subsection{Downsampling}
Using several optimization techniques the problem described in equation \eqref{eq:perseptualDownsamplingProblem} can be solved with a closed-form solution. The entire solution with a thorough explanation can be found in section "3.2 Optimization with SSIM" of the original paper \cite{percepDownScale}.


Formula \eqref{eq:perseptualDownsamplingFinal} calculates the value for the $i^{th}$ pixel in output image $D$. This solution preserves local luminance and contrast while maximizing the local structural similarity index.


\begin{equation}
d^*_i = \frac{1}{n_p}\sum_{P_k}{\mu^k_h+\frac{\sigma^k_h}{\sigma^k_l}(l_i-\mu^k_h)}
\label{eq:perseptualDownsamplingFinal}
\end{equation}
\begin{align*}
 P_k   &=  \text{the } n_p \text{patches overlapping this pixel} \\
 \mu^k_h   &=  \text{mean} \\
 \sigma^k_h   &=  \text{variance} \\
\end{align*}




\section{Learned Perceptual Image Patch Similarity}
In their work "The Unreasonable Effectiveness of Deep Features as a Perceptual Metric" Zhang et al. \cite{zhang2018unreasonable} investigate the usefulness of neural network deep features as a perceptual metric of human vision. Their work compares existing image similarity metrics like PSNR and SSIM to human judgements and those performed by a neural network. To evaluate the performance of these different metrics they created a new dataset of perceptual judgements. Its basis are two alternative forced choice (2AFC) tests. These ask which of the two distortions is more similar. A second test which asks whether two patches are the same or different is used as validation (one patch is a reference and one is distorted). The different metrics are then evaluated against this dataset. They conclude that these deep features perform very good as a metric for human vision. This is because a neural network trained for classification and detection also learns a representation that works well as effective perceptual judgement. They conclude that "[...] features that are good at semantic tasks are also good at self-supervised and unsupervised tasks, and also provide good models of human perceptual behaviour [...]" \cite{zhang2018unreasonable}. They refer to their trained networks as variants of their proposed Learned Perceptual Image Patch Similarity (LPIPS) metric. It computes the distance by running two image patches through a neural network, taking the $l2$  norm between the networks deep features of the network and normalizing this value.

This thesis uses the LPIPS similarity metric as loss for a neural network trained on image down-sampling. The resulting images are used as mip levels. In theory this should result in mip levels with the high visual similarity as perceived by humans. Further details on the implementation can be found in chapter \ref{chapter:implementation}.

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{cccc}
\subfloat[Base texture]{\includegraphics[width=0.25\textwidth]{mushroomTex/base}}&
\subfloat[Perceptual]{\includegraphics[width=0.25\textwidth]{mushroomTex/perceptualMip7}}&
\subfloat[LPIPS]{\includegraphics[width=0.25\textwidth]{mushroomTex/lpipsMip7}}&
\subfloat[Object]{\includegraphics[width=0.21\textwidth]{mushroom/front/mip7/6mushroomLPIPs}}
\end{tabular}
}
\caption{Perceptually and LPIPS mipmapped textures. Both are at mip level 7. Note the color shift in the LPIPS version (further details can be found in chapter \ref{chapter:implementation}).}
\label{fig:mushTexLPIPSMip7}
\end{figure}


\section{Parameterization Aware Downsampling}
LPIPS and perceptual down-sampling only optimize the scaled textures to look similar to the original image. They completely disregard that the texture is not used as is but rather projected onto a three-dimensional surface. This projection is not isometrical which can introduce projection errors, such as black lines where faces meet that are mapped to different parts on the texture. Parametrization aware down-sampling \cite{10.1111/j.1467-8659.2012.03141.x}  solves this issue by taking the projection into account during texture scaling. It calculates the actual surface coverage of each texel and weights them accordingly.

\subsection{Parameterization Aware Filtering}
The mapping from 2d texture space to the 3d surface of an object is non-uniform. Different triangles can have different texel densities, can overlap in texture space and parts of the texture might not be used at all.

Parameterization-Aware MIP-Mapping tries to reduce these issues by filtering textures in object space. This is done by calculating the surface coverage of each texel on the target object and weighting them accordingly. As with all methods described in this paper this happens in a preprocessing step and does not influence final render time.

Typical texture sampling calculates the coefficient $c_h$ for the sampling filter $h$ by using formula \eqref{eq:generalCHMipMapParam}.

\begin{equation}
c_h = \int \int_\mathbb{R} h(u,v) \sum_i \hat{c}_i \hat{b}_i(u,v)\dd{u}\dd{v}
\label{eq:generalCHMipMapParam}
\end{equation}

\begin{align*}
 c_h   &=  \text{samplig filter coefficient for filter h} \\
 h     &=  \text{sampling filter} \\   
 \hat{c}_i &=  \text{texel coefficients (i.e. colors)}\\
 \hat{b}_i &= \text{basis functions that comprise the input image }\hat{I}\\
 \hat{I} &= \text{input image}
\end{align*}

\pagebreak

In contrast Parameterization-Aware MIP-Mapping integrates over the surface $\Omega$ of the object:
\begin{equation}
c_h = \frac{\int \int_\Omega h(\Theta(p)) \sum_i{ \hat{c}_i \hat{b}_i(\Theta(p))}\dd{p}}
		{\int \int_\Omega h(\Theta(p))\dd{p}}
\label{eq:generalParam}
\end{equation}

\begin{align*}
 c_h   &=  \text{sampling filter coefficient for filter h} \\
 h     &=  \text{sampling filter (defined in texture space)} \\   
 p &=  \text{three dimensional points on the surface}\\
 \hat{b}_i &= \text{basis functions that comprise the input image }\hat{I} \text{ (defined in texture space)}\\
 \Theta &= \text{mapping between the surface of an object and a texture}
\end{align*}

Formula \eqref{eq:generalParam} includes a normalization term since the domain of $h$ might be distorted when integrating over a surface.

The actual implementation evaluates $c_h$ by summing over triangles ($T_k$) in a mesh. These triangles each have their own barycentric coordinates $\Omega$. Together with the mapping $\Gamma_k: \mathbb{R}^2 \rightarrow \mathbb{R}^3$, which maps from these barycentric coordinates to the triangle $T_k$, equation \eqref{eq:generalParam} can be rewritten as:

\begin{equation}
c_h = \frac{\sum_k \int_{0}^{1} \int_{0}^{x} h(\Theta(\Gamma_k(x,y))) \sum_i{ \hat{c}_i \hat{b}_i(\Theta(\Gamma_k(x,y)))\Delta_k}\dd{y}\dd{x}}
		{\sum_k \int_{0}^{1} \int_{0}^{x} h(\Theta(\Gamma_k(x,y))) \Delta_k \dd{y}\dd{x}}
\label{eq:imlementationParam}
\end{equation}
\begin{align*}
 \Delta_k   &=  \text{absolute value of the determinant of the Jacobian of } \Gamma_k \text{(equal to the area of triangle } T_k \text{)}
\end{align*}



The original paper \cite{10.1111/j.1467-8659.2012.03141.x} considers this mip mapping technique parametrization aware since it weights texels by their parametric distortion. The effects of this method can be seen in figure \ref{fig:paramAwareDownExampleFromPaper}. It shows the effect surface projection distortions can have on the final result. In the image a texture that is divided into a blue and a green area, but is mostly blue, is projected onto a surface in such a way, that blue and green triangles take up equal amounts of space in object space. With standard mip-generation techniques the blue color will completely dominate the lower mip levels. Parameterization-aware mipmapping prevents this issue by giving blue and green pixels equal weights during image down-sampling. 

In addition to weighting texels by their color contribution in object space the paper also introduces a least-squares approach to filtering the mip stack that takes into account the filtering method used by the GPU.


\begin{figure}[h!]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.35\textwidth]{paramAwareComparisonFromPaper}}%
\caption{This image which is from the original paper \cite{10.1111/j.1467-8659.2012.03141.x} on parameterization aware mipmapping shows the effects of parameterization aware down-sampling. The top row demonstrates the parametric distortion due to UV-Mapping with object space on the left and texture space on the right. The middle row shows trilinear mipmapping and the bottom one 16 times anisotropic mipmapping. In the middle and bottom row from left to right: normal box down-scaling, parameterization aware down-sampling, and parameterization aware constrained trilinear mipmaps.}\label{fig:paramAwareDownExampleFromPaper}
\end{figure}

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{cccc}
\subfloat[Base texture]{\includegraphics[width=0.18\textwidth]{mushroomTex/base}}&
\subfloat[Lanczos]{\includegraphics[width=0.18\textwidth]{mushroomTex/lanczosMip7}}&
\subfloat[Parameter aware]{\includegraphics[width=0.18\textwidth]{mushroomTex/paramAwareMip7}}&
\subfloat[Object]{\includegraphics[width=0.15\textwidth]{mushroom/front/mip7/5mushroomParamAware}}
\end{tabular}
}
\caption{Lanczos and parametrization aware mipmapped textures (both at mip level 7). The upper white blob is missing in the right texture since it is not mapped to any surface and therefore gets assigned zero weights during down-sampling.}
\label{fig:mushTexParamAwareMip7}
\end{figure}

\subsection{Implementation}
This implementation directly computes the area covered by each texel in surface space and weights them accordingly during down sampling. Further details regarding this can be found in section \ref{ParamAwareImpl}.



\section{Viewdependant Downsampling}
Objects can look different from different directions due to the distribution of the surface colors. For example a vase might be blue on one side but yellow on the other. When viewing an object from a distance you would expect this color distribution to stay the same. Unfortunately this property is not preserved with common mip mapping techniques. Due to the decrease in pixel density in lower mip maps the lowest levels might get so small that there are simply no longer enough pixels left in the texture to accurately represent the different colors. This leads to color information being either lost entirely or colors becoming blended together. To solve this problem a novel view-dependent mipmapping technique is proposed by this thesis. It adds directional data to the color information. This is done by projecting the entire object onto new textures that encode the directional information  implicitly.

Overall two new projection methods are introduced. The first technique projects the object onto a surrounding sphere which is then stored into a texture using sphere coordinates. The second approach renders the object to a virtual surrounding box (similar to sky or cube maps). This cube is then stored in six different images.

The second approach prerenders the object from different directions. During final rendering for each fragment the optimal initial rendering is chosen and the fragment color determined from there.

\subsection{Sphere Mapping}
This approach encodes view dependent directional color data by projecting the target object onto a surrounding sphere. The projection is then save to a texture using spherical coordinates with the x-axis representing the polar angle and the y-axis the elevation. This implicitly encodes directional data into the texture since each pixel directly correlates with a certain direction. During rendering the directional information is calculated for each fragment from its position in object space and its offset towards the spheres projection center. This center can be freely chosen but should be inside the objects geometry as positioning it outside will lead to projection errors.

\begin{figure}[h!]
\makebox[\textwidth][c]{
\begin{tabular}{ccc}
\subfloat[Base texture]{\includegraphics[width=0.25\textwidth]{mushroomTex/baseSphericViewDep}}&
\subfloat[Mip level 7]{\includegraphics[width=0.25\textwidth]{mushroomTex/sphercialVDMip7}}&
\subfloat[Object]{\includegraphics[width=0.21\textwidth]{mushroom/front/mip7/8mushroomSphericalBox}}
\end{tabular}
}
\caption{Example for view-dependent mipmapping with spherical projection.}
\label{fig:mushTexSphericalVDMip7}
\end{figure}


\subsubsection{Texture Resolution}
This method strongly depends on the resolution of the projected texture. If it is to small its colors will get blended together the same way as with conventional mipmapping and the benefit of view-dependence will get lost. Unfortunately mipmapping works by matching the texel size to the screens pixel size. No matter the initial texture resolution if an object gets too far away from the camera the texture will get to small and the directional color data will get lost.


In addition if the resolution of the target texture is too small comparatively large areas are covered by the same texture texel which leads to texture stretching. The effect can also happen if the surface geometry is misaligned. If a face is aligned almost perpendicular to the direction from sphere center to projection point its color can be stretched over its entire surface and fine details on it will be lost.


\subsection{Box Mapping}
This method prerenders the object onto a surrounding cube with each side encoding the colors that can be seen in that direction. These pre-generated images can then be processed further to better preserve specific colors. During actual rendering the image with best fit is then chosen for each fragment. 

This method ensures that even if the texture gets as small as one pixel and all colors in the chosen texture are blended into one, it still preserves some directional data since only the color seen from this particular side is used.

\subsubsection{Problems}
In areas where two textures "meet" a clear edge between them can be seen. This line is created because the two textures were taken from different directions so the colors in these border cases do not line up perfectly.


\subsection{Overhang Problem}
Both methods suffer from wrong color projections due to overhanging geometry. If parts of the geometry are occluded in the direction of a box projection plane they will always get shaded in the same color as the occluding part. A demonstration can be seen in figure \ref{fig:boxProjectionError}. The effect is especially noticeable on objects close to the camera where higher mip levels are used. Figure \ref{fig:boxProjErrorExample} shows the effects in a live example.

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=0.3\textwidth]{viewdepProjectionError}}%
\caption{Example for box projection. The blue object is projected onto the black line. Due to the geometry the red part will be occluded and receive the same color as the green area.}\label{fig:boxProjectionError}
\end{figure}

\begin{figure}[h!]
\begin{subfigure}{.6\textwidth}
\begin{tabular}{ccc}
\subfloat[Back]{\includegraphics[width=0.3\textwidth]{mushroomTex/vdBox/boxVDmip7Back}}&
\subfloat[Left]{\includegraphics[width=0.3\textwidth]{mushroomTex/vdBox/boxVDmip7Left}}&
\subfloat[Top]{\includegraphics[width=0.3\textwidth]{mushroomTex/vdBox/boxVDmip7Top}}\\
\subfloat[Front]{\includegraphics[width=0.3\textwidth]{mushroomTex/vdBox/boxVDmip7Front}}&
\subfloat[Right]{\includegraphics[width=0.3\textwidth]{mushroomTex/vdBox/boxVDmip7Right}}&
\subfloat[Bottom]{\includegraphics[width=0.3\textwidth]{mushroomTex/vdBox/boxVDmip7Bottom}}
\end{tabular}
\end{subfigure}%
\begin{subfigure}{.35\textwidth}
\subfloat[Object]{\includegraphics[width=1\textwidth]{mushroom/front/mip7/7mushroomViewDep}}
\end{subfigure}
\caption{Example for view-dependent mipmapping with box projection. The image in the right shows mip level 7. The grid on the left all six base textures (at mip level 0).}
\label{fig:mushTexBoxVDMip7}
\end{figure}

\begin{figure}[h]
\makebox[\textwidth][c]{
\begin{tabular}{c}
\subfloat[Box projection]{\includegraphics[width=0.5\textwidth]{browser/frontTop/mip0/7BowserViewDep}}\\
\subfloat[Spherical projection]{\includegraphics[width=0.5\textwidth]{browser/frontTop/mip0/8BowserViewDepSpherical}}
\end{tabular}
}
\caption{An example for the projection errors produced by the view-dependent mipmapping techniques. The left image uses box projection, the right one spherical projection.}\label{fig:boxProjErrorExample}
\end{figure}





\subsection{Solutions}
In general all the above mentioned problems with view dependent mipmapping can be reduced or alleviated altogether if this technique is only used for far away/ very small objects. This will not only reduce the mentioned effects, but the objects will be comparatively small, so that the negative effects are not that visible.