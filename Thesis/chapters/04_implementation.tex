% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Implementation}\label{chapter:implementation}
The implementation is divided into two parts. The c++ and python side - which was used for generating most of the mipmaps - and Unity 3D for displaying the results and generating comparison images. In addition, Unity internal Box and Kaiser mipmap generation techniques were used for the respective textures.

Since standard image formats like png or jpeg do not support mipmaps, a special format was needed for this. Microsoft's DirectX DDS file format was chosen since it supports mipmaps, has special hardware support and is compatible with Unity. The DDS files were generated using the DirectXTex library from Microsoft.

The Kaiser and box filter methods are standard methods for mipmap generation in Unity. 
These implementations were used directly for this analysis.\\
C++ was used for the perceptual and the parameterization aware down-sampling.\\
LPIPS and Lanczos down-scaling were implemented in Python using existing Python libraries.


\section{Mip Generation Process}
The c++ implementation uses the Visual Studio 2017 (v141) tool-set. DirectXTex from Microsoft is used for the DDS file and mip map handling. This includes file storage and mip generation.

The general process for the mip generation methods in c++ is completely identical. First an empty mip map of the desired depth is generated. Afterwards each level is filled with its respective mip texture. The final texture with all mip levels is then saved onto the disk.

The parametrization aware down-sampling generates each mip level from the base image. Mipmaps for perceptual down-scaling were generated from the previous mip level.

\section{Python}
Two mipmapping algorithms were implemented in Python (version 3.7.4). This includes the Lanczos filter and LPIPS down-sampling. Neural network handling for LPIPS was implemented with PyTorch. In addition Python was used to compute the image metrics for the comparison in chapter \ref{chapter:results}.



\subsection{General Problems}
Some mipmap generation methods produce miscolorations or black spots at the edges of uv islands once a texture is mapped to an object. These appear because the uv islands on lower mip levels no longer perfectly align with the corresponding area producing discolourations at the edges of the islands. An example for these errors can be seen in figure \ref{fig:browserMip5Comparison}. Extruding the colors of each uv island can fix this issue. The fix was not implemented due to time constraints.

\section{Lanczos Down-sampling}
Lanczos down-sampling uses a Lanczos filter,which is a low pass filter originally used for signal processing, for image down-sampling . 
This image down-scaling technique computes a weight for each pixel in the kernel using formula \eqref{eq:lanczosEquation} described in chapter \ref{chapter:techniques}. These weights are then used to calculate the weighted sum of all pixels in the kernel. Finally the sum is normalized too the valid color range.

As mentioned before, a Python implementation of this image scaling technique was used. In particular the version from OpenCV python which uses a kernel of size $8x8$.

\section{High Pass Filter Down-sampling}
For test purposes a high pass filter was also implemented. Unfortunately it produced no conclusive results. The generated mipmaps are highly blurred and do not really preserve high frequency details. This might be an inherent property of using high pass filters for image down sampling, but an error in the implementation is much more likely. Due to these reasons it is not considered in the results.

\section{Perceptual Down-sampling}
This thesis's c++ implementation of the perceptual down-sampling is based on pseudo code of the original paper \cite{percepDownScale} and a matlab implementation found on github \cite{percepMatlabRepo}. This matlab version mostly aligns with the code and math from the paper but differs in one computation where a kernel of size two is used in the paper, while one of size one in the implementation. To get as close to the results presented in the paper a kernel of size one was used in this thesis as well. Using a kernel of size two seemed to smooth the resulting images to much when compared to the example cases from the paper. Using a kernel of size one generated images much closer to those seen in the paper.

\begin{figure}[h]
\makebox[\textwidth][c]{
\begin{tabular}{ccc}
\subfloat[Ground truth]{\includegraphics[width=0.3\textwidth]{browser/front/mip0/0browserBox}} &
\subfloat[Box]{\includegraphics[width=0.3\textwidth]{browser/front/mip7/0browserBox}} &
\subfloat[Perceptual]{\includegraphics[width=0.3\textwidth]{browser/front/mip7/2browserPerceptual}}\\
\subfloat[Lanczos]{\includegraphics[width=0.3\textwidth]{browser/front/mip7/3browserLanczos}} &
\subfloat[Parametrization aware]{\includegraphics[width=0.3\textwidth]{browser/front/mip7/5browserParamAware}} &
\subfloat[LPIPS]{\includegraphics[width=0.3\textwidth]{browser/front/mip7/6browserLPIPs}}
\end{tabular}
}
\caption{This comparison demonstrates the color errors introduced by various techniques. Box down-sampling mixes colors together resulting in darker areas on the face and arms. Perceptual creates black discolorations. Lanczos keeps the colors mostly intact but still smears some of them together. LPIPS shows a severe red shift. Parametrization aware down-sampling generates the best results of all show methods. The ground truth has no mipmapping, all other images are on mip level seven.}
\label{fig:browserMip5Comparison}
\end{figure}

\subsection{Linear Algebra}
All matrix math related computations for this method were implemented using version 3.3.7 of the c++ library "Eigen" (\cite{eigenMath}). All mip levels for this method were generated from the previous level instead of the base texture. This is due to a bug in the code that was discovered to late to be included in the evaluation.


\subsection{Perceptual Pseudo Code}
The pseudo code seen in algorithm \ref{perceptualPseudoCode} shows the structure of the perceptual downscaling implementation and is identical to the implementation shown in the original paper \cite{percepDownScale}.\\
In  algorithm \ref{perceptualPseudoCode} all operations are element-wise on single channel images. Big letters denote individual matrices. $\epsilon = 10-6$, subSample$(X, y)$ subsamples the input image $X$ at intervals of size $y$, $I_X$ creates a matrix of the same size as $X$ filled with all ones, $X(C)$ creates a matrix with the same dimensions as $X$ filled with zeros and all entries from $X$ where the corresponding entry in matrix $C$ is true are copied to the new matrix, convValid$(X, P(y))$ convolves the image $X$ with an averaging filter of size $y \times y$ such that the kernel always stays within the bounds of the input matrix, convFull$(X, P(y))$ is similar to convValid$(X, P(y))$ except all values outside the matrix are assumed to be zero so that the filter kernel can go outside the matrix limits.



\begin{algorithm}
\caption{Perceptual Down-scaling}
\hspace*{\algorithmicindent} \textbf{Input: }\text{Input image $H$, downscaling factor $s$, patch size $n_p$} \\
\hspace*{\algorithmicindent} \textbf{Output: } \text{Downscaled image $D$} 
\begin{algorithmic}[1]
\Procedure{DownscaleImage}{}
\State $L \gets \text{subSample(convValid} (H, P(s)), s)$
\State $L_2 \gets \text{subSample(convValid} (H^2, P(s)), s)$
\State $M \gets \text{convValid} (L, P(\sqrt{n_p}))$
\State $S_l \gets \text{convValid} (L^2, P(\sqrt{n_p}))-M^2$
\State $S_h \gets \text{convValid} (L_2, P(\sqrt{n_p}))-M^2$
\State $R \gets \sqrt{S_h/S_l}$
\State $R(S_l<\epsilon) \gets 0$
\State $N \gets \text{convFull} (I_M, P(\sqrt{n_p}))$
\State $T \gets \text{convFull} (R \times M, P(\sqrt{n_p}))$
\State $M \gets \text{convFull} (M, P(\sqrt{n_p}))$
\State $R \gets \text{convFull} (R, P(\sqrt{n_p}))$
\State $D \gets (M + R \times L - T)/N$
\EndProcedure
\end{algorithmic}

\caption{\\
Pseudo-code for the perceptual down-sampling algorithm \ref{perceptualPseudoCode}.}
\label{perceptualPseudoCode}
\end{algorithm}

\section{LPIPS}
The implementation of the LPIPS image similarity metric used in this thesis was taken from the authors of the original paper \cite{zhang2018unreasonable}.

LPIPS down-sampling was implemented in Python through use of the PyTorch library. This enabled the network training to run completely on the graphics card which significantly sped up mipmap generation.

\subsection{Mip Level Generation}
To generate a mip level, the original image is first scaled down to the target resolution using bicubic interpolation. Next, a Stochastic Gradient Descent optimizer is trained for 450 iterations. The optimizer works on the downscaled version and tries to match this image as close as possible to the original image. It uses an initial learning rate of 0.5 and a momentum of 0.9. The loss is computed by taking the down-sampled image, scaling it back to the original resolution by using bicubic interpolation and computing the LPIPS image similarity value compared to the original texture. In addition, a pytorch ReduceLROnPlateau scheduler is used to reduce the learning rate by a factor of 10 when it stagnates for to long.

\subsection{Problems}
This implementation creates mipmaps that tend towards one of the primary colors. The color shift seems to favour the color that is most prevalent in the original texture. On higher mip levels the effect is barely noticeable whereas lower levels have strong color distortions. The cause of this effect could not be clearly determined. It might be an intrinsic behavior of the LPIPS implementation or caused by the used down sampling implementation. In case of the latter, longer training might solve the issue. An L2 regularization was tested to help prevent this problem but showed no improvement. Further investigation of this issue is outside the scope of this thesis.

\section{Parameterization Aware Down-sampling}  \label{ParamAwareImpl}
This down sampling method weights the individual texels of a texture by their projected size in object space. If a texel is part of multiple triangles it gets more weight.

\subsection{Texel Coverage}
Initially the algorithm calculates the surface coverage in object space for each texel. To do this, the algorithm loops over all faces and computes all texels that are mapped onto each face. Next the intersection area between each texel and its corresponding face or faces is calculated in texture space and then converted to surface space. The intersection area is found using a simple rectangle-triangle intersection test. The surface coverage of this texel is then stored in a lookup structure. When all faces are done, the final coverage value for each texel is calculated from the lookup structure.\\
Each texels coverage is weighted by the total area of the respective face it falls on. This is done to prevent small triangles having a disproportionate impact on the final result.

\subsection{Down-sampling}
The actual down-scaling is performed similar to a box filter. But whereas a box filter uses equal weights for each texel, this method weights them by their surface coverage. This is done by summing up all coverage values in a kernel patch and then weighting each pixel by its own contribution to the sum.

\subsection{Shortcomings}
If a fragment is shaded by the GPU and its computed mip level lies between two precomputed mipmap textures, an interpolation between these mip levels is performed. The original paper includes an additional step that optimizes the resulting mip stack for these interpolations. In particular, their contribution contains corrections for parametric distortions. However this optimization was not implemented for this thesis.

\section{View-Dependent Mipmapping}
The view dependent mipmaps are generated fundamentally different from all other mipmaps in this paper. They are divided into two versions. The first version projects the object onto a surrounding sphere to encode directional data. The second uses several textures that each show the object rendered from a specific perspective, this implementation uses six images creating a cube texture around the object.

For both versions the initial images were created in Unity through the process described below.

To remove potential lighting issues, these images were rendered without shadows and only ambient lighting. Normal mapping was also disabled.

\subsection{Sphere Mapping}
Here the object is rendered onto a surrounding sphere. This is done by using sphere coordinates with an arbitrarily large radius. The only constraint on the radius is that it has to be large enough to contain the entire object. This is only important during initial sphere texture generation as the radius can be ignored during final rendering.

The texture dimensions correspond to the elevation  and orbit angle (phi and theta). Texture x direction is elevation and texture y direction is orbit angle.

\begin{align}
\label{eq:cartesianToSpherical}
\begin{split}
r &= \sqrt{x^2 + y^2 + z^2}\\
\varphi &= \arctan{\frac{y}{x}}\\
\theta &= \arccos{\frac{z}{r}}\\
\end{split}
\end{align}
\begin{align}
\label{eq:sphericalToCartesian}
\begin{split}
x &= r \sin{\theta} \cos{\varphi}\\
y &= r \sin{\theta} \sin{\varphi}\\
z &= r \cos{\theta}\\
\end{split}
\end{align}

\begin{align*}
 r   &=  \text{radius} \\
 \varphi     &=  \text{elevation or azimuth angle} \\   
 \theta &=  \text{polar or inclination angle}\\
 (x,y,z) &= \text{Cartesian coordinates}\\
\end{align*}

Equation \ref{eq:sphericalToCartesian} shows the mapping from spherical to Cartesian coordinates, equation \ref{eq:cartesianToSpherical} shows the mapping from Cartesian to spherical coordinates.


\subsubsection{Texture Generation}
First a virtual sphere is placed around the object. Normally it is centred on the geometric center of the object. If the geometric center is outside the objects geometry, the sphere center has to be moved to a spot within the object. In theory, this new center can be anywhere within the geometry, but results are best if it is placed at a sensible point such as at the center of a big geometry cluster. The 360 degrees of the polar orbit and 180 degrees of the elevation are each divided by the textures width or height. This maps the texture to the sphere while also dividing the sphere into discrete segments.

A raycast is performed for each segment to determine its color. The raycast is  performed from a corner of a segment and directed towards the center of the sphere. At the first intersection point with the object the color value is retrieved and stored in the texture.

To avoid lighting issues, the object is only lit by white ambient lighting and all shadows are disabled during this process.

This texture only uses normal box mipmapping as described above.


\subsubsection{Final Sampling}
The final texture sampling is described later on in the shader section.


\subsection{Box Mapping}
In this version, the target object is rendered from specific directions which are then projected back onto the object during the actual rendering.


\subsubsection{Preprocessing}
As mentioned in chapter \ref{chapter:techniques}, the object is projected onto six textures placed in a cube configuration. For each texture, the camera is placed in such a way that the objects geometric center is at the center of the image frame. Next, the camera and render-texture are scaled so that the objects outer bounds align with the image frame. This avoids mapping issues during final rendering and keeps unused space on the texture to a minimum. Finally, the object is rendered and the resulting textures are saved onto the disk.

\subsubsection{Different Mipmapping Implementations}\label{sec:BoxMapDownSample}
Several different mipmap generation techniques were tested in combination with this algorithm. In addition to a simple box filter that ignores all pixels in a texture that do not show the object, two further methods specific to this technique were tested.


The first method relies on the idea of view-dependent mipmapping that certain colors are prevalent from certain directions. So each side has its own "primary" color. For example, Bowser from Super Mario appears yellow from the front but greenish from the back, and the mushroom which is red from the top but purple from below. Using this idea, a filter that assigns more weight to texels the closer they are to this color can be built. Color "closeness" is defined by the geometric distance between the texels color and the predefined one.

This results in mipmaps where the lower levels do not necessarily accurately represent the initial texture but instead a version where the colors shift closer and closer to this "primary" value. As a result, distant objects can be perceived much closer to the expected color value than with traditional techniques.

The second mip mapping technique uses precomputed normal images. These are created in the same way the color images are rendered. Only, instead of color values they encode the normal directions of the object in world space.\\
For image down-sampling, first the average direction for each patch is calculated from all normal directions in this patch. Afterwards every texel is weighted by the divergence of its corresponding normal vector from the patch average.

\subsubsection{Final Sampling}
The final texture sampling is described in the shader section.

\section{Shader}
The mipmaps were visualized in Unity using a custom shader. In theory, the standard shader from Unity could have been used but these have the disadvantage of selecting the appropriate mip level on their own. This is the desired behaviour for normal rendering in games or similar tasks but makes inspecting the individual mip levels very challenging. If the desired mip level can not be selected by hand, objects have to be moved away from the camera until the desired mip level is rendered. This prevents effective comparisons since different mip levels will result in different object scales. To make an evaluation possible, a custom shader that allows the user to select the used mip level by hand was created. In addition, this shader includes a very rudimentary lighting method.


\subsection{View Dependent Shader}
There are two versions for the view dependent shader. One for sphere mapping, and one for box projection. The main difference between these two and the shader used for the other methods is the uv-coordinate generation. Normally, each vertex has its own texture coordinates baked into the object. However, both view-dependent shaders can not use these coordinates since they do not use the original texture. Both shaders differ in the fragment shader stage and work primarily with fragments.


\subsubsection{Sphere Mapping}
The sphere mapping shader calculates the uv-coordinates from the fragments position and the predefined sphere center. Both values are in object space. From these two values an offset vector is calculated and subsequently converted into sphere coordinates. The resulting polar and elevation angles are normalized before they can then be used directly as texture lookup values.

\subsubsection{Image Mapping}

In addition to selecting the correct texture mapping for a fragment, this shader also has to select the correct texture from the list of possible sides.

The correct texture is chosen based on the normal vector of each fragment. The sphere around the fragment is divided into six regions, corresponding to the six precomputed textures. The texture from the region the vector points into is then used. The process is visualized by figure \ref{fig:fragmentBoxMapping}.

The texture coordinates within the chosen side depend on the fragments position in object space. Similar to sphere mapping the offset vector between the fragments position and a predefined point in space is computed. For box projection, the predefined point is not the geometric center but rather the upper left corner of the bounding box in object space. This vector is then scaled so that all of its axis are ranging from zero to one. Afterwards, two values of this vector are used directly to query the texture. The used values depend on the selected texture. For example the texture facing in the forward direction only needs the x and y dimensions of the vector, z can be discarded in this case. The resulting color value can then be used by the shader as normal.

\begin{figure}
\makebox[\textwidth][c]{
\includegraphics[width=0.3\textwidth]{fragmentBoxMapping}
}
\caption{Example for texture selection used in box projection. The orange dot represents a shader fragment, the black box the surrounding textures, and the arrow the surface normal. The texture the arrow points towards is chosen.}
\label{fig:fragmentBoxMapping}
\end{figure}