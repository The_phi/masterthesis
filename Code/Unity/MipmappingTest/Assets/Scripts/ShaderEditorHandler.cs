﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderEditorHandler : MonoBehaviour
{
    [Range(0.0f, 9.0f)]
    public float m_mipLevelAlbedo = 1;

    public List<Material> m_materials;

    void OnValidate()
    {
        setShaderValues();
    }

    public void setShaderValues()
    {
        foreach (Material mat in m_materials)
        {
            mat.SetFloat("m_MipLevelAlbedo", m_mipLevelAlbedo);
        }
    }

    public void setShaderValues(float _mipAlbedo)
    {
        m_mipLevelAlbedo = _mipAlbedo;
        setShaderValues();
    }
}
