#include "pch.h"
#include "ParamAwareFilter.h"

#include "Obj3D.h"
#include "Geometry.h"
#include <cmath>

float ParamAwareFilter::sign(float2 p1, float2 p2, float2 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool ParamAwareFilter::pointInTriangle(float2 pt, float2 v1, float2 v2, float2 v3)
{
	float d1, d2, d3;
	bool has_neg, has_pos;

	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}

bool isInInterval(float val, float x, float y)
{
	if (x > y)
	{
		if (val >= y && val <= x)
		{
			return true;
		}
	}
	else
	{
		if (val <= y && val >= x)
		{
			return true;
		}
	}
	return false;
}

///Calculate intersection of two lines.
///return true if found, false if not found or error
bool ParamAwareFilter::intersection(float2 p1, float2 p2, float2 v1, float2 v2, float2& out)
{
	float a1 = p1.y - p2.y;
	float b1 = p2.x - p1.x;
	float c1 = a1 * p1.x + b1 * p1.y;

	float a2 = v2.y - v1.y;
	float b2 = v1.x - v2.x;
	float c2 = a2 * v1.x + b2 * v1.y;

	float delta = a1 * b2 - a2 * b1;

	if (delta == 0)
	{
		return false;
	}
	else
	{
		out.x = (b2 * c1 - b1 * c2) / delta;
		out.y = (a1 * c2 - a2 * c1) / delta;
	}

	if (isInInterval(out.x, p1.x, p2.x) == false)
	{
		return false;
	}
	if (isInInterval(out.y, p1.y, p2.y) == false)
	{
		return false;
	}
	if (isInInterval(out.x, v1.x, v2.x) == false)
	{
		return false;
	}
	if (isInInterval(out.y, v1.y, v2.y) == false)
	{
		return false;
	}
	return true;
}

std::vector<float2> ParamAwareFilter::calculateIntersectionPoly(std::vector<float2> &rectangle, std::vector<float2> &triangle)
{
	std::vector<float2> intersectionPoints;

	// first compute all triangle rectangle intersections
	for (int i = 0; i < 3; ++i)
	{
		// get subject polygon edge
		float2 triP1 = triangle[i];
		float2 triP2 = triangle[(i + 1) % 3];

		for (int j = 0; j < 4; ++j)
		{
			float2 rectP1 = rectangle[j];
			float2 rectP2 = rectangle[(j + 1) % 4];

			float2 inter;

			if (intersection(rectP1, rectP2, triP1, triP2, inter))
			{
				intersectionPoints.push_back(inter);
				intersection(triP1, triP2, rectP1, rectP2, inter);
			}
		}
	}

	// compute all points inside rectangle
	int xBiggest = findBiggest(rectangle[0].x, rectangle[1].x, rectangle[2].x, rectangle[3].x);
	int xSmallest = findSmallest(rectangle[0].x, rectangle[1].x, rectangle[2].x, rectangle[3].x);
	int yBiggest = findBiggest(rectangle[0].y, rectangle[1].y, rectangle[2].y, rectangle[3].y);
	int ySmallest = findSmallest(rectangle[0].y, rectangle[1].y, rectangle[2].y, rectangle[3].y);
	for (int i = 0; i < 3; ++i)
	{
		if (triangle[i].x < xBiggest && triangle[i].x > xSmallest)
		{
			if (triangle[i].y < yBiggest && triangle[i].y > ySmallest)
			{
				intersectionPoints.push_back(triangle[i]);
			}
		}
	}


	// compute all points inside triangle
	for (int i = 0; i < 4; ++i)
	{
		if (pointInTriangle(rectangle[i], triangle[0], triangle[1], triangle[2]))
		{
			intersectionPoints.push_back(rectangle[i]);
		}
	}

	return intersectionPoints;
}

void ParamAwareFilter::computeTexelWeights(Obj3D& obj, int dimX, int dimY)
{
	std::vector<ObjTriangle> faces = obj.getFaces();
	float2 textureSize(dimX - 1, dimY - 1);

	m_texelCoverageMap.resize(dimX);
	for (int i = 0; i < dimX; ++i)
	{
		m_texelCoverageMap[i].resize(dimY);
	}

	float sizeOfAllTriangles = 0;

	// iterate over all faces
	for (int i = 0; i < faces.size(); ++i)
	{
		float2 texCoord1 = faces[i].vertex1UV;
		float2 texCoord2 = faces[i].vertex2UV;
		float2 texCoord3 = faces[i].vertex3UV;


		// calculate triangle areas in texture and in object space
		float triangleAreaTexSpace = GeometryMath::Triangle::getTriangleArea(texCoord1, texCoord2, texCoord3);
		float triangleAreaObjSpace = GeometryMath::Triangle::getTriangleArea3D(faces[i].vertex1, faces[i].vertex2, faces[i].vertex3);
		sizeOfAllTriangles += triangleAreaObjSpace;

		float2 texCoord1TexSpace = texCoord1 * textureSize;
		texCoord1TexSpace.y = textureSize.y - texCoord1TexSpace.y;
		keepCoordinatesInRange(texCoord1TexSpace, dimX, dimY);
		float2 texCoord2TexSpace = texCoord2 * textureSize;
		texCoord2TexSpace.y = textureSize.y - texCoord2TexSpace.y;
		keepCoordinatesInRange(texCoord2TexSpace, dimX, dimY);
		float2 texCoord3TexSpace = texCoord3 * textureSize;
		texCoord3TexSpace.y = textureSize.y - texCoord3TexSpace.y;
		keepCoordinatesInRange(texCoord3TexSpace, dimX, dimY);

		// find biggest and smalles texture coordinates
		int xBiggestTexCoord = findBiggest(texCoord1TexSpace.x, texCoord2TexSpace.x, texCoord3TexSpace.x);
		int yBiggestTexCoord = findBiggest(texCoord1TexSpace.y, texCoord2TexSpace.y, texCoord3TexSpace.y);
		int xSmallestTexCoord = findSmallest(texCoord1TexSpace.x, texCoord2TexSpace.x, texCoord3TexSpace.x);
		int ySmallestTexCoord = findSmallest(texCoord1TexSpace.y, texCoord2TexSpace.y, texCoord3TexSpace.y);

		Triangle2D tri2D(texCoord1TexSpace, texCoord2TexSpace, texCoord3TexSpace);

		// iterate over all pixels
		handleTriangle(tri2D, xBiggestTexCoord, yBiggestTexCoord,
			xSmallestTexCoord, ySmallestTexCoord, triangleAreaObjSpace, triangleAreaTexSpace);
	}

	// compute final texel coverage
	for (int y = 0; y < dimX; ++y)
	{
		for (int x = 0; x < dimX; ++x)
		{
			TexelCoverage total(0, 0, 0, 0);
			std::vector<TexelCoverage>& texelData = m_texelCoverageMap[x][y];
			for (int i = 0; i < texelData.size(); ++i)
			{
				total.areaTexSpace += texelData[i].areaTexSpace;
				total.areaObjSpace += texelData[i].areaObjSpace;
				total.areaTriangleTexSpace += texelData[i].areaTriangleTexSpace;
				total.areaTriangleObjSpace += texelData[i].areaTriangleObjSpace;
			}
			texelData.push_back(total);
		}
	}
}

int ParamAwareFilter::handleTriangle(Triangle2D& _triangle, int xBiggestTexCoord, int yBiggestTexCoord, int xSmallestTexCoord, int ySmallestTexCoord, 
	float triangleAreaObjSpace, float triangleAreaTexSpace)
{
	int intercections = 0;

	std::vector<float2> tri;
	tri.push_back(_triangle.point1);
	tri.push_back(_triangle.point2);
	tri.push_back(_triangle.point3);

	// iterate over all pixels
	for (int y = ySmallestTexCoord; y <= yBiggestTexCoord; ++y)
	{
		for (int x = xSmallestTexCoord; x <= xBiggestTexCoord; ++x)
		{
			float2 rectPoint1(x, y);
			float2 rectPoint2(x + 1, y);
			float2 rectPoint3(x + 1, y + 1);
			float2 rectPoint4(x, y + 1);

			//Rectangle2D rect2D(rectPoint1, rectPoint2, rectPoint3, rectPoint4);
			std::vector<float2> rect;
			rect.push_back(rectPoint1);
			rect.push_back(rectPoint2);
			rect.push_back(rectPoint3);
			rect.push_back(rectPoint4);


			// calculate pixel size in texture space
			std::vector<float2> overlap = calculateIntersectionPoly(rect, tri);

			float areaOverlap = 0;
			int countOverlap = overlap.size();

			if (countOverlap > 2)
			{
				calculateIntersectionPoly(rect, tri);
				for (int i = 2; i <= countOverlap; ++i)
				{
					int idx0 = i - 2;
					int idx1 = i - 1;
					int idx2 = i % countOverlap;
					areaOverlap += GeometryMath::Triangle::getTriangleArea(overlap[idx0], overlap[idx1], overlap[idx2]);
				}

				float areaOverlapObjSpace = (areaOverlap / triangleAreaTexSpace) * triangleAreaObjSpace;
				TexelCoverage coverage(areaOverlap, areaOverlapObjSpace,
					triangleAreaTexSpace, triangleAreaObjSpace);

				m_texelCoverageMap[x][y].push_back(coverage);
			}
		}
	}

	return intercections;
}

void ParamAwareFilter::calculateDownScaledImage(const int _scalingFactor, const Image& _baseImage, const Image& _targetImage)
{
	const size_t dimX = _baseImage.width;
	const size_t dimY = _baseImage.height;

	const uint8_t* _pSrcImg = _baseImage.pixels;
	uint8_t* _pDstImg = _targetImage.pixels;

	const size_t rowPitchOrig = _baseImage.rowPitch;
	const size_t pixelSize = rowPitchOrig / dimX;

	const size_t rowPitchDst = _targetImage.rowPitch;

	// iterate over original image
	for (int y = 0; y < dimY; y += _scalingFactor)
	{
		for (int x = 0; x < dimX; x += _scalingFactor)
		{
			double red = 0;
			double green = 0;
			double blue = 0;

			double totalCoverage = 0;

			// iterate over all pixels in filter
			for (int xFilter = x; xFilter < x + _scalingFactor; ++xFilter)
			{
				for (int yFilter = y; yFilter < y + _scalingFactor; ++yFilter)
				{
					std::vector<TexelCoverage>& texelData = m_texelCoverageMap[xFilter][yFilter];
					if (texelData.size() > 0)
					{
						// compute total coverage of all pixles in filter
						totalCoverage += texelData[texelData.size() - 1].areaObjSpace;
					}
				}
			}

			// iterate over all pixels in filter
			for (int xFilter = x; xFilter < x + _scalingFactor; ++xFilter)
			{
				for (int yFilter = y; yFilter < y + _scalingFactor; ++yFilter)
				{
					std::vector<TexelCoverage>& texelData = m_texelCoverageMap[xFilter][yFilter];
					if (texelData.size() == 0)
					{
						continue;
					}


					int srcPixelIndex = yFilter * rowPitchOrig + xFilter * pixelSize;
					uint8_t redTexel = _pSrcImg[srcPixelIndex + 2];
					uint8_t greenTexel = _pSrcImg[srcPixelIndex + 1];
					uint8_t blueTexel = _pSrcImg[srcPixelIndex + 0];

					// compute new color based on texel weights
					double factor = (texelData[texelData.size() - 1].areaObjSpace / totalCoverage);
					red += redTexel * factor;
					green += greenTexel * factor;
					blue += blueTexel * factor;
				}
			}

			// store new color in destination image
			int dstY = (y / _scalingFactor);
			int dstX = (x / _scalingFactor);
			int dstPixelIndex = dstY * rowPitchDst + dstX * pixelSize;
			_pDstImg[dstPixelIndex + 2] = (uint8_t)red;
			_pDstImg[dstPixelIndex + 1] = (uint8_t)green;
			_pDstImg[dstPixelIndex + 0] = (uint8_t)blue;
			_pDstImg[dstPixelIndex + 3] = 255;
		}

	}
}

int ParamAwareFilter::findBiggest(float a, float b, float c)
{
	float biggest = a;
	if (b > biggest)
	{
		biggest = b;
	}
	if (c > biggest)
	{
		biggest = c;
	}
	return ceil(biggest);
}

int ParamAwareFilter::findSmallest(float a, float b, float c)
{
	float smallest = a;
	if (b < smallest)
	{
		smallest = b;
	}
	if (c < smallest)
	{
		smallest = c;
	}
	return floor(smallest);
}

int ParamAwareFilter::findSmallest(float a, float b, float c, float d)
{
	float smallest = a;
	if (b < smallest)
	{
		smallest = b;
	}
	if (c < smallest)
	{
		smallest = c;
	}
	if (d < smallest)
	{
		smallest = d;
	}
	return smallest;
}

int ParamAwareFilter::findBiggest(float a, float b, float c, float d)
{
	float biggest = a;
	if (b > biggest)
	{
		biggest = b;
	}
	if (c > biggest)
	{
		biggest = c;
	}
	if (d > biggest)
	{
		biggest = d;
	}
	return biggest;
}

void ParamAwareFilter::keepCoordinatesInRange(float2& _coordinates, int _dimX, int _dimY)
{
	if (_coordinates.x > _dimX)
	{
		_coordinates.x = _coordinates.x - _dimX;
	}
	
	if (_coordinates.x < 0)
	{
		_coordinates.x = _coordinates.x + _dimX;
	}
	
	if (_coordinates.y > _dimY)
	{
		_coordinates.y = _coordinates.y - _dimY;
	}
	
	if (_coordinates.y < 0)
	{
		_coordinates.y = _coordinates.y + _dimY;
	}
}