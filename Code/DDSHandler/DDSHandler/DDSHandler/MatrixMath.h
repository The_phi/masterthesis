#pragma once
#include <vector>
#include <Eigen/Dense>

// multipy from https://www.geeksforgeeks.org/c-program-multiply-two-matrices/

using namespace std;

class MatrixMath
{
public:
	static Eigen::ArrayXXd& rdivide(const Eigen::ArrayXXd& _A, const Eigen::ArrayXXd& _B);
	static void setZeroOnSmaller(Eigen::ArrayXXd& _mat, const Eigen::ArrayXXd& _comp, double _e);
};