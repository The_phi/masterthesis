﻿Shader "Custom/Unlit/ViewDepMapping"
{
    Properties
    {
		m_FrontTex("Front Tex", 2D) = "white" {}
		m_BackTex("Back Tex", 2D) = "white" {}
		m_LeftTex("Left Tex", 2D) = "white" {}
		m_RightTex("Right Tex", 2D) = "white" {}
		m_TopTex("Top Tex", 2D) = "white" {}
		m_BottomTex("Bottom Tex", 2D) = "white" {}
		m_bounds("bounding box size", Vector) = (1,1,1,1)
		m_centerOffset("center Offset", Vector) = (1,1,1,1)
		m_MipLevelAlbedo("Mip Level Albedo", Range(0,9)) = 1
		m_MainLightDirection("MainLightDirection", Vector) = (0,0,0,0)
		m_LightColor("LightColor", Color) = (1,1,1,1)
		m_AmbientLightStrength("Ambient Light strength",  Range(0.0,1.0)) = 0.1
		m_useMipSelector("Use mip selector slider",  Range(0.0,1.0)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #include "UnityCG.cginc"
            #pragma vertex vert
            #pragma fragment frag


			sampler2D m_FrontTex;
			sampler2D m_BackTex;
			sampler2D m_LeftTex;
			sampler2D m_RightTex;
			sampler2D m_TopTex;
			sampler2D m_BottomTex;

			uniform fixed m_MipLevelAlbedo;

			float4 m_bounds;
			float4 m_centerOffset;

			uniform float4 m_LightColor;
			uniform float3 m_MainLightDirection;
			float m_AmbientLightStrength;

			uniform float m_useMipSelector;

            struct VS_IN
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float3 tangent : TANGENT;
            };

			struct VS_OUT
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float3 worldPos : TEXCOORD1;
				float3 mappingCoord : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				float3 lightdir : TEXCOORD4;
            };

			float3 GetWeights(float3 _normal) {
				float3 triW = abs(_normal);
				return _normal / (triW.x + triW.y + triW.z);
			}

			fixed4 getFrontCol(float3 _direction)
			{
				float3 front = _direction;
				front.x = front.x - m_centerOffset.x;
				front.y = front.y - m_centerOffset.y;
				front = front / m_bounds;
				front.x = 1 - front.x;
				float4 texCoord = float4(0, 0, 0, 0);
				texCoord.xy = front.xy;
				texCoord.w = m_MipLevelAlbedo;
				if (m_useMipSelector > 0.5f)
				{
					return tex2Dlod(m_FrontTex, texCoord).rgba;
				}
				else
				{
					return tex2D(m_FrontTex, texCoord.xy).rgba;
				}
			}
			
			fixed4 getBackCol(float3 _direction)
			{
				float3 back = _direction;
				back.x = back.x - m_centerOffset.x;
				back.y = back.y - m_centerOffset.y;
				back = back / m_bounds;
				float4 texCoord = float4(0, 0, 0, 0);
				texCoord.xy = back.xy;
				texCoord.w = m_MipLevelAlbedo;
				if (m_useMipSelector > 0.5f)
				{
					return tex2Dlod(m_BackTex, texCoord).rgba;
				}
				else
				{
					return tex2D(m_BackTex, texCoord.xy).rgba;
				}
			}

			fixed4 getLeftCol(float3 _direction)
			{
				float3 left = _direction;
				left.z = left.z - m_centerOffset.z;
				left.y = left.y - m_centerOffset.y;
				left = left / m_bounds;
				float4 texCoord = float4(0, 0, 0, 0);
				texCoord.xy = left.zy;
				texCoord.w = m_MipLevelAlbedo;
				if (m_useMipSelector > 0.5f)
				{
					return tex2Dlod(m_LeftTex, texCoord).rgba;
				}
				else
				{
					return tex2D(m_LeftTex, texCoord.xy).rgba;
				}

			}
			
			fixed4 getRightCol(float3 _direction)
			{
				float3 right = _direction;
				right.z = right.z - m_centerOffset.z;
				right.y = right.y - m_centerOffset.y;
				right = right / m_bounds;
				right.z = 1 - right.z;
				float4 texCoord = float4(0, 0, 0, 0);
				texCoord.xy = right.zy;
				texCoord.w = m_MipLevelAlbedo;
				if (m_useMipSelector > 0.5f)
				{
					return tex2Dlod(m_RightTex, texCoord).rgba;
				}
				else
				{
					return tex2D(m_RightTex, texCoord.xy).rgba;
				}
			}
			
			fixed4 getTopCol(float3 _direction)
			{
				float3 top = _direction;
				top.xz = top.xz - m_centerOffset.xz;
				top = top / m_bounds;
				float4 texCoord = float4(0, 0, 0, 0);
				texCoord.xy = top.xz;
				texCoord.w = m_MipLevelAlbedo;
				if (m_useMipSelector > 0.5f)
				{
					return tex2Dlod(m_TopTex, texCoord).rgba;
				}
				else
				{
					return tex2D(m_TopTex, texCoord.xy).rgba;
				}
			}

			fixed4 getBottomCol(float3 _direction)
			{
				float3 bottom = _direction;
				bottom.xz = bottom.xz - m_centerOffset.xz;
				bottom = bottom / m_bounds;
				bottom.z = 1 - bottom.z;
				float4 texCoord = float4(0, 0, 0, 0);
				texCoord.xy = bottom.xz;
				texCoord.w = m_MipLevelAlbedo;
				if (m_useMipSelector > 0.5f)
				{
					return tex2Dlod(m_BottomTex, texCoord).rgba;
				}
				else
				{
					return tex2D(m_BottomTex, texCoord.xy).rgba;
				}
			}

			float3 calculateViewDependantColor(VS_OUT input)
			{
				fixed4 colTop = getTopCol(input.mappingCoord);
				fixed4 colBottom = getBottomCol(input.mappingCoord);
				fixed4 colFront = getFrontCol(input.mappingCoord);
				fixed4 colBack = getBackCol(input.mappingCoord);
				fixed4 colRight = getRightCol(input.mappingCoord);
				fixed4 colLeft = getLeftCol(input.mappingCoord);

				float3 sampleVec = input.normal;
				float3 weights = GetWeights(sampleVec);

				float3 albedoSum = float3(0, 0, 0);


				if (abs(weights.x) > abs(weights.y) && abs(weights.x) > abs(weights.z))
				{
					if (weights.x >= 0)
					{
						albedoSum = colLeft.rgb;
					}
					else
					{
						albedoSum = colRight.rgb;
					}
				}
				else if (abs(weights.y) > abs(weights.x) && abs(weights.y) > abs(weights.z))
				{
					if (weights.y >= 0)
					{
						albedoSum = colTop.rgb;
					}
					else
					{
						albedoSum = colBottom.rgb;
					}
				}
				else if (abs(weights.z) > abs(weights.x) && abs(weights.z) > abs(weights.y))
				{
					if (weights.z >= 0)
					{
						albedoSum = colFront.rgb;
					}
					else
					{
						albedoSum = colBack.rgb;
					}
				}

				return albedoSum;
			}


			///////////////////////////////////
			///////////SHADER CODE ////////////
			///////////////////////////////////

			VS_OUT vert (VS_IN input)
            {
				VS_OUT output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.uv = input.uv;
				output.normal = input.normal;
				output.worldPos = mul(unity_ObjectToWorld, input.vertex);
				output.mappingCoord = input.vertex;
				output.viewDir = normalize(UnityWorldSpaceViewDir(output.worldPos));

				// calc lightDir vector heading current vertex
				output.lightdir = normalize(m_MainLightDirection);

                return output;
            }

            fixed4 frag (VS_OUT input) : COLOR
            {
				float3 albedoSum = calculateViewDependantColor(input);

				float3 albedo = calculateViewDependantColor(input);
				float3 lightDir = normalize(input.lightdir);
				float3 diffuse = saturate(dot(input.normal, -lightDir));
				diffuse = m_LightColor * albedo * diffuse;

				float3 ambient = float3(m_AmbientLightStrength, m_AmbientLightStrength, m_AmbientLightStrength) * albedo;

				// combine all of colors
				return fixed4(ambient + diffuse, 1);
            }
            ENDCG
        }
    }
	FallBack "Diffuse"
}
