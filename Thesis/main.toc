\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {american}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgments}{iii}{Doc-Start}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abstract}{iv}{chapter*.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Mipmapping}{1}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Aliasing Effects}{1}{section*.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Problems with Standard Mipmapping}{2}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}About this Thesis}{3}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Related Work}{4}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Image Comparison Metrics}{4}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}SSIM}{4}{subsection.2.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}FSIM}{4}{subsection.2.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}HDR-VDP-2}{5}{subsection.2.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Learned Perceptual Image Patch Similarity}{5}{subsection.2.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Mipmapping}{5}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Anisotropic Filtering}{6}{subsection.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Other LOD Techniques}{7}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Meshes}{7}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Simulations}{7}{subsection.2.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Fluids}{7}{section*.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Cloth}{7}{section*.9}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Techniques}{8}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Bilinear Down-sampling}{8}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Box Down-sampling}{8}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Kaiser Down-sampling}{9}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Lanczos Downsampling}{10}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Lanczos Filter}{10}{subsection.3.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}High Pass Filter Downsampling}{11}{section.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Perceptual Down-sampling}{11}{section.3.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.1}SSIM}{12}{subsection.3.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.2}Downsampling Problem}{13}{subsection.3.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.3}Downsampling}{13}{subsection.3.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.7}Learned Perceptual Image Patch Similarity}{14}{section.3.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.8}Parameterization Aware Downsampling}{15}{section.3.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.8.1}Parameterization Aware Filtering}{15}{subsection.3.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.8.2}Implementation}{17}{subsection.3.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.9}Viewdependant Downsampling}{18}{section.3.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.9.1}Sphere Mapping}{18}{subsection.3.9.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Texture Resolution}{18}{section*.19}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.9.2}Box Mapping}{19}{subsection.3.9.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Problems}{19}{section*.20}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.9.3}Overhang Problem}{19}{subsection.3.9.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.9.4}Solutions}{21}{subsection.3.9.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Implementation}{22}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Mip Generation Process}{22}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Python}{22}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}General Problems}{23}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Lanczos Down-sampling}{23}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}High Pass Filter Down-sampling}{23}{section.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Perceptual Down-sampling}{23}{section.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.1}Linear Algebra}{24}{subsection.4.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.2}Perceptual Pseudo Code}{24}{subsection.4.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}LPIPS}{25}{section.4.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6.1}Mip Level Generation}{26}{subsection.4.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6.2}Problems}{26}{subsection.4.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.7}Parameterization Aware Down-sampling}{26}{section.4.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7.1}Texel Coverage}{26}{subsection.4.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7.2}Down-sampling}{27}{subsection.4.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7.3}Shortcomings}{27}{subsection.4.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.8}View-Dependent Mipmapping}{27}{section.4.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.8.1}Sphere Mapping}{27}{subsection.4.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Texture Generation}{28}{section*.25}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Final Sampling}{29}{section*.26}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.8.2}Box Mapping}{29}{subsection.4.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Preprocessing}{29}{section*.27}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Different Mipmapping Implementations}{29}{section*.28}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Final Sampling}{30}{section*.29}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.9}Shader}{30}{section.4.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.9.1}View Dependent Shader}{30}{subsection.4.9.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Sphere Mapping}{30}{section*.30}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Image Mapping}{31}{section*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Results}{32}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Comparison Methodology}{32}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Test Objects}{32}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Image Generation}{33}{subsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Methodology Issues}{33}{section*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Comparison Technique}{34}{subsection.5.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}General Findings}{34}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Performance Impacts}{36}{section.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Memory Usage}{36}{subsection.5.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}View-dependent Methods}{37}{section.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Projection Errors}{38}{section.5.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.5.1}Down-sampling Techniques for Box Projection}{39}{subsection.5.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Positive Results}{39}{section.5.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Limitations and Further Improvements}{45}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Projection Errors}{45}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Mesh Distortions}{45}{section.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Usage of Other Down-sampling Techniques}{46}{section.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}UV-projection Changes}{46}{section.6.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.5}Specific Colors for Directions}{46}{section.6.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.6}LPIPs Improvements}{46}{section.6.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Conclusion}{47}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Figures}{48}{chapter*.47}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Tables}{52}{chapter*.48}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{53}{chapter*.49}%
