// based on: https://github.com/ramenhut/image-resampler/blob/master/Source/Kernels/vnImageLanczos.cpp
//
// Copyright (c) 2002-2009 Joe Bertolami. All Right Reserved.
//
// vnImageLanczos.h
//
//   Redistribution and use in source and binary forms, with or without
//   modification, are permitted provided that the following conditions are met:
//
//   * Redistributions of source code must retain the above copyright notice, this
//     list of conditions and the following disclaimer.
//
//   * Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
//   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
//   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//  Additional Information:
//
//   For more information, visit http://www.bertolami.com.
//

#include "pch.h"
#include "LanczosFilter.h"

void LanczosFilter::LanczosKernel(const Image& _pSrcImage,
	double _fCoeffN, int _fX, int _fY, const Image& _pDstImage, const int _scalingFactor)
{
	float fSampleCount = 0;

	int iRadius = _fCoeffN;
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;
	const uint8_t* pSrcData = _pSrcImage.pixels;
	const size_t srcRowPitch = _pSrcImage.rowPitch;
	const size_t srcPixelSize = srcRowPitch / dimX;

	float redCol = 0;
	float greenCol = 0;
	float blueCol = 0;

	for (int j = -iRadius + 1; j <= iRadius; j++)
	{
		for (int i = -iRadius + 1; i <= iRadius; i++)
		{
			int iX = _fX + i;
			int iY = _fY + j;

			// if outside bounds
			if (iX < 0 || iY < 0 || iX >= dimX || iY >= dimY)
			{
				continue;
			}

			float fXDelta = (float)_fX - iX;
			float fYDelta = (float)_fY - iY;
			float fDistance = sqrtf(fXDelta * fXDelta + fYDelta * fYDelta);
			float fWeight = vnLanczosWeight(_fCoeffN, fabs(fXDelta)) *
				vnLanczosWeight(_fCoeffN, fabs(fYDelta));

			int srcPixelIndex = iY * srcRowPitch + iX * srcPixelSize;
			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];

			redCol += fWeight * redTexel;
			greenCol += fWeight * greenTexel;
			blueCol += fWeight * blueTexel;

			fSampleCount += fWeight;
		}
	}

	// Normalize our bicubic sum back to the valid pixel range
	float fScaleFactor = 1.0 / fSampleCount;
	redCol = fScaleFactor * redCol;
	greenCol = fScaleFactor * greenCol;
	blueCol = fScaleFactor * blueCol;

	// Write our weighted sum to our output
	uint8_t* pDstdata = _pDstImage.pixels;
	int dstPixelIndex = (_fY / _scalingFactor) * _pDstImage.rowPitch + (_fX / _scalingFactor) * srcPixelSize;
	pDstdata[dstPixelIndex + 2] = (uint8_t) redCol;
	pDstdata[dstPixelIndex + 1] = (uint8_t) greenCol;
	pDstdata[dstPixelIndex + 0] = (uint8_t) blueCol;
	pDstdata[dstPixelIndex + 3] = 255;
}

void LanczosFilter::lanczosDownsampling(const Image& _pSrcImage,
	double _fCoeffN, const Image& _pDstImage, const int _scalingFactor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;

	for (int y = 0; y < dimY / _scalingFactor; ++y)
	{
		int yDif = y * _scalingFactor;
		if (y % 2 == 1)
		{
			++yDif;
		}
		for (int x = 0; x < dimX / _scalingFactor; ++x)
		{
			int xDif = x * _scalingFactor;
			if (x % 2 == 1)
			{
				++xDif;
			}
			LanczosKernel(_pSrcImage, _fCoeffN, xDif, yDif, _pDstImage, _scalingFactor);
		}

	}
}
