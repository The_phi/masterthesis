#pragma once

#include <string>

typedef unsigned long long ULONG_PTR;

class TextureHandler;

class PngToDDsConverter
{
public:
	PngToDDsConverter(TextureHandler* _pTexHandler);
	~PngToDDsConverter();

	void convertFolder(std::string _folderPath);

private:
	ULONG_PTR m_gdiplusToken;
	TextureHandler* m_pTexHandler;
};