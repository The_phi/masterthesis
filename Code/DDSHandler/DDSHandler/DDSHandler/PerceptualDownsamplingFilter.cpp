#include "pch.h"
#include "PerceptualDownsamplingFilter.h"

#include "MatrixMath.h"

#include <iostream>
// for timekeeping
#include <chrono>

void PerceptualDownsamplingFilter::perceptualDownsampling(const Image& _pSrcImage, const Image& _pDstImage, int _scalingFactor, int _np)
{
	Eigen::ArrayXXi  imgMatRed(_pSrcImage.width, _pSrcImage.height);
	Eigen::ArrayXXi  imgMatGreen(_pSrcImage.width, _pSrcImage.height);
	Eigen::ArrayXXi  imgMatBlue(_pSrcImage.width, _pSrcImage.height);
	// width of one row in image in bytes
	const size_t rowPitch = _pSrcImage.rowPitch;
	const size_t pixelSize = rowPitch / _pSrcImage.width;

	// fill matrices
	loadImageIntoMatrices(_pSrcImage.pixels, imgMatRed, imgMatGreen, imgMatBlue,
		pixelSize, rowPitch, _pSrcImage.width, _pSrcImage.height);

	// compute downscaled matrices
	Eigen::ArrayXXi* redSmall = createPerceptualDownscaledMatrix(imgMatRed, _scalingFactor, 2);
	Eigen::ArrayXXi* greenSmall = createPerceptualDownscaledMatrix(imgMatGreen, _scalingFactor, 2);
	Eigen::ArrayXXi* blueSmall = createPerceptualDownscaledMatrix(imgMatBlue, _scalingFactor, 2);



	// store back in image
	size_t scaledWidth = _pDstImage.width;
	size_t scaledHeight = _pDstImage.height;
	size_t halfRowPitch = _pDstImage.rowPitch;
	storeMatricesInImage(_pDstImage.pixels, *redSmall, *greenSmall, *blueSmall,
		pixelSize, halfRowPitch, scaledWidth, scaledHeight);

	delete redSmall;
	delete greenSmall;
	delete blueSmall;
}

Eigen::ArrayXXi* PerceptualDownsamplingFilter::createPerceptualDownscaledMatrix(const Eigen::ArrayXXi& _baseMat, int _s, int _np)
{
	Eigen::ArrayXXd HSquare = _baseMat.pow(2).cast<double>();
	// divide by 256 to avoide integer overflow
	HSquare = HSquare / 256;
	Eigen::ArrayXXd& W = convValid(_baseMat.cast<double>(), 1);

	int npSqrt = (int)(std::sqrt(_np) + 0.5);
	Eigen::ArrayXXd& L = subSample(W, _s);

	Eigen::ArrayXXd& L2 = subSample(convValid(HSquare, _s), _s);
	Eigen::ArrayXXd& M = convValid(L, npSqrt);
	Eigen::ArrayXXd LSquare = L.pow(2);
	Eigen::ArrayXXd MSquare = M.pow(2);
	Eigen::ArrayXXd S4 = convValid(LSquare, npSqrt);
	Eigen::ArrayXXd Sl = S4 - MSquare;

	Eigen::ArrayXXd Sh = convValid(L2, npSqrt) - MSquare;

	Eigen::ArrayXXd R = MatrixMath::rdivide(Sh, Sl).sqrt();
	MatrixMath::setZeroOnSmaller(R, Sl, 0.000001);

	Eigen::ArrayXXd Im = Eigen::ArrayXXd::Ones(M.rows(), M.cols());
	Eigen::ArrayXXd& N = convFull(Im, npSqrt);

	Eigen::ArrayXXd& T = convFull(R * M, npSqrt);

	M = convFull(M, npSqrt);
	R = convFull(R, npSqrt);

	Eigen::ArrayXXd K = R * L;
	Eigen::ArrayXXd J = M + K + T;

	Eigen::ArrayXXi Jint = J.cast<int>();
	Eigen::ArrayXXd& D1 = MatrixMath::rdivide(Jint.cast<double>(), N);
	Eigen::ArrayXXi D = D1.cast<int>();
	Eigen::ArrayXXi* output = new Eigen::ArrayXXi(D);

	return output;
}

TextureMatrices PerceptualDownsamplingFilter::loadTextureIntoMatrices(const Image& _baseImage)
{
	size_t width = _baseImage.width;
	size_t height = _baseImage.height;

	Eigen::ArrayXXi* imgMatRed = new Eigen::ArrayXXi(width, height);
	Eigen::ArrayXXi* imgMatGreen = new Eigen::ArrayXXi(width, height);
	Eigen::ArrayXXi* imgMatBlue = new Eigen::ArrayXXi(width, height);
	// width of one row in image in bytes
	const size_t rowPitch = _baseImage.rowPitch;
	const size_t pixelSize = rowPitch / _baseImage.width;

	// fill matrices
	loadImageIntoMatrices(_baseImage.pixels, *imgMatRed, *imgMatGreen, *imgMatBlue,
		pixelSize, rowPitch, width, height);

	TextureMatrices matrices;
	matrices.imgRed = imgMatRed;
	matrices.imgGreen = imgMatGreen;
	matrices.imgBlue = imgMatBlue;

	return matrices;
}

void PerceptualDownsamplingFilter::loadImageIntoMatrices(const uint8_t* _pSrc, Eigen::ArrayXXi& _redMat, Eigen::ArrayXXi& _greenMat, Eigen::ArrayXXi& _blueMat,
	const size_t _pixelSize, const size_t _rowPitch, const size_t _width, const size_t _height)
{
	for (size_t y = 0; y < _height; ++y)
	{
		for (size_t x = 0; x < _width; ++x)
		{
			_redMat(x, y) = _pSrc[y * _rowPitch + x * _pixelSize + 2];
			_greenMat(x, y) = _pSrc[y * _rowPitch + x * _pixelSize + 1];
			_blueMat(x, y) = _pSrc[y * _rowPitch + x * _pixelSize + 0];
		}
	}
}

void PerceptualDownsamplingFilter::storeMatricesInImage(uint8_t* _pDst, const Eigen::ArrayXXi& _redMat, const Eigen::ArrayXXi& _greenMat, const Eigen::ArrayXXi& _blueMat,
	const size_t _pixelSize, const size_t _rowPitch, const size_t _width, const size_t _height)
{
	for (size_t y = 0; y < _height; ++y)
	{
		for (size_t x = 0; x < _width; ++x)
		{
			// red
			_pDst[y * _rowPitch + x * _pixelSize + 2] = (uint8_t)_redMat(x, y);
			//outPixels[y * halfRowPitch + x * pixelSize + 2] = 0;

			// green
			_pDst[y * _rowPitch + x * _pixelSize + 1] = (uint8_t)_greenMat(x, y);
			//outPixels[y * halfRowPitch + x * pixelSize + 1] = 0;

			// blue
			_pDst[y * _rowPitch + x * _pixelSize + 0] = (uint8_t)_blueMat(x, y);
			//outPixels[y * halfRowPitch + x * pixelSize + 0] = 0;

			// alpha
			_pDst[y * _rowPitch + x * _pixelSize + 3] = 255;
		}
	}
}

Eigen::ArrayXXd& PerceptualDownsamplingFilter::convValid(const Eigen::ArrayXXd& _mat, const int _kernelSize)
{
	const int rows = _mat.rows();
	const int columns = _mat.cols();

	const float avgFac = 1 / (float)(_kernelSize * _kernelSize);

	Eigen::ArrayXXd* result = new Eigen::ArrayXXd(rows, columns);

	// iterate image
	for (int x = 0; x < rows; ++x)
	{
		for (int y = 0; y < columns; ++y)
		{
			//int div = (int)(_kernelSize / 2.0f + 0.5);
			int div = _kernelSize / 2;
			int sum = 0;
			int counter = 0;

			for (int i = -div; i <= div; ++i)
			{
				for (int j = -div; j <= div; ++j)
				{
					if (x + i >= 0 && x + i < rows)
					{
						if (y + j >= 0 && y + j < columns)
						{
							sum += _mat(x + i, y + j);
							counter++;
						}
					}
				}
			}
			(*result)(x, y) = sum / (double)counter;
		}
	}

	return *result;
}

Eigen::ArrayXXd& PerceptualDownsamplingFilter::convFull(const Eigen::ArrayXXd& _mat, const int _kernelSize)
{
	const int rows = _mat.rows();
	const int columns = _mat.cols();

	float avgFac = 1 / (float)(_kernelSize * _kernelSize);

	Eigen::ArrayXXd* result = new Eigen::ArrayXXd(rows, columns);

	// iterate image
	for (int x = 0; x < rows; ++x)
	{
		for (int y = 0; y < columns; ++y)
		{
			//int div = (int)(_kernelSize / 2.0f + 0.5);
			int div = _kernelSize / 2;
			int sum = 0;

			for (int i = -div; i <= div; ++i)
			{
				for (int j = -div; j <= div; ++j)
				{
					int value = 0;
					if (x + i >= 0 && x + i < rows)
					{
						if (y + j >= 0 && y + j < columns)
						{
							value = _mat(x + i, y + j);
						}
					}
					sum += value;
				}
			}
			(*result)(x, y) = sum * avgFac;
		}
	}

	return *result;
}

Eigen::ArrayXXd& PerceptualDownsamplingFilter::subSample(const Eigen::ArrayXXd& _mat, const int _sampleDist)
{
	const int rows = _mat.rows();
	const int columns = _mat.cols();

	Eigen::ArrayXXd* result = new Eigen::ArrayXXd(rows / _sampleDist, columns / _sampleDist);

	for (int y = 0; y < columns / _sampleDist; ++y)
	{
		// fix for image sliding while subsampling
		int yDif = y * _sampleDist;
		if (y % 2 == 1)
		{
			++yDif;
		}
		for (int x = 0; x < rows / _sampleDist; ++x)
		{
			int xDif = x * _sampleDist;
			if (x % 2 == 1)
			{
				++xDif;
			}
			(*result)(x, y) = _mat(xDif, yDif);
		}

	}

	return *result;
}
