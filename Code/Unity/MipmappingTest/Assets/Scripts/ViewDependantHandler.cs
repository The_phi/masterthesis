﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode()]

public class ViewDependantHandler : MonoBehaviour
{
    private Bounds m_bounds;

    private Vector3 m_centerCorrectionOffset;

    public Material m_material;

    private float scaleFactor = 1;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        m_bounds = GetComponent<Renderer>().bounds;
        //m_material = GetComponent<Material>();
        float parentXScale = 1;
        if (transform.parent != null)
        {
            parentXScale = transform.parent.transform.localScale.x;

        }
        scaleFactor = 1 / parentXScale;

        Vector3 globalMeshCenter = transform.position;
        //globalMeshCenter = transform.TransformPoint(transform.position);
        Vector3 localBoundsCenter = transform.InverseTransformPoint(m_bounds.center);
        Vector3 extents = m_bounds.extents;
        extents *= scaleFactor;
        m_centerCorrectionOffset = localBoundsCenter;
        m_centerCorrectionOffset += extents;
        //m_centerCorrectionOffset *= scaleFactor;



        m_material.SetVector("m_centerOffset", new Vector4(m_centerCorrectionOffset.x, m_centerCorrectionOffset.y, m_centerCorrectionOffset.z, 1));


        //Vector3 extents = m_bounds.extents;
        extents *= 2;
        Vector4 totalBounds = new Vector4(extents.x, extents.y, extents.z, 1);
        m_material.SetVector("m_bounds", totalBounds);
    }
}
