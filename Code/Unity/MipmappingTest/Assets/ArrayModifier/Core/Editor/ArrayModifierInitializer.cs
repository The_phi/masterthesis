﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace ArrayModifier
{
    [InitializeOnLoad]
    public class ArrayModifierInitializer
    {
        static ArrayModifierInitializer()
        {
            EditorApplication.update += Initialize;
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            EditorSceneManager.sceneOpened += OnSceneOpened;
        }

        private static void Initialize()
        {
            var arrayModifiers = GameObject.FindObjectsOfType<ArrayModifier>();
            foreach (var arrayModifier in arrayModifiers)
            {
                arrayModifier.Calculate();
            }

            EditorApplication.update -= Initialize;
        }

        private static void OnSceneOpened(Scene scene, OpenSceneMode openSceneMode)
        {
            Initialize();
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange change)
        {
            Initialize();
        }
    }
}