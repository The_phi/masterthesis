# flips the numbers in a csv file
import os
import os.path
import argparse
from numpy import genfromtxt
import csv

# 2. Construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--path", required=True, help="path to base folder")
args = vars(ap.parse_args())

directory = args["path"]

for csvFile in os.listdir(directory):
    filePath = directory + "/" + csvFile
    print(filePath)
    data = genfromtxt(filePath, delimiter=',')

    with open(filePath,"w+") as csvFile:
        csvWriter = csv.writer(csvFile, delimiter=',')
        for i in data:
            csvWriter.writerow(["{:.3f}".format(x) for x in i])
