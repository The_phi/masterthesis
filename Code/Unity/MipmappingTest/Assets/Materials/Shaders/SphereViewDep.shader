﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Unlit/SphereViewDep"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		m_center("object center offset", Vector) = (1,1,1,1)
		m_MipLevelAlbedo("Mip Level Albedo", Range(0,9)) = 1
		m_MainLightDirection("MainLightDirection", Vector) = (0,0,0,0)
		m_LightColor("LightColor", Color) = (1,1,1,1)
		m_AmbientLightStrength("Ambient Light strength",  Range(0.0,1.0)) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
				float4 normal : NORMAL;
				float3 lightdir : TEXCOORD1;
				float4 fragmentPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 m_center;

			float m_PI = 3.14159265358979;

			uniform fixed m_MipLevelAlbedo;
			uniform float4 m_LightColor;
			uniform float3 m_MainLightDirection;
			float m_AmbientLightStrength;

			/// float2.x => polar
			/// float2.y => elevation
			/// polar is theta is x
			/// elevation is phi is y
			float2 CartesianToSpherical(float4 cartCoords)
			{
				cartCoords = normalize(cartCoords);
				float radius = 1;
				float x = cartCoords.x;
				float y = cartCoords.y;
				float z = cartCoords.z;

				float polar = acos(z / radius);
				float elevation = atan(y / x);

				float2 coordDeg = degrees(float2(polar, elevation));
				coordDeg.x = coordDeg.x * (1.0 / 360);
				coordDeg.y = coordDeg.y * (1.0 / 180);

				if (cartCoords.y < 0)
				{
					coordDeg.x = 1 - coordDeg.x;
				}


				return coordDeg;
			}

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal = v.normal;
				o.lightdir = normalize(m_MainLightDirection);
                o.fragmentPos = v.vertex;

                return o;
            }

			float4 frag (v2f i) : SV_Target
            {
				float4 fragOffset = i.fragmentPos - m_center;
				float2 uvCoord = CartesianToSpherical(fragOffset);
				float4 texCoord = float4(uvCoord,0, m_MipLevelAlbedo);
				
				float3 albedo = tex2Dlod(_MainTex, texCoord);
				float3 lightDir = normalize(i.lightdir);
				float3 diffuse = saturate(dot(i.normal, -lightDir));
				diffuse = m_LightColor * albedo * diffuse;
				float3 ambient = float3(m_AmbientLightStrength, m_AmbientLightStrength, m_AmbientLightStrength) * albedo;

				// combine all of colors
				return float4(ambient + diffuse, 1);
            }
            ENDCG
        }
    }
}
