from wand import image as wandImg
import os
import glob
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True, help="Path to conversion folder")
args = vars(ap.parse_args())

directory = args["first"]
data_path = os.path.join(directory,'*g')
files = glob.glob(data_path)
for filename in os.listdir(directory):
    fileExtension = filename[-4:]
    if fileExtension != ".dds":
        continue
    filePath = directory + "/" + filename
    conversionPath = directory + "/pngs/"
    if not os.path.exists(conversionPath):
        os.makedirs(conversionPath)
    conversionPath = conversionPath + filename[:-4] + ".png"
    
    
    print(filename)


    ddsImg = wandImg.Image(filename=filePath)
    ddsImg.compression = "no"
    ddsImg.save(filename=conversionPath)
    ddsImg.destroy()

