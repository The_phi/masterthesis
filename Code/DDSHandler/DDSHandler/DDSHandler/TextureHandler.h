#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

// Windows Runtime Library. Needed for Microsoft::WRL::ComPtr<> template class.
#include <wrl.h>

#include "Helper.h"
#include "DirectXTex/d3dx12.h"

using namespace Microsoft::WRL;

class ParamAwareFilter;
struct float3;

namespace DirectX
{
	class Image;
	class ScratchImage;
	struct TexMetadata;
}

class TextureHandler
{
public:
	TextureHandler();

	std::unique_ptr<DirectX::ScratchImage> loadTexture(std::wstring _fileName);
	void saveTexture(const std::unique_ptr<DirectX::ScratchImage>& _image, std::wstring _fileName);
	void saveTexture(const DirectX::Image* _image, std::wstring _fileName);
	void saveTexture(const DirectX::ScratchImage& _image, std::wstring _fileName);

	void createPerceptualDownscaledMipTexture(std::wstring _fileName, const int _mipLevels);
	void createLanczosFilterMipTexture(std::wstring _fileName, const int _mipLevels);
	void createHighPassFilterMipTexture(std::wstring _fileName, const int _mipLevels);
	void createParamAwareMipTexture(std::wstring _textureFileName, std::string _objFileName, const int _mipLevels);
	void createParameterAwareMipmappedImage(std::wstring fileName, const int _mipLevels, ParamAwareFilter* _pParamAwareFilter);

	void createViewDependantNormalMipImage(std::wstring _fileName, const int _mipLevels);
	void createViewDepBoxMipmappedImage(std::wstring _fileName, const int _mipLevels);
	void createViewDepColorMipmappedImage(std::wstring _fileName, const int _mipLevels, const float3 _avgColor);

	void saveMipMapsToFolder(std::wstring _fileName, std::wstring _folderName, DirectX::ScratchImage& _mipChain);

	void combineImagesInFolderToDDs(std::string _folderPath, std::wstring _ddsFileName);

private:
	HRESULT Setup2DMips(_In_reads_(nimages) const DirectX::Image* baseImages, _In_ size_t nimages, _In_ const DirectX::TexMetadata& mdata, _Out_ DirectX::ScratchImage& mipChain) noexcept;
	void SetupMipChain(const DirectX::Image* _pBaseImage, const int _mipLevels, DirectX::ScratchImage& _mipChain);
};