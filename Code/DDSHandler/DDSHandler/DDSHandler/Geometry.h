#pragma once

#include <math.h>

struct float2
{
	float x;
	float y;

	float2(float _x, float _y)
	{
		x = _x;
		y = _y;
	}

	float2()
	{
		x = 0;
		y = 0;
	}

	float2 operator+(const float2& a)
	{
		return float2(a.x + x, a.y + y);
	}
	
	float2 operator-(const float2& a)
	{
		return float2(a.x - x, a.y - y);
	}

	float2 operator*(const float a)
	{
		return float2(a * x, a * y);
	}
	
	float2 operator*(const float2& a)
	{
		return float2(a.x * x, a.y * y);
	}

	float2 crossProduct(const float2& a)
	{
		return float2(0, x * a.y - y * a.x);
	}

	float length()
	{
		return sqrt((x * x) + (y * y));
	}
};

struct float3
{
	float3(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}

	float3()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	float x;
	float y;
	float z;

	void normalize() {
		float magnitude = 0.0f;
		magnitude += pow(x, 2.0f);
		magnitude += pow(y, 2.0f);
		magnitude += pow(z, 2.0f);
		magnitude = sqrt(magnitude);
		x /= magnitude;
		y /= magnitude;
		z /= magnitude;
	}

	float3 operator+(const float3& a)
	{
		return float3(a.x + x, a.y + y, a.z + z);
	}

	float3 operator-(const float3& a)
	{
		return float3(a.x - x, a.y - y, a.z - z);
	}
	
	float3 operator/(const float a)
	{
		return float3(x / a, y / a, z / a);
	}
	
	float3 operator/(const int a)
	{
		return float3(x / a, y / a, z / a);
	}

	float3 crossProduct(const float3& a)
	{
		float3 val;
		val.x = y * a.z - z * a.y;
		val.y = z * a.x - x * a.z;
		val.z = x * a.y - y * a.x;
		return val;
	}

	/// euclidean norm between two vectors
	float norm(const float3& a)
	{
		float norm = std::powf(a.x - x, 2) + std::powf(a.y - y, 2) + std::powf(a.z - z, 2);
		return std::sqrtf(norm);
	}

	float length()
	{
		return sqrt((x * x) + (y * y));
	}
};

struct Triangle3D
{
	float3 vertex1;
	float3 vertex2;
	float3 vertex3;
};

struct Triangle2D
{
	Triangle2D(float2 p1, float2 p2, float2 p3) :
		point1(p1), point2(p2), point3(p3)
	{
	}

	float2 point1;
	float2 point2;
	float2 point3;
};

struct ObjTriangle
{
	float3 vertex1;
	float3 vertex2;
	float3 vertex3;
	float3 vertex1Normal;
	float3 vertex2Normal;
	float3 vertex3Normal;
	float2 vertex1UV;
	float2 vertex2UV;
	float2 vertex3UV;
};

struct Rectangle2D
{
	Rectangle2D(float2 p1, float2 p2, float2 p3, float2 p4) :
		point1(p1), point2(p2), point3(p3), point4(p4)
	{
	}

	float2 point1;
	float2 point2;
	float2 point3;
	float2 point4;
};

namespace GeometryMath
{
	static class Triangle
	{
	public:
		static float getTriangleArea(float2 _p0, float2 _p1, float2 _p2)
		{
			float2 val1 = _p1 - _p0;
			float2 val2 = _p2 - _p0;

			return 0.5f * val1.crossProduct(val2).length();
		}
	
		static float getTriangleArea3D(float3 _p0, float3 _p1, float3 _p2)
		{
			float3 val1 = _p1 - _p0;
			float3 val2 = _p2 - _p0;

			return 0.5f * val1.crossProduct(val2).length();
		}
	};
}