﻿Shader "Custom/Unlit/ViewDepMappingNormal"
{
	Properties
	{
		m_LightColor("LightColor", Color) = (1,1,1,1)
		m_MainTex("Albedo (RGB)", 2D) = "white" {}
		m_NormalMap("NormalMap", 2D) = "white" {}
		m_MipLevelAlbedo("Mip Level Albedo", Range(0,9)) = 1
		m_MipLevelNormal("Mip Level Normal", Range(0.0,9.0)) = 1
	}
		SubShader
		{
			Pass
			{
				Tags{ "RenderType" = "Opaque" }
				LOD 200

				CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vs_main
				#pragma fragment fs_main

				struct VS_IN
				{
					float4 position : POSITION;
					float3 normal : NORMAL;
					float3 tangent : TANGENT;
					float2 uv : TEXCOORD0;
				};

				struct VS_OUT
				{
					float4 position : POSITION;
					float2 uv : TEXCOORD0;
					float3 normal : NORMAL;

					// TANGENT, BINORMAL, NORMAL semantics are only available for input of vertex shader
					float3 T : TEXCOORD3;
					float3 B : TEXCOORD4;
					float3 N : TEXCOORD5;
				};

				uniform fixed m_MipLevelAlbedo;
				uniform fixed m_MipLevelNormal;

				uniform sampler m_MainTex;
				uniform sampler m_NormalMap;

				VS_OUT vs_main(VS_IN input)
				{
					VS_OUT o;

					// calc output position directly
					o.position = UnityObjectToClipPos(input.position);

					// pass uv coord
					o.uv = input.uv;

					o.normal = input.normal;

					// calc Normal, Binormal, Tangent vector in world space
				// cast 1st arg to 'float3x3' (type of input.normal is 'float3')
					float3 worldNormal = mul((float3x3)unity_ObjectToWorld, input.normal);
					float3 worldTangent = mul((float3x3)unity_ObjectToWorld, input.tangent);

					float3 binormal = cross(input.normal, input.tangent.xyz); // *input.tangent.w;
					float3 worldBinormal = mul((float3x3)unity_ObjectToWorld, binormal);

					// and, set them
					o.N = normalize(worldNormal);
					o.T = normalize(worldTangent);
					o.B = normalize(worldBinormal);

					return o;
				}

				float4 fs_main(VS_OUT input) : COLOR
				{
					// sample the texture
					float4 texCoord = float4(0,0,0,0);
					texCoord.xy = input.uv;

					// obtain a normal vector on tangent space
					float3 tangentNormal = tex2Dlod(m_NormalMap, texCoord).xyz;
					// and change range of values (0 ~ 1)
					tangentNormal = normalize(tangentNormal * 2 - 1);

					float3x3 TBN = float3x3(normalize(input.T), normalize(input.B), normalize(input.N));
					TBN = transpose(TBN);
					// finally we got a normal vector from the normal map
					float3 worldNormal = mul(TBN, tangentNormal);

					float3 finalNormal = input.normal;
					float3 worldFinalNormal = mul(TBN, finalNormal);
					//finalNormal += worldNormal;

					texCoord.w = m_MipLevelAlbedo;
					// Lambert here (cuz we're calculating Normal vector in this pixel shader)
					texCoord.w = m_MipLevelAlbedo;
					float4 albedo = tex2Dlod(m_MainTex, texCoord);

					worldNormal = normalize(worldNormal);
					worldNormal = worldNormal + 1;
					worldNormal = worldNormal / 2;

					// combine all of colors
					return float4(worldNormal, 1);
				}
				ENDCG
			}
		}
			FallBack "Diffuse"
}
