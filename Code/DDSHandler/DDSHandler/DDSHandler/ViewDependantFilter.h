#pragma once

#include <cmath>
// D3D12 extension library.
#include "DirectXTex/d3dx12.h"
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"

using namespace DirectX;

struct float3;

class ViewDependantFilter
{
public:

	static void ViewDependantNormalDownsampling(const Image& _pSrcImage, const Image& _pSrcNormalImage,
		const Image& _pDstImage, const int _scalingFactor);

	static void ViewDepBoxDownsampling(const Image& _pSrcImage, const Image& _pDstImage, const int _scalingFactor);
	static void ViewDepColorDownsampling(const Image& _pSrcImage, const Image& _pDstImage, const int _scalingFactor, const float3 _avgColor);

private:
	static void ViewDependantNormalKernel(const Image& _pSrcImage, const Image& _pSrcNormalImage,
		int _fX, int _fY, const Image& _pDstImage, const int _scalingFactor);

	static void ViewDepBoxKernel(const Image& _pSrcImage,
		int _x, int _y, const Image& _pDstImage, const int _scalingFactor);

	static void ViewDepColorKernel(const Image& _pSrcImage, const float3 _avgColor,
		int _x, int _y, const Image& _pDstImage, const int _scalingFactor);

};
#pragma once
