﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
// based on https://gist.github.com/enghqii/eb99185fc029b9eb46fa

Shader "Custom/Unlit/TextureSelector"
{
	Properties{
		m_LightColor("LightColor", Color) = (1,1,1,1)
		m_MainTex("Albedo (RGB) 1", 2D) = "white" {}
		m_SecondMainTex("Albedo (RGB) 2", 2D) = "white" {}
		m_NormalMap("NormalMap", 2D) = "white" {}
		m_TextureSelection("Switch between Albedo maps",  Range(0.0,1.0)) = 0.0
		m_MipLevelAlbedo("Mip Level Albedo", Range(0,9)) = 1
		m_MipLevelNormal("Mip Level Normal", Range(0.0,9.0)) = 1
		m_NormalFactor("Normal mapping strength", Range(0.0,1.0)) = 1
		m_UseMipLvlInterpolation("Use mip level interpolation", Int) = 1
		m_MainLightDirection("MainLightDirection", Vector) = (0,0,0,0)
		m_AmbientLightStrength("Ambient Light strength",  Range(0.0,1.0)) = 0.1
	}
		SubShader
		{
			Pass
			{
				Tags{ "RenderType" = "Opaque" }
				LOD 200

				CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vs_main
				#pragma fragment fs_main

				struct VS_IN
				{
					float4 position : POSITION;

					float3 normal : NORMAL;
					float3 tangent : TANGENT;
					// float3 binormal : BINORMAL; // unity does not support BINORMAL semantic?
					float2 uv : TEXCOORD0;
				};

				struct VS_OUT
				{
					float4 position : POSITION;
					float2 uv : TEXCOORD0;
					float3 lightdir : TEXCOORD1;
					float3 viewdir : TEXCOORD2;

					float3 T : TEXCOORD3;
					float3 B : TEXCOORD4;
					float3 N : TEXCOORD5;

					// TANGENT, BINORMAL, NORMAL semantics are only available for input of vertex shader
				};

				uniform float4 m_LightColor;
				float m_AmbientLightStrength;
				uniform fixed m_MipLevelAlbedo;
				uniform fixed m_MipLevelNormal;
				float m_NormalFactor;
				uniform int m_UseMipLvlInterpolation;

				uniform float3 m_MainLightDirection;

				uniform sampler m_MainTex;
				uniform sampler m_SecondMainTex;
				uniform sampler m_NormalMap;
				float m_TextureSelection;

				VS_OUT vs_main(VS_IN input)
				{
					VS_OUT output;

					// calc output position directly
					output.position = UnityObjectToClipPos(input.position);

					// pass uv coord
					output.uv = input.uv;

					// calc lightDir vector heading current vertex
					output.lightdir = normalize(m_MainLightDirection);

					// calc viewDir vector 
					float4 worldPosition = mul(unity_ObjectToWorld, input.position);
					float3 viewDir = normalize(worldPosition.xyz - _WorldSpaceCameraPos.xyz);
					output.viewdir = viewDir;

					// calc Normal, Binormal, Tangent vector in world space
					// cast 1st arg to 'float3x3' (type of input.normal is 'float3')
					float3 worldNormal = mul((float3x3)unity_ObjectToWorld, input.normal);
					float3 worldTangent = mul((float3x3)unity_ObjectToWorld, input.tangent);

					float3 binormal = cross(input.normal, input.tangent.xyz); // *input.tangent.w;
					float3 worldBinormal = mul((float3x3)unity_ObjectToWorld, binormal);

					// and, set them
					output.N = normalize(worldNormal);
					output.T = normalize(worldTangent);
					output.B = normalize(worldBinormal);

					return output;
				}

				float4 fs_main(VS_OUT input) : COLOR
				{
					// sample the texture
					float4 texCoord = float4(0,0,0,0);
					texCoord.xy = input.uv;
					if (m_UseMipLvlInterpolation > 0)
					{
						texCoord.w = m_MipLevelNormal;

					}
					else
					{
						texCoord.w = (int)m_MipLevelNormal;
					}

					// obtain a normal vector on tangent space
					float3 tangentNormal = tex2Dlod(m_NormalMap, texCoord).xyz;
					// and change range of values (0 ~ 1)
					tangentNormal = normalize(tangentNormal * 2 - 1);
					tangentNormal = lerp(float3(0.5, 0.5, 1), tangentNormal, m_NormalFactor);

					// 'TBN' transforms the world space into a tangent space
					// we need its inverse matrix
					// Tip : An inverse matrix of orthogonal matrix is its transpose matrix
					float3x3 TBN = float3x3(normalize(input.T), normalize(input.B), normalize(input.N));
					TBN = transpose(TBN);

					// finally we got a normal vector from the normal map
					float3 worldNormal = mul(TBN, tangentNormal);

					if (m_UseMipLvlInterpolation > 0)
						texCoord.w = m_MipLevelAlbedo;
					else
						texCoord.w = (int)m_MipLevelAlbedo;
					// Lambert here (cuz we're calculating Normal vector in this pixel shader)
					texCoord.w = m_MipLevelAlbedo;

					// albedo interpolation
					float4 albedo = tex2Dlod(m_MainTex, texCoord);
					float4 albedo2 = tex2Dlod(m_SecondMainTex, texCoord);
					albedo = albedo * m_TextureSelection + albedo2 * (1 - m_TextureSelection);

					float3 lightDir = normalize(input.lightdir);
					// calc diffuse, as we did in pixel shader
					float3 diffuse = saturate(dot(worldNormal, -lightDir));
					diffuse = m_LightColor * albedo.rgb * diffuse;

					// make some ambient,
					float3 ambient = float3(m_AmbientLightStrength, m_AmbientLightStrength, m_AmbientLightStrength) * albedo;

					// combine all of colors
					return float4(ambient + diffuse, 1);
				}
				ENDCG
			}
		}
			FallBack "Diffuse"
}