import argparse
import imutils
import cv2
import numpy as np
from PIL import Image
#from wand import image as wandImg


# needed to load perceptal nn
import sys
sys.path.insert(0, r"C:\Users\Cortana\Desktop\UniZeug\masterThesis\Code\python\PerceptualSimilarity")
from util import util
import models

import torch
import torchvision
import torch

def tensor2im(image_tensor, imtype=np.uint8, cent=1., factor=255./2.):
    image_numpy = image_tensor[0].cpu().float().numpy()
    image_numpy = np.clip(image_numpy, -1, 1)
    image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + cent) * factor
    return image_numpy.astype(imtype)



ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True, help="image path")
args = vars(ap.parse_args())
argFirst = args["first"]

albedoImg = cv2.imread(argFirst).astype(np.float32)
albedo = util.im2tensor(albedoImg).to(torch.device('cuda:0'))
origTensor = albedo

tensShape = origTensor.shape



# load nn
loss_fn = models.PerceptualLoss(model='net-lin', net='alex', use_gpu=True)
learningRate = 0.5

# write original image as base mip level
cv2.imwrite("mip0.png", albedoImg)
# create mip maps
scaleFactor = 0.5
for i in range(0, 10):
    # scale image down
    scaledTensor = torch.nn.functional.interpolate(origTensor, scale_factor=scaleFactor,
            mode="bicubic", align_corners=False)
    scaledTensor.requires_grad_(True)
    
    optimizer = torch.optim.SGD([scaledTensor], lr=learningRate, momentum=0.9, weight_decay=0.0001)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.1)
    #optimizer = torch.optim.Adam([scaledTensor], lr=learningRate, betas=(0.9, 0.999))
    for a in range(450):
        # scale tensot up then Compute loss
        upscaledTensor = torch.nn.functional.interpolate(scaledTensor, size=tensShape[2:],
                mode="bicubic", align_corners=False)
        perceptualLoss = torch.mean(loss_fn.forward(upscaledTensor, origTensor, normalize=True))
        lossValue = perceptualLoss.item()

        lambdaVal = torch.tensor(1.)
        l2_reg = torch.tensor(0.)
        for param in loss_fn.parameters():
            l2_reg += torch.norm(param)
        perceptualLoss += lambdaVal * l2_reg

        perceptualLoss.backward()
        optimizer.step()
        #optimizer.zero_grad()
        torch.clamp(scaledTensor, -1, 1)
        scheduler.step(metrics=lossValue)
        print(lossValue)

    # save to file
    scaledNpArray = scaledTensor.cpu().detach()
    scaledNpArray = tensor2im(scaledNpArray)
    cv2.imwrite("mip" + str(i + 1) + ".png", scaledNpArray)

    scaleFactor = scaleFactor / 2

    if(learningRate < 0.1):
        learningRate = learningRate * 10

    print("miplevel done")
    print("-----------------")

    # clear old unecessary data
    del scaledTensor
    torch.cuda.empty_cache()