#include "pch.h"
#include "HighPassFilter.h"

void HighPassFilter::highPassKernel(const Image& _pSrcImage,
	int _kernelSize, int _x, int _y, const Image& _pDstImage, const int _scalingFactor)
{
	float fSampleCount = 0;

	//
	// Scan the kernel space adding up the bicubic weights and pixel values
	//

	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;
	const uint8_t* pSrcData = _pSrcImage.pixels;
	const size_t srcRowPitch = _pSrcImage.rowPitch;
	const size_t srcPixelSize = srcRowPitch / dimX;

	float redCol = 0;
	float greenCol = 0;
	float blueCol = 0;

	int kernelSize = _kernelSize * _kernelSize;

	for (int j = -_kernelSize + 1; j <= _kernelSize; j++)
	{
		for (int i = -_kernelSize + 1; i <= _kernelSize; i++)
		{
			int deltaX = _x + i;
			int deltaY = _y + j;

			// if outside bounds
			if (deltaX < 0 || deltaY < 0 || deltaX >= dimX || deltaY >= dimY)
			{
				continue;
			}

			float factor = -1;

			if ((j == (_kernelSize - 1) / 2) && (i == (_kernelSize - 1) / 2))
			{
				factor = kernelSize - 1;
			}
			int srcPixelIndex = deltaY * srcRowPitch + deltaX * srcPixelSize;
			uint8_t redTexel = pSrcData[srcPixelIndex + 2];
			uint8_t greenTexel = pSrcData[srcPixelIndex + 1];
			uint8_t blueTexel = pSrcData[srcPixelIndex + 0];

			redCol += factor * redTexel;
			greenCol += factor * greenTexel;
			blueCol += factor * blueTexel;
		}
	}

	//
	// Normalize our bicubic sum back to the valid pixel range
	//
	float fScaleFactor = -1.0 / (kernelSize * 3);
	redCol = fScaleFactor * redCol;
	greenCol = fScaleFactor * greenCol;
	blueCol = fScaleFactor * blueCol;

	if (redCol < 0)
		redCol = 0;
	if (greenCol < 0)
		greenCol = 0;
	if (blueCol < 0)
		blueCol = 0;
	if (redCol > 255)
		redCol = 255;
	if (greenCol > 255)
		greenCol = 255;
	if (blueCol > 255)
		blueCol = 255;


	//
	// Write our weighted sum to our output
	//

	uint8_t* pDstdata = _pDstImage.pixels;
	int dstPixelIndex = (_y / _scalingFactor) * _pDstImage.rowPitch + (_x / _scalingFactor) * srcPixelSize;
	pDstdata[dstPixelIndex + 2] = (uint8_t)redCol;
	pDstdata[dstPixelIndex + 1] = (uint8_t)greenCol;
	pDstdata[dstPixelIndex + 0] = (uint8_t)blueCol;
	pDstdata[dstPixelIndex + 3] = 255;
}

void HighPassFilter::highPassDownsampling(const Image& _pSrcImage,
	int _kernelSize, const Image& _pDstImage, const int _scalingFactor)
{
	int dimX = _pSrcImage.width;
	int dimY = _pSrcImage.height;

	for (int y = 0; y < dimY; y += _scalingFactor)
	{
		for (int x = 0; x < dimX; x += _scalingFactor)
		{
			highPassKernel(_pSrcImage, _kernelSize, x, y, _pDstImage, _scalingFactor);
		}

	}
}