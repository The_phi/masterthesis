﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereTextureGenerator : MonoBehaviour
{
    public int m_dimX = 1024;
    public int m_dimY = 1024;
    public Vector3 m_specificCenter;
    public bool m_useSpecificCenter = false;

    private GameObject m_targetGO;
    private int m_targetInstanceId = 0;

    // for debugging only
    public Color m_color;

    private float m_xOffset;
    private float m_yOffset;

    private Texture2D m_texture;
    private Bounds m_bounds;

    // Start is called before the first frame update
    void Start()
    {
        m_targetGO = gameObject;
        m_targetInstanceId = m_targetGO.GetInstanceID();

        m_xOffset = 360.0f / m_dimX;
        m_yOffset = 180.0f / m_dimY;

        m_texture = new Texture2D(m_dimX, m_dimY);
        m_bounds = m_targetGO.GetComponent<Renderer>().bounds;
        Debug.Log(m_bounds.center);

        iterateTexture();
        string filePath = "screenGrabs/" + m_targetGO.name +  "SphericalViewDep.png";
        saveTextureAsPNG(filePath);


    }

    private void iterateTexture()
    {
        Debug.Log(m_bounds.center);
        Vector3 center = m_bounds.center;
        center = this.transform.position;
        if(m_useSpecificCenter)
        {
            center = m_specificCenter;
        }

        for (int x = 0; x < m_dimX; x++)
        {
            float xGrad = x * m_xOffset;
            xGrad = xGrad * Mathf.Deg2Rad;
            for (int y = 0; y < m_dimY; y++)
            {
                float yGrad = y * m_yOffset;
                yGrad = yGrad * Mathf.Deg2Rad;
                Vector3 rayOrigin;
                SphericalToCartesian(5, xGrad, yGrad, out rayOrigin);

                Vector3 rayDirection = -rayOrigin;
                rayOrigin = center + rayOrigin;
                Color color = performRaycast(rayDirection, rayOrigin);

                m_texture.SetPixel(x, y, color);
            }
        }
    }

    private Color performRaycast(Vector3 _rayDirection, Vector3 _rayOrigin)
    {
        RaycastHit raycastHit;

        if (Physics.Raycast(_rayOrigin, _rayDirection, out raycastHit))
        {
            if (raycastHit.collider.gameObject.GetInstanceID() != m_targetInstanceId)
            {
                Debug.Log("wrong hit");
                return new Color(1, 1, 1, 0);
            }

            Renderer renderer = raycastHit.collider.GetComponent<MeshRenderer>();
            Texture2D texture2D = renderer.material.mainTexture as Texture2D;
            Vector2 pCoord = raycastHit.textureCoord;
            pCoord.x *= texture2D.width;
            pCoord.y *= texture2D.height;

            Vector2 tiling = renderer.material.mainTextureScale;
            Color color = texture2D.GetPixel(Mathf.FloorToInt(pCoord.x * tiling.x), Mathf.FloorToInt(pCoord.y * tiling.y));
            m_color = color;

            return color;
        }

        return new Color(1,1,1,0);
    }

    // polar is theta is x
    // elevation is phi is y
    // taken from https://gamedev.stackexchange.com/a/81715
    public static void SphericalToCartesian(float radius, float polar, float elevation, out Vector3 outCart)
    {
        //float a = radius * Mathf.Cos(elevation);
        //outCart.x = a * Mathf.Cos(polar);
        //outCart.y = radius * Mathf.Sin(elevation);
        //outCart.z = a * Mathf.Sin(polar);

        outCart.x = radius * Mathf.Sin(polar) * Mathf.Cos(elevation);
        outCart.y = radius * Mathf.Sin(polar) * Mathf.Sin(elevation);
        outCart.z = radius * Mathf.Cos(polar);
    }

    // taken from https://gamedev.stackexchange.com/a/81715
    public static void CartesianToSpherical(Vector3 cartCoords, out float outRadius, out float outPolar, out float outElevation)
    {
        if (cartCoords.x == 0)
        {
            cartCoords.x = Mathf.Epsilon;
        }
        outRadius = Mathf.Sqrt((cartCoords.x * cartCoords.x)
                        + (cartCoords.y * cartCoords.y)
                        + (cartCoords.z * cartCoords.z));
        outPolar = Mathf.Atan(cartCoords.z / cartCoords.x);
        if (cartCoords.x < 0)
            outPolar += Mathf.PI;
        outElevation = Mathf.Asin(cartCoords.y / outRadius);
    }

    private void saveTextureAsPNG(string _filePath)
    {
        System.IO.File.WriteAllBytes(_filePath, m_texture.EncodeToPNG());
    }
}
