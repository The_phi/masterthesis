#include "pch.h"

#include "PngToDDsConverter.h"
#include <iostream>
#include <filesystem>

namespace fs = std::filesystem;


// png handling
#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")


// DirectX 12 specific headers.
// D3D12 extension library.
#include "DirectXTex/d3dx12.h"
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"
using namespace DirectX;


#include "TextureHandler.h"

PngToDDsConverter::PngToDDsConverter(TextureHandler* _pTexHandler)
{
	// Start Gdiplus 
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);

	m_pTexHandler = _pTexHandler;
}

PngToDDsConverter::~PngToDDsConverter()
{
	// Shutdown Gdiplus 
	Gdiplus::GdiplusShutdown(m_gdiplusToken);
}

void PngToDDsConverter::convertFolder(std::string _folderPath)
{
	std::string path = std::filesystem::current_path().string() + "/" + _folderPath;
	for (const auto & entry : fs::directory_iterator(path))
	{
		const std::string filePath = entry.path().string().c_str();
		const std::wstring wsTmp(filePath.begin(), filePath.end());
		const wchar_t* fileNameChar = wsTmp.c_str();
		
		Bitmap* pBitmap = Bitmap::FromFile(fileNameChar);
		BitmapData bitmapData;
		pBitmap->LockBits(&Gdiplus::Rect(0, 0, pBitmap->GetWidth(), pBitmap->GetHeight()),
			ImageLockModeWrite, PixelFormat32bppARGB, &bitmapData);

		// generate dds scratch image
		TexMetadata mdata = {};
		mdata.width = bitmapData.Width;
		mdata.height = bitmapData.Height;
		mdata.dimension = TEX_DIMENSION_TEXTURE2D;
		mdata.depth = mdata.arraySize = 1;
		mdata.mipLevels = 1;
		mdata.format = DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM;
		ScratchImage scratchImage;
		// init scratch image
		HRESULT hr = scratchImage.Initialize(mdata);
		if (FAILED(hr))
		{
			std::cout << "could not convert image: " << filePath << "\n";
			continue;
		}

		const DirectX::Image* dstImage = scratchImage.GetImage(0, 0, 0);
		const int dimX = dstImage->width;
		const int dimY = dstImage->height;
		uint8_t* pDstData = dstImage->pixels;
		const size_t dstRowPitch = dstImage->rowPitch;
		const size_t dstPixelSize = dstRowPitch / dimX;

		unsigned int *pRawBitmap = (unsigned int*)bitmapData.Scan0;   // for easy access and indexing

		for (int y = 0; y < dimY; ++y)
		{
			for (int x = 0; x < dimX; ++x)
			{
				unsigned int curColor = pRawBitmap[y * bitmapData.Stride / 4 + x];
				int blue = curColor & 0xff;
				int green = (curColor & 0xff00) >> 8;
				int red = (curColor & 0xff0000) >> 16;
				int alpha = (curColor & 0xff000000) >> 24;

				int dstPixelIndex = y * dstRowPitch + x * dstPixelSize;
				pDstData[dstPixelIndex + 0] = (uint8_t)blue;
				pDstData[dstPixelIndex + 1] = (uint8_t)green;
				pDstData[dstPixelIndex + 2] = (uint8_t)red;
				pDstData[dstPixelIndex + 3] = (uint8_t) alpha;
			}
		}

		// construct new file path
		std::string fileName = filePath.substr(filePath.find_last_of("\\") + 1);
		fileName = fileName.substr(0, fileName.size() - 4);
		std::string folderPath = filePath.substr(0, filePath.find_last_of("\\"));
		std::string ddsFilePath = folderPath + "/" + fileName + ".dds";
		std::wstring fileNameWStr(ddsFilePath.begin(), ddsFilePath.end());
		m_pTexHandler->saveTexture(scratchImage, fileNameWStr);
	}
}
