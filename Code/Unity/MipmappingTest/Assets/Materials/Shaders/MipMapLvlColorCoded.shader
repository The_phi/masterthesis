﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// taken from https://forum.unity.com/threads/color-coded-mipmap-levels-shader.256290/

Shader "Custom/MipMapLvlColorCoded"
{
	Properties{
	 _MainTex("Diffuse Texture", 2D) = "white"{}

	 _ShowTexture("Show Texture", Range(0.0,1.0)) = 1

	 _MipMapLvl_00("MipMapLvl 00", Color) = (1.0, 0.0, 0.0, 1.0)
	 _MipMapLvl_01("MipMapLvl 01", Color) = (0.0, 1.0, 0.0, 1.0)
	 _MipMapLvl_02("MipMapLvl 02", Color) = (0.0, 0.0, 1.0, 1.0)
	 _MipMapLvl_03("MipMapLvl 03", Color) = (1.0, 0.0, 0.72, 1.0)
	 _MipMapLvl_04("MipMapLvl 04", Color) = (1.0, 1.0, 0.0, 1.0)
	 _MipMapLvl_05("MipMapLvl 05", Color) = (1.0, 0.64, 0.0, 1.0)
	 _MipMapLvl_06("MipMapLvl 06", Color) = (0.0, 0.75, 1.0, 1.0)
	 _MipMapLvl_07("MipMapLvl 07", Color) = (0.5, 0.5, 0.5, 1.0)
	 _MipMapLvl_08("MipMapLvl 08", Color) = (0.75, 0.0, 0.75, 1.0)
	 _MipMapLvl_09("MipMapLvl 09", Color) = (0.35, 0.45, 0.85, 1.0)
	 _MipMapLvl_10("MipMapLvl 10", Color) = (0.8, 0.2, 1.0, 1.0)
	 _MipMapLvl_11("MipMapLvl 11", Color) = (0.0, 0.0, 0.0, 1.0)

	}
	SubShader{
			
		Tags { "LightMode" = "ForwardBase" }
		LOD 200

		Pass
		{
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag


			//User defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _MainTex_TexelSize; //Supplies (1/width, 1/height, width, height)
			uniform fixed _ShowTexture;

			uniform fixed4 _MipMapLvl_00;
			uniform fixed4 _MipMapLvl_01;
			uniform fixed4 _MipMapLvl_02;
			uniform fixed4 _MipMapLvl_03;
			uniform fixed4 _MipMapLvl_04;
			uniform fixed4 _MipMapLvl_05;
			uniform fixed4 _MipMapLvl_06;
			uniform fixed4 _MipMapLvl_07;
			uniform fixed4 _MipMapLvl_08;
			uniform fixed4 _MipMapLvl_09;
			uniform fixed4 _MipMapLvl_10;
			uniform fixed4 _MipMapLvl_11;


			//The struct we input into the vert function
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				fixed2 texcoord : TEXCOORD0;

			};

			//What we output from the vert function and send into the fragment function
			struct vertexOutput {
				float4 pos : SV_POSITION;
				fixed2 tex : TEXCOORD0;

			};

			//vertex function
			vertexOutput vert(vertexInput input)
			{
				vertexOutput o;

				o.tex = input.texcoord;
				o.pos = UnityObjectToClipPos(input.vertex);

				return o;
			}


			//fragment function
			fixed4 frag(vertexOutput i) : COLOR
			{
				// per pixel screen space partial derivatives
				// rate of change in both directions
				float2 dx = ddx(i.tex);
				float2 dy = ddy(i.tex);

				// convert normalized texture coordinates to texel units before calling mip_map_level
				float2 textureCoord = i.tex * _MainTex_TexelSize.zw;

				// ddx Returns the partial derivative of the input In with respect to the screen-space x-coordinate.
				// rate of change in texture coord
				float2 dx_vtc = ddx(textureCoord);
				float2 dy_vtc = ddy(textureCoord);
				float delta_max_sqr = max(dot(dx_vtc, dx_vtc), dot(dy_vtc, dy_vtc));
				half mipmapLevel = max(0, 0.5 * log2(delta_max_sqr));


				half mipLevelFloor = floor(mipmapLevel);
				fixed4 weight = mipmapLevel - mipLevelFloor;
				fixed4 colorOut = fixed4(1.0, 1.0, 1.0, 1.0);

				if (_ShowTexture < 0.5) {

					if (mipLevelFloor == 0.0) colorOut = lerp(_MipMapLvl_00, _MipMapLvl_01, weight);
					else if (mipLevelFloor == 1.0) colorOut = lerp(_MipMapLvl_01, _MipMapLvl_02, weight);
					else if (mipLevelFloor == 2.0) colorOut = lerp(_MipMapLvl_02, _MipMapLvl_03, weight);
					else if (mipLevelFloor == 3.0) colorOut = lerp(_MipMapLvl_03, _MipMapLvl_04, weight);
					else if (mipLevelFloor == 4.0) colorOut = lerp(_MipMapLvl_04, _MipMapLvl_05, weight);
					else if (mipLevelFloor == 5.0) colorOut = lerp(_MipMapLvl_05, _MipMapLvl_06, weight);
					else if (mipLevelFloor == 6.0) colorOut = lerp(_MipMapLvl_06, _MipMapLvl_07, weight);
					else if (mipLevelFloor == 7.0) colorOut = lerp(_MipMapLvl_07, _MipMapLvl_08, weight);
					else if (mipLevelFloor == 8.0) colorOut = lerp(_MipMapLvl_08, _MipMapLvl_09, weight);
					else if (mipLevelFloor == 9.0) colorOut = lerp(_MipMapLvl_09, _MipMapLvl_10, weight);
					else if (mipLevelFloor == 10.0) colorOut = lerp(_MipMapLvl_10, _MipMapLvl_11, weight);
					else if (mipLevelFloor == 11.0) colorOut = _MipMapLvl_11;

				}
				else
				{
						colorOut = tex2Dgrad(_MainTex, i.tex.xy*_MainTex_ST.xy + _MainTex_ST.zw, dx, dy);

				}


				return colorOut;
			}
		ENDCG

		}
	}

	FallBack "Diffuse"
}
