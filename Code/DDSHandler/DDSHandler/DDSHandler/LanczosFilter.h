#pragma once

#include <cmath>
// D3D12 extension library.
#include "DirectXTex/d3dx12.h"
#include "DirectXTex/DirectXTex.h"
#include "DirectXTex/DirectXTexP.h"

using namespace DirectX;


constexpr double m_pi = 3.14159265358979323846;

//
// Normalized sinc function
//
inline double vnSinc(double fX)
{
	if (0.0 == fX) return 1.0;

	return sin(m_pi * fX) / (m_pi * fX);
}

//
// Lanczos weighing function
//
inline double vnLanczosWeight(double fN, double fDistance)
{
	if (fDistance <= fN)
	{
		return vnSinc(fDistance) * vnSinc(fDistance / fN);
	}

	return 0.0f;
}

class LanczosFilter
{
	public:
		//
		// Lanczos Kernel
		//
		//   LanczosKernel performs a simple combined 2D lanczos sampling of a source image.
		//   Lanczos filters are non-separable. 
		// 
		// Parameters:
		// 
		//   pSrcImage:  The read-only source image to filter.
		//
		//   fCoeffN:    The theta lanczos coefficient
		//
		//   fX, fY:     Specifies the coordinates to sample in the source image.
		//
		//
		//
		static void LanczosKernel(const Image& _pSrcImage,
			double _fCoeffN, int _fX, int _fY, const Image& _pDstImage, const int _scalingFactor);

		static void lanczosDownsampling(const Image& _pSrcImage,
			double _fCoeffN, const Image& _pDstImage, const int _scalingFactor);
};

