#include "pch.h"
#include "MatrixMath.h"

Eigen::ArrayXXd& MatrixMath::rdivide(const Eigen::ArrayXXd& _A, const Eigen::ArrayXXd& _B)
{
	if (_A.rows() != _B.rows())
	{
		throw std::invalid_argument("matrices not matching");
	}
	if (_A.cols() != _B.cols())
	{
		throw std::invalid_argument("matrices not matching");
	}

	Eigen::ArrayXXd* result = new Eigen::ArrayXXd(_A.rows(), _A.cols());

	for (int x = 0; x < _A.rows(); ++x)
	{
		for (int y = 0; y < _A.cols(); ++y)
		{
			(*result)(x, y) = _A(x, y) / _B(x, y);
		}

	}

	return *result;
}

void MatrixMath::setZeroOnSmaller(Eigen::ArrayXXd& _mat, const Eigen::ArrayXXd& _comp, double _e)
{
	for (int x = 0; x < _mat.rows(); ++x)
	{
		for (int y = 0; y < _mat.cols(); ++y)
		{
			double elem = _mat(x, y);
			if (std::isnan(elem))
			{
				_mat(x, y) = 0;
			}
			else if (_comp(x, y) < _e)
			{
				_mat(x, y) = 0;
			}
			else
			{
			}
		}
	}
}