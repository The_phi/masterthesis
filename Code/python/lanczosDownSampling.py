import argparse
import imutils
import cv2
import numpy as np
from PIL import Image
import scipy.misc




ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True, help="image path")
args = vars(ap.parse_args())
argFirst = args["first"]

albedoImg = cv2.imread(argFirst).astype(np.float32)


# write original image as base mip level
cv2.imwrite("mip0.png", albedoImg)
# create mip maps
scaleFactor = 2
origImgShape = albedoImg.shape
for i in range(0, 10):
    scaledImage = cv2.resize(albedoImg, dsize=(int(origImgShape[0] / scaleFactor),
            int(origImgShape[1] / scaleFactor)), interpolation=cv2.INTER_LANCZOS4)
    # save to file
    cv2.imwrite("mip" + str(i + 1) + ".png", scaledImage)

    scaleFactor = scaleFactor * 2